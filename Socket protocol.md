# Socket protocol

## UDP multicast

### Base format (client->serveur)

| length (in bytes) | type | meaning            |
| ----------------- | ---- | ------------------ |
| 1                 | byte | id                 |
| 6                 | addr | addr of the client |

#### details:

- **id**: the id of the message

#### description:

Used as a template for other messages (id value is define there and arguments composition is precised here).

### List user

| length                                 | type   | meaning |
| -------------------------------------- | ------ | ------- |
| 4                                      | int    | nb_user |
| $\sum_{i=1}^{n}{7+length(username_i)}$ | User[] | users   |

With `User` defined as:

| length                | type   | meaning  |
| --------------------- | ------ | -------- |
| 4                     | byte[] | ip_addr  |
| 2                     | short  | port     |
| ??? (zero terminated) | string | username |

#### details:

$\forall user \in users$

- **ip_addr**: the ip address of the client's *tcp service socket*
- **port**: the port of the client's *tcp service socket*
- **username**: the username to associate with the client

#### description:

- **id**: 0x10
- **length**: $4+\sum_{i=1}^{n}{7+length(username_i)}$
- **send by**: the server
- **received by**: every client
- **when**: after a client connect, disconnect or request the user list

### Join / Username change

| length               | type   | meaning  |
| -------------------- | ------ | -------- |
| 4                    | byte[] | ip_addr  |
| 2                    | short  | port     |
| ??? (zero terminated) | string | username |

#### details:

- **ip_addr**: the ip address of the client's *tcp service socket*
- **port**: the port of the client's *tcp service socket*
- **username**: the username to associate with the client

#### description:

- **id**: 0x00
- **length**: 7 + length(username)
- **send by**: a client
- **received by**: the server
- **when**: a client first join the group multicast *or* a client want to change it username


### Leave

| length | type   | meaning |
| ------ | ------ | ------- |
| 4      | byte[] | ip_addr |
| 2      | short  | port    |

#### details:

- **ip_addr**:  the ip address of the client's *tcp service socket*
- **port**: the port of the client's *tcp service socket*

#### description:

- **id**: 0x01
- **length**: 6
- **send by**: a client
- **received by**: the server
- **when**: a client disconnect

### Ping

| length | type | meaning |
||||
||



#### details:

#### description:

- **id**: 0x11
- **length**: 0
- **send by**: the server
- **received by**: every client
- **when**: periodically

### Pong

| length | type   | meaning |
| ------ | ------ | ------- |
| 4      | byte[] | ip_addr |
| 2      | short  | port    |

#### details:

- **ip_addr**:  the ip address of the client's *tcp service socket*
- **port**: the port of the client's *tcp service socket*

#### description:

- **id**: 0x02
- **length**: 6
- **send by**: a client
- **received by**: the server
- **when**: after receiving a ping from the serveur

### Request list

| length | type | meaning |
||||
||



#### details:

#### description:

- **id**: 0x03
- **length**: 0
- **send by**: a client
- **received by**: the server
- **when**: when the client require a new client list

### Connection failure

| length | type   | meaning |
| ------ | ------ | ------- |
| 4      | byte[] | ip_addr |
| 2      | short  | port    |
| 1      | byte   | reason  |

#### details:

- **ip_addr**: the ip v4 of the client's tcp
- **port**: the port of the of the client's tcp


- **reason**: motif of the connection. Can be:
  - 0x00: you're banned
  - 0x01: too many users are connected
  - 0x02: your username is too long
  - 0x03: you didn't properly joined

(Realisticaly, only 0x00 will happen but neah, better have it now).

#### description:

- **id**: 0x12
- **length**: 1
- **send by**: the server
- **received by**: a client
- **when**: a client try to join but the server has to decline

### Closing

| length | type | meaning |
||||
||

#### details:

#### description:

- **id**: 0x13
- **length**: 1
- **send by**: the server
- **received by**: every client
- **when**: the server close

## TCP

| length | type   | meaning |
| ------ | ------ | ------- |
| 4      | int    | length  |
| length | string | message |

#### details:

- **length**:  the length of the message
- **message**: the message of the client. Not zero terminated (allocation length + 1 if you want a zero terminated string)

