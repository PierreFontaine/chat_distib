{
  "list-client": {
    "from": "server",
    "to": "client",
    "descr": "A message send by the server to update the clients' list of connected clients.",
    "message": {
      "name": "message",
      "size": null,
      "descr": "A message containing the list of the client connected",
      "d_size": 22,
      "child": [
        {
          "name": "base",
          "size": 1,
          "descr": "The base of every message",
          "d_size": 1,
          "child": [
            {
              "name": "id",
              "size": 1,
              "descr": "The id of the message (0x10)",
              "d_size": 1,
              "child": []
            }
          ]
        },
        {
          "name": "nb-client",
          "size": 4,
          "descr": "The number of client connected",
          "d_size": 4,
          "child": []
        },
        {
          "name": "client-list",
          "size": null,
          "descr": "The list of the clients connected (number stored in bytes 2-5)",
          "d_size": 17,
          "child": [
            {
              "name": "client",
              "size": null,
              "descr": "A client with an username and the TCP's address of",
              "d_size": 16,
              "child": [
                {
                  "name": "address",
                  "size": 6,
                  "descr": "The address of a client's TCP (ipv4 + port)",
                  "d_size": 6,
                  "child": [
                    {
                      "name": "ip",
                      "size": 4,
                      "descr": "The ipv4 of a client's TCP",
                      "d_size": 4,
                      "child": []
                    },
                    {
                      "name": "port",
                      "size": 2,
                      "descr": "The port of a client's TCP",
                      "d_size": 2,
                      "child": []
                    }
                  ]
                },
                {
                  "name": "username",
                  "size": null,
                  "descr": "The username of a client (0-terminated)",
                  "d_size": 10,
                  "child": []
                }
              ]
            },
            "..."
          ]
        }
      ]
    },
    "example": {
      "List with no client": [
        {
          "name": "id",
          "sub": "0x10",
          "values": [16]
        },
        {
          "name": "nb-client",
          "sub": "2",
          "values": [0, 0, 0, 0]
        }
      ],
      "List with 2 clients: Foo & Bar": [
        {
          "name": "id",
          "sub": "0x10",
          "values": [16]
        },
        {
          "name": "nb-client",
          "sub": "2",
          "values": [0, 0, 0, 2]
        },
        {
          "name": "ip 1",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port 1",
          "sub": "6000",
          "values": [23, 112]
        },
        {
          "name": "username 1",
          "sub": "\"Foo\"",
          "values": [70, 111, 111, 0]
        },
        {
          "name": "ip 2",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port 2",
          "sub": "6001",
          "values": [23, 113]
        },
        {
          "name": "username 2",
          "sub": "\"Bar\"",
          "values": [66, 97, 111, 0]
        }
      ]
    }
  },
  "join": {
    "from": "client",
    "to": "server",
    "descr": "A message send by a client to join a server or change it username.",
    "message": {
      "name": "join",
      "size": null,
      "descr": "A message containing a connection request",
      "d_size": 17,
      "child": [
        {
          "name": "base-client",
          "size": 7,
          "descr": "The base of every client's message",
          "d_size": 7,
          "child": [
            {
              "name": "base",
              "size": 1,
              "descr": "The base of every message",
              "d_size": 1,
              "child": [
                {
                  "name": "id",
                  "size": 1,
                  "descr": "The id of the message (0x00)",
                  "d_size": 1,
                  "child": []
                }
              ]
            },
            {
              "name": "address",
              "size": 6,
              "descr": "The address of the client's TCP (ipv4 + port)",
              "d_size": 6,
              "child": [
                {
                  "name": "ip",
                  "size": 4,
                  "descr": "The ipv4 of the client's TCP",
                  "d_size": 4,
                  "child": []
                },
                {
                  "name": "port",
                  "size": 2,
                  "descr": "The port of the client's TCP",
                  "d_size": 2,
                  "child": []
                }
              ]
            }
          ]
        },
        {
          "name": "username",
          "size": null,
          "descr": "The username of a client (0-terminated)",
          "d_size": 10,
          "child": []
        }
      ]
    },
    "example": {
      "Foo join the server": [
        {
          "name": "id",
          "sub": "0x00",
          "values": [0]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        },
        {
          "name": "port",
          "sub": "\"Foo\"",
          "values": [70, 111, 111, 0]
        }
      ]
    }
  },
  "leave": {
    "from": "client",
    "to": "server",
    "descr": "A message send by a client before disconnecting.",
    "message": {
      "name": "message",
      "size": 7,
      "descr": "A message informing of the disconnect",
      "d_size": 7,
      "child": [
        {
          "name": "base-client",
          "size": 7,
          "descr": "The base of every client's message",
          "d_size": 7,
          "child": [
            {
              "name": "base",
              "size": 1,
              "descr": "The base of every message",
              "d_size": 1,
              "child": [
                {
                  "name": "id",
                  "size": 1,
                  "descr": "The id of the message (0x01)",
                  "d_size": 1,
                  "child": []
                }
              ]
            },
            {
              "name": "address",
              "size": 6,
              "descr": "The address of the client's TCP (ipv4 + port)",
              "d_size": 6,
              "child": [
                {
                  "name": "ip",
                  "size": 4,
                  "descr": "The ipv4 of the client's TCP",
                  "d_size": 4,
                  "child": []
                },
                {
                  "name": "port",
                  "size": 2,
                  "descr": "The port of the client's TCP",
                  "d_size": 2,
                  "child": []
                }
              ]
            }
          ]
        }
      ]
    },
    "example": {
      "The client with the TCP at 127.0.0.1:6000 disconnect": [
        {
          "name": "id",
          "sub": "0x01",
          "values": [1]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        }
      ]
    }
  },
  "ping":{
    "from": "server",
    "to": "client",
    "descr": "A message send by the server to check that a client didn't leave",
    "message": {
      "name": "message",
      "size": 1,
      "descr": "A ping",
      "d_size": 1,
      "child": [
        {
          "name": "base",
          "size": 1,
          "descr": "The base of every message",
          "d_size": 1,
          "child": [
            {
              "name": "id",
              "size": 1,
              "descr": "The id of the message (0x11)",
              "d_size": 1,
              "child": []
            }
          ]
        }
      ]
    },
    "example": {
      "The server ping": [
        {
          "name": "id",
          "sub": "0x11",
          "values": [17]
        }
      ]
    }
  },
  "pong": {
    "from": "client",
    "to": "server",
    "descr": "A message send by a client to inform the server that it's still here.",
    "message": {
      "name": "message",
      "size": 7,
      "descr": "A pong",
      "d_size": 7,
      "child": [
        {
          "name": "base-client",
          "size": 7,
          "descr": "The base of every client's message",
          "d_size": 7,
          "child": [
            {
              "name": "base",
              "size": 1,
              "descr": "The base of every message",
              "d_size": 1,
              "child": [
                {
                  "name": "id",
                  "size": 1,
                  "descr": "The id of the message (0x02)",
                  "d_size": 1,
                  "child": []
                }
              ]
            },
            {
              "name": "address",
              "size": 6,
              "descr": "The address of the client's TCP (ipv4 + port)",
              "d_size": 6,
              "child": [
                {
                  "name": "ip",
                  "size": 4,
                  "descr": "The ipv4 of the client's TCP",
                  "d_size": 4,
                  "child": []
                },
                {
                  "name": "port",
                  "size": 2,
                  "descr": "The port of the client's TCP",
                  "d_size": 2,
                  "child": []
                }
              ]
            }
          ]
        }
      ]
    },
    "example": {
      "The client with the TCP at 127.0.0.1:6000 pong": [
        {
          "name": "id",
          "sub": "0x01",
          "values": [1]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        }
      ]
    }
  },
  "req_list": {
    "from": "client",
    "to": "server",
    "descr": "A message send by a client to request the list of clients.",
    "message": {
      "name": "message",
      "size": 7,
      "descr": "A message requesting the list of clients",
      "d_size": 7,
      "child": [
        {
          "name": "base-client",
          "size": 7,
          "descr": "The base of every client's message",
          "d_size": 7,
          "child": [
            {
              "name": "base",
              "size": 1,
              "descr": "The base of every message",
              "d_size": 1,
              "child": [
                {
                  "name": "id",
                  "size": 1,
                  "descr": "The id of the message (0x03)",
                  "d_size": 1,
                  "child": []
                }
              ]
            },
            {
              "name": "address",
              "size": 6,
              "descr": "The address of the client's TCP (ipv4 + port)",
              "d_size": 6,
              "child": [
                {
                  "name": "ip",
                  "size": 4,
                  "descr": "The ipv4 of the client's TCP",
                  "d_size": 4,
                  "child": []
                },
                {
                  "name": "port",
                  "size": 2,
                  "descr": "The port of the client's TCP",
                  "d_size": 2,
                  "child": []
                }
              ]
            }
          ]
        }
      ]
    },
    "example": {
      "The client with the TCP at 127.0.0.1:6000 request the client list": [
        {
          "name": "id",
          "sub": "0x01",
          "values": [1]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        }
      ]
    }
  },
  "err_co": {
    "from": "server",
    "to": "client",
    "descr": "A message send by the server after a config change or an illegal client message. Contain the motif of the error.<br/>Valid motifs are:<ul><li>0 => you're banned</li><li>1 => too many client already connected</li><li>2 => username too long</li><li>3 => not connected</li></ul>",
    "message": {
      "name": "message",
      "size": 8,
      "descr": "A error message",
      "d_size": 8,
      "child": [
        {
          "name": "base",
          "size": 1,
          "descr": "The base of every message",
          "d_size": 1,
          "child": [
            {
              "name": "id",
              "size": 1,
              "descr": "The id of the message (0x12)",
              "d_size": 1,
              "child": []
            }
          ]
        },
        {
          "name": "address",
          "size": 6,
          "descr": "The address of the client's TCP (ipv4 + port)",
          "d_size": 6,
          "child": [
            {
              "name": "ip",
              "size": 4,
              "descr": "The ipv4 of the client's TCP",
              "d_size": 4,
              "child": []
            },
            {
              "name": "port",
              "size": 2,
              "descr": "The port of the client's TCP",
              "d_size": 2,
              "child": []
            }
          ]
        },
        {
          "name": "error",
          "size": 1,
          "descr": "The error code",
          "d_size": 1,
          "child": []
        }
      ]
    },
    "example": {
      "127.0.0.1:6000 is banned": [
        {
          "name": "id",
          "sub": "0x12",
          "values": [18]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        },
        {
          "name": "error",
          "sub": "banned",
          "values": [0]
        }
      ],
      "127.0.0.1:6000 is dropped because too many client are already connected": [
        {
          "name": "id",
          "sub": "0x12",
          "values": [18]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        },
        {
          "name": "error",
          "sub": "too many",
          "values": [1]
        }
      ],
      "127.0.0.1:6000's username is too long": [
        {
          "name": "id",
          "sub": "0x12",
          "values": [18]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        },
        {
          "name": "error",
          "sub": "too long",
          "values": [2]
        }
      ],
      "127.0.0.1:6000 sent a client message before joining the server": [
        {
          "name": "id",
          "sub": "0x12",
          "values": [18]
        },
        {
          "name": "ip",
          "sub": "127.0.0.1",
          "values": [127, 0, 0, 1]
        },
        {
          "name": "port",
          "sub": "6000",
          "values": [23, 112]
        },
        {
          "name": "error",
          "sub": "not joined",
          "values": [3]
        }
      ]
    }
  },
  "closing":{
    "from": "server",
    "to": "client",
    "descr": "A message send by the server to inform the clients it's closing",
    "message": {
      "name": "message",
      "size": 1,
      "descr": "A closing message",
      "d_size": 1,
      "child": [
        {
          "name": "base",
          "size": 1,
          "descr": "The base of every message",
          "d_size": 1,
          "child": [
            {
              "name": "id",
              "size": 1,
              "descr": "The id of the message (0x13)",
              "d_size": 1,
              "child": []
            }
          ]
        }
      ]
    },
    "example": {
      "The server close": [
        {
          "name": "id",
          "sub": "0x13",
          "values": [19]
        }
      ]
    }
  }
}