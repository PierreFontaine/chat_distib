#include <stdlib.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <pthread.h>

#include "terminal_lite.h"
#include "sync.h"
#include "utils.h"
#include "../lib/udp.h"

static const struct utils_option main_ignore_key = {
		.name = "ignore-key",
		.help = "if given, the program will not log inputted key",
		.param = NULL,
		.sh_opt = 'k',
};
static const struct utils_option main_ttl = {
		.name = "udp-ttl",
		.help = "the ttl of message sent through the UDP multicast",
		.param = "TTL",
		.sh_opt = 't',
};
static const struct utils_option main_port = {
		.name = "port",
		.help = "The port the server will use",
		.param = "PORT",
		.sh_opt = 'p',
};
static const struct utils_option main_address = {
		.name = "address",
		.help = "The address of the multicast to connect to",
		.param = "ADDRESS",
		.sh_opt = 'a',
};


bool main_filter_udp(void* null, struct message message){
	return true;
}
bool main_start_udp(struct sync* sync_obj, struct udp_obj_legacy* udp_obj, pthread_t* thread){
	struct udp_args_legacy* args = malloc(sizeof(struct udp_args_legacy));
	if(args == NULL){
		return false;
	}
	
	args->state = udp_obj;
	args->sync_obj = sync_obj;
	args->sync = (_Bool (*)(void*, struct message))sync_udp_adapter;
	args->filter_obj = NULL;
	args->filter = main_filter_udp;
	
	if(pthread_create(thread, NULL, (void* (*)(void*))udp_process_legacy, args) != 0){
		free(args);
		return false;
	}
	return true;
}
bool main_start_keyboard(struct sync* sync_obj, pthread_t* thread){
	struct keyboard_args* args = malloc(sizeof(struct keyboard_args));
	args->sync_obj = sync_obj;
	args->sync = (_Bool (*)(void*, struct keyboard_event))sync_keyboard_adapter;
	if(pthread_create(thread, NULL, (void* (*)(void*))keyboard_process, args) != 0){
		return false;
	}
	return true;
}

int main(int argc, char** argv){
	int port = 6000;
	char* address = "239.0.1.1";
	uint8_t ttl = 1;
	int ignore_key = 0;
	
	struct utils_option options[4];
	struct utils_task tasks[4];
	
	utils_common_setup_option(0, main_port, UTILS_SET_INT, &port, options, tasks);
	utils_common_setup_option(1, main_address, UTILS_SET_STR, &address, options, tasks);
	utils_common_setup_option(2, main_ttl, UTILS_SET_UINT8, &ttl, options, tasks);
	utils_common_setup_option(3, main_ignore_key, UTILS_INCR_INT, &ignore_key, options, tasks);
	
	printf("Parsing arguments");
	if(!utils_parse_args(argc, argv, tasks, 4)){
		SCREAM("Error while parsing the argument");
		return EXIT_FAILURE;
	}
	
	printf("Init object");
	struct sync* sync_obj = sync_init();
	if(sync_obj == NULL){
		SCREAM("Error while init the sync");
		return EXIT_FAILURE;
	}
	
	struct udp_obj_legacy* udp_obj = udp_init_legacy(inet_addr(address), (uint16_t)port);
	if(udp_obj == NULL){
		SCREAM("Error while init the udp object");
		sync_free(sync_obj);
		return EXIT_FAILURE;
	}
	if(!udp_change_ttl_legacy(udp_obj, ttl)){
		SCREAM("Unable to change the ttl of the udp");
		udp_free_legacy(udp_obj);
		sync_free(sync_obj);
		return EXIT_FAILURE;
	}
	
	printf("Starting threads");
	pthread_t thread_keyboard;
	if(!main_start_keyboard(sync_obj, &thread_keyboard)){
		SCREAM("Error starting the keyboard thread");
		udp_free_legacy(udp_obj);
		sync_free(sync_obj);
		return EXIT_FAILURE;
	}
	
	pthread_t thread_message;
	if(!main_start_udp(sync_obj, udp_obj, &thread_message)){
		SCREAM("Error starting the udp handler");
		udp_free_legacy(udp_obj);
		sync_free(sync_obj);
		return EXIT_FAILURE;
	}
	
	struct message_user user[10] = {
			{
					.addr = {.port = 6000, .ip = {127, 0, 0, 1}},
					.username = "User 0"
			},
			{
					.addr = {.port = 6001, .ip = {127, 0, 0, 1}},
					.username = "User 1"
			},
			{
					.addr = {.port = 6002, .ip = {127, 0, 0, 1}},
					.username = "User 2"
			},
			{
					.addr = {.port = 6003, .ip = {127, 0, 0, 1}},
					.username = "User 3"
			},
			{
					.addr = {.port = 6004, .ip = {127, 0, 0, 1}},
					.username = "User 4"
			},
			{
					.addr = {.port = 6005, .ip = {127, 0, 0, 1}},
					.username = "User 5"
			},
			{
					.addr = {.port = 6006, .ip = {127, 0, 0, 1}},
					.username = "User 6"
			},
			{
					.addr = {.port = 6007, .ip = {127, 0, 0, 1}},
					.username = "User 7"
			},
			{
					.addr = {.port = 6008, .ip = {127, 0, 0, 1}},
					.username = "User 8"
			},
			{
					.addr = {.port = 6009, .ip = {127, 0, 0, 1}},
					.username = "User 9"
			},
	};
	struct message_user buffer_user[10];
	int current = 0;
	bool selected[10] = {true, true, true, true, true, true, true, true, true, true};
	
	char* key_cmd_str[] = {
			"UP",
			"DOWN",
			"LEFT",
			"RIGHT",
			"ENTER",
			"DEL",
			"NONE",
	};
	
	struct termios t = keyboard_setup();
	
	term_clear();
	bool loop = true;
	while(loop){
		printf("\n\n\n\n\n\n");
		int height = term_height();
		for(int j=0; j<2; j++){
			term_move(1, height-3+j);
			for(int i=0; i<5; i++){
				int pos = i + j * 5;
				if(current == pos){
					term_set_color_front(term_bright(TERM_WHITE));
					term_write("[");
				}else{
					term_write(" ");
				}
				if(selected[pos]){
					term_set_color_front(term_bright(TERM_WHITE));
				}else{
					term_set_color_front(term_bright(TERM_BLACK));
				}
				
				term_write("User %d", pos);
				if(current == pos){
					term_set_color_front(term_bright(TERM_WHITE));
					term_write("]");
				}else{
					term_write(" ");
				}
				term_write("   ");
			}
		}
		term_move(1, height-1);
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("Client:     ");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("j");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("oin       ");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("l");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("eave      p");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("o");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("ng       ");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("r");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("eq_list");
		
		term_move(1, height);
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("Server:     li");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("s");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("t_user  p");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("i");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("ng       ");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("e");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("rr_co     ");
		term_set_color_front(term_bright(TERM_WHITE));
		term_write("c");
		term_set_color_front(term_bright(TERM_BLACK));
		term_write("losing");
		
		term_reset_color();
		fflush(stdout);
		struct sync_event event = sync_poll(sync_obj);
		for(int i=0; i<4; i++){
			term_move(1, height-i);
			term_clear_line();
		}
		term_move(1, height-6);
		switch(event.type){
			case SYNC_KEYBOARD:{
				if(ignore_key == 0){
					if(event.keyboard.type == KEYBOARD_KEY){
						printf("Received keyboard key %c (%d)\n", event.keyboard.key, event.keyboard.key);
					}else{
						printf("Received keyboard command \"%s\" (%d)\n", key_cmd_str[event.keyboard.cmd],
								event.keyboard.cmd);
					}
				}
				if(event.keyboard.type == KEYBOARD_KEY){
					switch(event.keyboard.key){
						case 's':{
							unsigned nb_selected = 0;
							for(int i=0; i<10; i++){
								if(selected[i]){
									buffer_user[nb_selected] = user[i];
									nb_selected++;
								}
							}
							udp_send_legacy(*udp_obj, message_get_lst_user(user, nb_selected));
							break;
						}
						case 'j':{
							udp_send_legacy(*udp_obj, message_get_join(user[current]));
							break;
						}
						case 'l':{
							udp_send_legacy(*udp_obj, message_get_leave(user[current].addr));
							break;
						}
						case 'i':{
							udp_send_legacy(*udp_obj, message_get_ping());
							break;
						}
						case 'o':{
							udp_send_legacy(*udp_obj, message_get_pong(user[current].addr));
							break;
						}
						case 'r':{
							udp_send_legacy(*udp_obj, message_get_req_lst(user[current].addr));
							break;
						}
						case 'e':{ // fixme allow other error code
							udp_send_legacy(*udp_obj, message_get_err_co(user[current].addr, MESSAGE_ERRCO_BANNED));
							break;
						}
						case 'c':{
							udp_send_legacy(*udp_obj, message_get_closing());
							break;
						}
						case 'y':{
							selected[current] = true;
							break;
						}
						case 'n':{
							selected[current] = false;
							break;
						}
						case 'q':{
							loop = false;
							break;
						}
						case '0':{current = 0; break;}
						case '1':{current = 1; break;}
						case '2':{current = 2; break;}
						case '3':{current = 3; break;}
						case '4':{current = 4; break;}
						case '5':{current = 5; break;}
						case '6':{current = 6; break;}
						case '7':{current = 7; break;}
						case '8':{current = 8; break;}
						case '9':{current = 9; break;}
						default:break;
					}
				}
				break;
			}
			case SYNC_UDP:{
				message_print(event.udp);
				break;
			}
			default:{
				SCREAM("Unknown event type");
				break;
			}
		}
	}
	sync_close(sync_obj);
	
	printf("Waiting for the threads to finish\n");
	printf(" -> Waiting for the keyboard thread (try inputting some keys)\n");
	pthread_join(thread_keyboard, NULL);
	printf(" -> Waiting for the message thread (waiting for a message)\n");
	pthread_join(thread_message, NULL);
	
	keyboard_reset(t);
	
	printf("Freeing resources\n");
	if(!udp_free_legacy(udp_obj)){
		SCREAM("Error while freeing the udp_obj_legacy");
		return EXIT_FAILURE;
	}
	if(!sync_free(sync_obj)){
		SCREAM("Error while freeing the sync");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}