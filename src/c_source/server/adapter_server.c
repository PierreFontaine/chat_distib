#include "adapter_server.h"

#include "utils.h"

#include <stdlib.h>

void adapter_server_user_count(core_t state, struct config_change_event* event){
	if(event->entry != CONFIG_USER_COUNT_MAX){
		SCREAM("Attempt to call adapter_server::adapter_server_user_count for the wrong entry (got id: %d)", event->entry);
		return;
	}
	core_set_user_max(state, event->value);
}
void adapter_server_username_length(core_t state, struct config_change_event* event){
	if(event->entry != CONFIG_USERNAME_LENGTH_MAX){
		SCREAM("Attempt to call adapter_server::adapter_server_username_length for the wrong entry (got id: %d)", event->entry);
		return;
	}
	core_set_username_length_max(state, event->value);
}
void adapter_server_timeout_max(core_t state, struct config_change_event* event){
	if(event->entry != CONFIG_TIMEOUT_MAX){
		SCREAM("Attempt to call adapter_server::adapter_server_timeout_max for the wrong entry (got id: %d)", event->entry);
		return;
	}
	core_set_timeout_max(state, event->value);
}

void adapter_server_ttl(udp_t udp, struct config_change_event* event){
	if(event->entry != CONFIG_MULTICAST_TTL){
		SCREAM("Attempt to call adapter_server::adapter_server_ttl for the wrong entry (got id: %d)", event->entry);
		return;
	}
	udp_change_ttl(udp, (uint8_t)event->value);
}

void adapter_server_timeout_freq(tim_t tim, struct config_change_event* event){
	if(event->entry != CONFIG_TIMEOUT_FREQ){
		SCREAM("Attempt to call adapter_server::adapter_server_timeout_freq for the wrong entry (got id: %d)", event->entry);
		return;
	}
	struct timespec timespec = {
			.tv_sec = event->value/1000,
			.tv_nsec = (event->value%1000)*1000,
	};
	tim_set_delay(tim, timespec);
}

void adapter_server_kick(udp_t udp, struct core_kick* kick){
	enum message_err_co_type type;
	switch(kick->type){
		case CORE_KICK_BANNED:{
			type = MESSAGE_ERRCO_BANNED;
			break;
		}
		case CORE_KICK_USER_COUNT:{
			type = MESSAGE_ERRCO_TOO_MANY_USER;
			break;
		}
		case CORE_KICK_USERNAME_LENGTH:{
			type = MESSAGE_ERRCO_USERNAME_TOO_LONG;
			break;
		}
		case CORE_KICK_TIMEOUT:{
			// type = MESSAGE_ER fixme error type for timeout
			// break;
			return;
		}
		default:{
			return;
		}
	}
	udp_send(udp, message_get_err_co(kick->user.addr, type));
}
void adapter_server_update(udp_t udp, array_t user_array){
	unsigned nb_user = (unsigned)array_size(user_array);
	struct message_user* user = malloc(sizeof(*user) * nb_user);
	if(user == NULL){
		SCREAM("Unable to alloc the user array to update the clients");
		return;
	}
	for(int i=0; i<nb_user; i++){
		user[i] = *(struct message_user*)array_get(user_array, i);
	}
	udp_send(udp, message_get_lst_user(user, nb_user));
	free(user);
}
void adapter_server_illegal(udp_t udp, struct message_addr* addr){
	udp_send(udp, message_get_err_co(*addr, MESSAGE_ERRCO_NOT_CONNECTED));
}

void adapter_server_process_message(core_t state, udp_t udp, struct message message){
	if(!message_from_client(message)){
		return;
	}
	struct message_addr addr = message_get_client(message);
	if(message.type != MESSAGE_TYPE_JOIN && !core_is_present(state, addr)){
		core_emit_illegal(state, addr);
		return;
	}
	core_reset_timeout(state, addr);
	switch(message.type){
		case MESSAGE_TYPE_JOIN:{
			switch(core_add_user(state, message.join)){
				case CORE_JOIN_OK:{
					// NO-OP
					break;
				}
				case CORE_JOIN_ALREADY_HERE:{
					// NO-OP
					break;
				}
				case CORE_JOIN_BANNED:{
					udp_send(udp, message_get_err_co(message.join.addr, MESSAGE_ERRCO_BANNED));
					break;
				}
				case CORE_JOIN_USER_COUNT:{
					udp_send(udp, message_get_err_co(message.join.addr, MESSAGE_ERRCO_TOO_MANY_USER));
					break;
				}
				case CORE_JOIN_USERNAME_LENGTH:{
					udp_send(udp, message_get_err_co(message.join.addr, MESSAGE_ERRCO_USERNAME_TOO_LONG));
					break;
				}
				case CORE_JOIN_ERROR:{
					SCREAM("Internal error while attempting to add the client %s", message.join.username);
					return;
				}
			}
			break;
		}
		case MESSAGE_TYPE_LEAVE:{
			core_user_leave(state, message.leave);
			break;
		}
		case MESSAGE_TYPE_PONG:{
			// NO-OP
			break;
		}
		case MESSAGE_TYPE_REQ_LST:{
			core_emit_update(state);
			break;
		}
		default:{ // lst-user, ping, errco, closing
			// NO-OP
			return;
		}
	}
}

int adapter_server_get_user(core_t state, struct message_user** users){
	return core_get_users(state, users, NULL);
}
