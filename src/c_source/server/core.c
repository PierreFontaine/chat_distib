#include "core.h"

#include "utils.h"
#include "listener.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct core_user_cell{
	struct message_user user;
	int timeout;
};
struct core_state {
	array_t user_array;
	listener_t listener_kick;
	listener_t listener_update;
	listener_t listener_illegal;
	array_t ban_array;
	unsigned username_length;
	unsigned user_max;
	unsigned timeout_max;
};

core_t core_init(){
	core_t res = NULL;
	res = malloc(sizeof(*res));
	if(res == NULL){
		SCREAM("Failed to alloc a core_t");
		return NULL;
	}
	res->user_max = 10;
	res->username_length = 10;
	
	res->ban_array = array_init();
	if(res->ban_array == NULL){
		free(res);
		SCREAM("Failed to init ban_array");
		return NULL;
	}
	
	res->listener_kick = listener_init();
	if(res->listener_kick == NULL){
		array_free(res->ban_array);
		free(res);
		SCREAM("Failed to init listener_kick");
		return NULL;
	}
	
	res->listener_update = listener_init();
	if(res->listener_update == NULL){
		listener_free(res->listener_kick);
		array_free(res->ban_array);
		free(res);
		SCREAM("Failed to init listener_update");
		return NULL;
	}
	
	res->listener_illegal = listener_init();
	if(res->listener_illegal == NULL){
		listener_free(res->listener_update);
		listener_free(res->listener_kick);
		array_free(res->ban_array);
		free(res);
		SCREAM("Failed to init listener_illegal");
		return NULL;
	}
	
	res->user_array = array_init();
	if(res->user_array == NULL){
		listener_free(res->listener_illegal);
		listener_free(res->listener_update);
		listener_free(res->listener_kick);
		array_free(res->ban_array);
		free(res);
		SCREAM("Failed to init user_array");
	}
	
	return res;
}
bool core_free(core_t state){
	if(state == NULL){
		return true;
	}
	bool res = true;
	
	// free username present
	for(int i=0, c=array_size(state->user_array); i<c; i++){
		struct message_user* current = core_get_user(state, i);
		free(current->username);
	}
	res = array_free(state->user_array) && res;
	
	res = listener_free(state->listener_kick) && res;
	res = listener_free(state->listener_update) && res;
	res = listener_free(state->listener_illegal) && res;
	
	// free banned
	for(int i=0, c=array_size(state->ban_array); i<c; i++){
		struct message_user* current = array_get(state->ban_array, i);
		free(current->username);
	}
	res = array_free(state->ban_array) && res;
	free(state);
	return res;
}

enum core_join core_add_user(core_t state, struct message_user user){
	if(array_size(state->user_array) == state->user_max){
		return CORE_JOIN_USER_COUNT;
	}
	if(strlen(user.username) + 1 > state->username_length){
		return CORE_JOIN_USERNAME_LENGTH;
	}
	if(core_is_banned(state, user.addr)){
		return CORE_JOIN_BANNED;
	}
	
	// check if the user is already connected
	for(int i=0, c=array_size(state->user_array); i<c; i++){
		struct message_user* current = core_get_user(state, i);
		if(MESSAGE_EQ_ADDR(user.addr, current->addr)){
			// update the username
			if(strcmp(user.username, current->username) != 0){
				char* tmp = utils_copy_alloc(user.username);
				if(tmp == NULL){
					SCREAM("Error while updating username of the user '%s'", current->username);
					return CORE_JOIN_ERROR;
				}
				free(current->username);
				current->username = tmp;
			}
			return CORE_JOIN_OK;
		}
	}
	
	struct core_user_cell* cell = malloc(sizeof(*cell));
	if(cell == NULL){
		SCREAM("Failed to alloc a core_user_cell");
		return CORE_JOIN_ERROR;
	}
	cell->user.addr = user.addr;
	cell->user.username = utils_copy_alloc(user.username);
	cell->timeout = 0;
	if(!array_push(state->user_array, cell)){
		free(cell);
		SCREAM("Failed to insert the user '%s' in the user list", user.username);
		return CORE_JOIN_ERROR;
	}
	
	if(!core_emit_update(state)){
		SCREAM("Failed to notify of the new user");
	}
	
	return CORE_JOIN_OK;
}
bool core_user_leave(core_t state, struct message_addr addr){
	for(int i=0, c=array_size(state->user_array); i<c; i++){
		struct message_user* user = core_get_user(state, i);
		if(MESSAGE_EQ_ADDR(user->addr, addr)){
			if(!core_remove_user_list(state, i)){
				SCREAM("Failed to remove the user with TCP @ "MESSAGE_ADDR_FMT, MESSAGE_ADDR_FMT_PARAM(addr));
				return false;
			}
			core_emit_update(state);
			return true;
		}
	}
	return false;
}
bool core_is_banned(core_t state, struct message_addr addr){
	for(int i=0, c=array_size(state->ban_array); i<c; i++){
		struct message_user* current = array_get(state->ban_array, i);
		if(MESSAGE_EQ_ADDR(current->addr, addr)){
			return true;
		}
	}
	return false;
}
bool core_is_present(core_t state, struct message_addr addr){
	for(int i=0, c=array_size(state->user_array); i<c; i++){
		struct message_user* current = core_get_user(state, i);
		if(MESSAGE_EQ_ADDR(current->addr, addr)){
			return true;
		}
	}
	return false;
}

bool core_incr_timeout_legacy(core_t state, int max_timeout){
	bool change = false;
	for(int i=0; i<array_size(state->user_array); i++){
		struct core_user_cell* current = array_get(state->user_array, i);
		current->timeout++;
		if(current->timeout >= max_timeout){
			core_emit_kick(state, CORE_KICK_TIMEOUT, current->user);
			if(!core_remove_user_list(state, i)){
				SCREAM("Failed to remove user for timeout (name: %s)", current->user.username);
				continue;
			}
			i--;
			change = true;
		}
	}
	if(change){
		core_emit_update(state);
	}
	return change;
}
bool core_incr_timeout(core_t state){
	bool ok = true;
	bool change = false;
	for(int i=0; i<array_size(state->user_array); i++){
		struct core_user_cell* current = array_get(state->user_array, i);
		current->timeout++;
		if(current->timeout >= state->timeout_max){
			core_emit_kick(state, CORE_KICK_TIMEOUT, current->user);
			if(!core_remove_user_list(state, i)){
				SCREAM("Failed to kick user for timeout (name: %s)", current->user.username);
				ok = false;
			}
			i--;
			change = true;
		}
	}
	if(change){
		core_emit_update(state);
	}
	return ok;
}
bool core_reset_timeout(core_t state, struct message_addr addr){
	for(int i=0, c=array_size(state->user_array); i<c; i++){
		struct core_user_cell* current = array_get(state->user_array, i);
		if(MESSAGE_EQ_ADDR(current->user.addr, addr)){
			current->timeout = 0;
			return true;
		}
	}
	return false;
}

bool core_set_ban_list(core_t state, struct message_user const* users, int nb_user){
	SCREAM("Set ban list to %d", nb_user);
	bool ok = true;
	
	for(int i=0, c=array_size(state->ban_array); i<c; i++){
		struct message_user* user = array_get(state->ban_array, i);
		free(user->username);
	}
	array_clean(state->ban_array);
	for(int i=0; i<nb_user; i++){
		struct message_user* tmp = malloc(sizeof(*tmp));
		tmp->username = utils_copy_alloc(users[i].username);
		tmp->addr = users[i].addr;
		if(!array_push(state->ban_array, tmp)){
			free(tmp);
			SCREAM("Failed to push user '%s' on to the ban list", users[i].username);
			ok = false;
		}
	}
	SCREAM("Got %d banned !", array_size(state->ban_array));
	return ok;
}
bool core_add_ban(core_t state, struct message_user user){
	return core_add_ban_list(state, &user, 1);
}
bool core_add_ban_list(core_t state, struct message_user const* users, int nb_banned){
	SCREAM("Add %d banned", nb_banned);
	if(nb_banned <= 0){
		SCREAM("Attempt to extends the ban list with a negative amount of user");
		return true;
	}
	
	bool ok = true;
	
	// add the user
	for(int i=0; i<nb_banned; i++){
		bool already_in = false;
		// check if the user is already in
		for(int j=0, c=array_size(state->ban_array); j<c; j++){
			struct message_user* current = array_get(state->ban_array, j);
			if(MESSAGE_EQ_ADDR(current->addr, users[i].addr)){
				already_in = true;
				break;
			}
		}
		if(!already_in){
			struct message_user* tmp = message_user_copy(users[i]);
			if(tmp == NULL){
				SCREAM("Failed to copy user '%s'", users[i].username);
				ok = false;
				continue;
			}
			if(!array_push(state->ban_array, tmp)){
				message_user_free(tmp);
				SCREAM("Failed to push user '%s' on to the ban list", users[i].username);
				ok = false;
			}
		}
	}
	
	// kick banned user
	bool change = false;
	for(int i=0; i<nb_banned; i++){
		for(int j=0; j<array_size(state->user_array); j++){
			struct message_user* current = core_get_user(state, j);
			if(MESSAGE_EQ_ADDR(users[i].addr, current->addr)){
				core_emit_kick(state, CORE_KICK_BANNED, *current);
				if(!core_remove_user_list(state, j)){
					SCREAM("Failed to ban kicked user (name: %s)", current->username);
					ok = false;
				}
				change = true;
			}
		}
	}
	if(change){
		core_emit_update(state);
	}
	
	return ok;
}

int core_get_users(core_t state, struct message_user** users, int** timeout){
	if(users != NULL){
		struct message_user* tmp_user = malloc(sizeof(*tmp_user) * array_size(state->user_array));
		if(tmp_user == NULL){
			SCREAM("Unable to alloc a copy of the user list (size: %ld)", sizeof(*tmp_user) * array_size(state->user_array));
			return -1;
		}
		
		for(int i=0, c=array_size(state->user_array); i<c; i++){
			tmp_user[i] = *core_get_user(state, i);
		}
		*users = tmp_user;
	}
	if(timeout != NULL){
		int* tmp_timeout = malloc(sizeof(*tmp_timeout) * array_size(state->user_array));
		if(tmp_timeout == NULL){
			if(users != NULL){
				free(*users);
				*users = NULL;
			}
			SCREAM("Unable to alloc a copy of the user timeout list (size: %ld)", sizeof(*tmp_timeout) * array_size(state->user_array));
			return -1;
		}
		
		for(int i=0, c=array_size(state->user_array); i<c; i++){
			struct core_user_cell* cell = array_get(state->user_array, i);
			tmp_timeout[i] = cell->timeout;
		}
		*timeout = tmp_timeout;
	}
	return array_size(state->user_array);
}
int core_get_banned(core_t state, struct message_user** banned){
	if(banned != NULL){
		struct message_user* tmp = malloc(sizeof(*tmp) * array_size(state->ban_array));
		if(tmp == NULL){
			SCREAM("Failed to alloc a copy of the user ban list (size: %ld)", sizeof(*tmp) * array_size(state->ban_array));
			return -1;
		}
		
		for(int i=0, c=array_size(state->ban_array); i < c; i++){
			tmp[i] = *(struct message_user*)array_get(state->ban_array, i);
		}
		*banned = tmp;
	}
	return array_size(state->ban_array);
}


unsigned core_user_max(core_t state){
	return state->user_max;
}
bool core_set_user_max(core_t state, unsigned val){
	state->user_max = val;
	
	// kick last users
	if(val < array_size(state->user_array)){
		for(int i=array_size(state->user_array)-1; i>=val; i--){
			struct message_user* current = core_get_user(state, i);
			core_emit_kick(state, CORE_KICK_USER_COUNT, *current);
			if(!core_remove_user_list(state, i)){
				SCREAM("Failed to remove excess user (pos: %d | username: %s)", i, current->username);
				return false;
			}
		}
		core_emit_update(state);
	}
	
	return true;
}
unsigned core_username_length_max(core_t state){
	return state->username_length;
}
bool core_set_username_length_max(core_t state, unsigned val){
	state->username_length = val;
	
	// kick users with an username too long
	bool change = false;
	for(int i=0; i<array_size(state->user_array); i++){
		struct message_user* current = core_get_user(state, i);
		if(strlen(current->username) + 1 > val){
			core_emit_kick(state, CORE_KICK_USERNAME_LENGTH, *current);
			if(!core_remove_user_list(state, i)){
				SCREAM("Failed to remove the user with TCP @ "MESSAGE_ADDR_FMT, MESSAGE_ADDR_FMT_PARAM(current->addr));
				return false;
			}
			i--;
			change = true;
		}
	}
	if(change){
		core_emit_update(state);
	}
	
	return true;
}
unsigned core_timeout_max(core_t state){
	return state->timeout_max;
}
bool core_set_timeout_max(core_t state, unsigned val){
	state->timeout_max = val;
	
	bool ok = true;
	bool change = false;
	for(int i=0; i<array_size(state->user_array); i++){
		struct core_user_cell* current = array_get(state->user_array, i);
		if(current->timeout >= state->timeout_max){
			core_emit_kick(state, CORE_KICK_TIMEOUT, current->user);
			if(!core_remove_user_list(state, i)){
				SCREAM("Failed to kick user for timeout (name: %s)", current->user.username);
				ok = false;
			}
			i--;
			change = true;
		}
	}
	if(change){
		core_emit_update(state);
	}
	return ok;
}

_Bool core_register_handler_kick(core_t state, core_handler_user_kick kick, void* data, _Bool clean_up){
	return listener_register(state->listener_kick, (listener_handler)kick, data, clean_up);
}
_Bool core_unregister_handler_kick(core_t state, core_handler_user_kick kick){
	return listener_unregister(state->listener_kick, (listener_handler)kick);
}
void core_emit_kick(core_t state, enum core_kick_type type, struct message_user user){
	struct core_kick kick;
	kick.type = type;
	kick.user = user;
	listener_emit(state->listener_kick, &kick);
}

_Bool core_register_handler_update(core_t state, core_handler_user_list_update update, void* data, _Bool clean_up){
	return listener_register(state->listener_update, (listener_handler)update, data, clean_up);
}
_Bool core_unregister_handler_update(core_t state, core_handler_user_list_update update){
	return listener_unregister(state->listener_update, (listener_handler)update);
}
bool core_emit_update(core_t state){
	array_t tmp = array_init();
	if(tmp == NULL){
		SCREAM("Failed to init an array_t");
		return false;
	}
	for(int i=0, c=array_size(state->user_array); i<c; i++){
		struct message_user* current = core_get_user(state, i);
		struct message_user* tmp_user = message_user_copy(*current);
		if(tmp_user == NULL){
			array_free(tmp);
			SCREAM("Failed to copy an user");
			return false;
		}
		if(!array_push(tmp, tmp_user)){
			free(tmp_user);
			array_free(tmp);
			SCREAM("Failed to push user '%s' in tmp user list", current->username);
			return false;
		}
	}
	listener_emit(state->listener_update, tmp);
}

_Bool core_register_handler_illegal(core_t state, core_handler_illegal_op illegal, void* data, _Bool clean_up){
	return listener_register(state->listener_illegal, (listener_handler)illegal, data, clean_up);
}
_Bool core_unregister_handler_illegal(core_t state, core_handler_illegal_op illegal){
	return listener_unregister(state->listener_illegal, (listener_handler)illegal);
}
void core_emit_illegal(core_t state, struct message_addr addr){
	listener_emit(state->listener_illegal, &addr);
}

static struct message_user* core_get_user(core_t state, int pos){
	struct core_user_cell* data = array_get(state->user_array, pos);
	if(data == NULL){
		SCREAM("Failed to get user at pos %d", pos);
		return NULL;
	}
	return &data->user;
}

static bool core_remove_user_list(core_t state, int pos){
	struct message_user* user = core_get_user(state, pos);
	if(user == NULL){
		SCREAM("Failed to get the user at pos %d", pos);
		return false;
	}
	free(user->username);
	array_remove(state->user_array, pos);
}
