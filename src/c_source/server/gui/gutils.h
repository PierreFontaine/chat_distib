#ifndef PROJECT_GUI_H
#define PROJECT_GUI_H

#include "message.h"


void draw_layout();

void draw_command(int nb_cmd, char const* cmd[], int selected, _Bool focused);

void draw_list_user(int position, struct message_user user, int timeout);
void draw_list_users(int nb_user, struct message_user* users, int* timeouts, int offset);

void draw_select_user(int position, struct message_user user, _Bool selected, _Bool current);
void draw_select_users(int nb_user, struct message_user* users, _Bool selected[], int current, int offset);

#endif
