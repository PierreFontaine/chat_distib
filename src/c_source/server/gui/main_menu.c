#include "main_menu.h"

#include "gutils.h"

#include "terminal_lite.h"

void gui_main_draw(struct gui_main_args* extra){
	struct message_user* users;
	int* timeout;
	int nb_user = core_get_users(extra->state, &users, &timeout);
	term_reset_color();
	draw_list_users(nb_user, users, timeout, 0);
}