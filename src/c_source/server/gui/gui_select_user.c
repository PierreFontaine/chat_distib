#include "gui_select_user.h"

#include "gui_scroll.h"

#include "utils.h"
#include "terminal_lite.h"
#include "message.h"

#include <stdlib.h>
#include <stdbool.h>

struct gui_select_user{
	struct gui_scroll scroll;
	gui_select_user_fetch fetch;
	gui_select_user_commit commit;
	void* extra;
	bool* selected;
	struct message_user* users;
	int nb_user;
	bool def;
	bool open;
};

gui_select_user_t gui_select_user_init(void* extra, gui_select_user_fetch fetch, gui_select_user_commit commit, bool default_){
	gui_select_user_t res = malloc(sizeof(*res));
	if(res == NULL){
		SCREAM("Failed to alloc a gui_select_user");
		return NULL;
	}
	
	gui_scroll_reset(&res->scroll);
	res->fetch = fetch;
	res->commit = commit;
	res->extra = extra;
	res->selected = NULL;
	res->users = NULL;
	res->nb_user = -1;
	res->def = default_;
	res->open = false;
	
	return res;
}

bool gui_select_user_update(gui_select_user_t gui_select_user, struct keyboard_event event){
	if(!gui_select_user->open){
		if(!gui_select_user_load(gui_select_user)){
			SCREAM("Failed to load the user list");
			return false;
		}
		gui_scroll_set_nb_elt(&gui_select_user->scroll, (unsigned)gui_select_user->nb_user);
		gui_select_user->open = true;
	}
	unsigned current = gui_scroll_get_current(&gui_select_user->scroll);
	
	if(event.type == KEYBOARD_CMD){
		if(event.cmd == KEYBOARD_UP){
			gui_scroll_move(&gui_select_user->scroll, -1);
		}else if(event.cmd == KEYBOARD_DOWN){
			gui_scroll_move(&gui_select_user->scroll, 1);
		}else if(event.cmd == KEYBOARD_ENTER){
			if(!gui_select_user_store(gui_select_user)){
				SCREAM("Failed to commit the change on the user list");
			}
			gui_select_user->open = false;
			return false;
		}else if(event.cmd == KEYBOARD_RIGHT){
			gui_select_user->selected[current] = true;
		}else if(event.cmd == KEYBOARD_LEFT){
			gui_select_user->selected[current] = false;
		}
	}
	return true;
}
void gui_select_user_draw(gui_select_user_t gui_select_user){
	int height = term_height();
	if(height <= 4){
		return;
	}
	
	gui_scroll_set_display_size(&gui_select_user->scroll, (unsigned)height-4);
	unsigned current = gui_scroll_get_current(&gui_select_user->scroll);
	int pos = 3;
	for(unsigned i=gui_scroll_get_offset(&gui_select_user->scroll), c=gui_scroll_get_last_elt(&gui_select_user->scroll); i<=c; i++){
		if(current == i){
			if(gui_select_user->selected[i]){
				term_set_color_front(term_bright(TERM_YELLOW));
				term_move_write(2, pos, "<");
			}else{
				term_set_color_front(term_bright(TERM_BLACK));
				term_move_write(2, pos, ">");
			}
		}else{
			if(gui_select_user->selected[i]){
				term_set_color_front(TERM_YELLOW);
			}else{
				term_set_color_front(TERM_BLACK);
			}
		}
		
		term_move_write(4, pos, MESSAGE_ADDR_FMT": %s",
				MESSAGE_ADDR_FMT_PARAM(gui_select_user->users[i].addr), gui_select_user->users[i].username);
		pos++;
	}
}

static bool gui_select_user_load(gui_select_user_t gui_select_user){
	gui_select_user->nb_user = gui_select_user->fetch(gui_select_user->extra, &gui_select_user->users);
	if(gui_select_user->nb_user <= 0){
		if(gui_select_user->users != NULL){
			free(gui_select_user->users);
			gui_select_user->users = NULL;
		}
		SCREAM("Failed to fetch the user list (nb_user: %d)", gui_select_user->nb_user);
		gui_select_user->nb_user = -1;
		gui_select_user->users = NULL;
		return false;
	}
	
	gui_select_user->selected = malloc(sizeof(bool) * gui_select_user->nb_user);
	if(gui_select_user->selected == NULL){
		free(gui_select_user->users);
		SCREAM("Failed to alloc the boolean array");
		gui_select_user->nb_user = -1;
		gui_select_user->users = NULL;
		return false;
	}
	SCREAM("Start loading %d users", gui_select_user->nb_user);
	for(int i=0; i<gui_select_user->nb_user; i++){
		SCREAM("Loading user %s", gui_select_user->users[i].username);
		gui_select_user->selected[i] = gui_select_user->def;
	}
	return true;
}
static bool gui_select_user_store(gui_select_user_t gui_select_user){
	int nb_selected = 0;
	for(int i=0; i<gui_select_user->nb_user; i++){
		if(gui_select_user->selected[i]){
			nb_selected++;
		}
	}
	struct message_user* tmp = malloc(sizeof(*tmp) * nb_selected);
	int i=0;
	for(int j=0; j<gui_select_user->nb_user; j++){
		if(gui_select_user->selected[j]){
			tmp[i] = gui_select_user->users[j];
			i++;
		}
	}
	bool result = gui_select_user->commit(gui_select_user->extra, tmp, nb_selected);
	free(gui_select_user->users);
	free(tmp);
	return result;
}