#include "gui_scroll.h"

#include "utils.h"

#include <stdbool.h>

void gui_scroll_reset(gui_scroll_t gui_scroll){
	gui_scroll->nb_elt = 0;
	gui_scroll->display_size = 1;
	gui_scroll->offset = 0;
	gui_scroll->last_elt = 0;
	gui_scroll->dirty = false;
}

void gui_scroll_move(gui_scroll_t gui_scroll, int pos){
	if(gui_scroll->dirty){
		gui_scroll_recompute_pos(gui_scroll);
	}
	
	if(pos < 0){
		// all the way up already
		if(gui_scroll->current == 0){
			return;
		}
		
		// the offset is unsigned, so risk of overflow
		if(gui_scroll->current < -pos){
			gui_scroll->current = 0;
		}else{
			gui_scroll->current += pos;
		}
	}else{
		// al the way down already
		if(gui_scroll->last_elt == gui_scroll->current){
			return;
		}
		
		gui_scroll->current += pos;
	}
	gui_scroll->dirty = true;
}
void gui_scroll_set_nb_elt(gui_scroll_t gui_scroll, unsigned nb_elt){
	if(nb_elt != gui_scroll->nb_elt){
		gui_scroll->nb_elt = nb_elt;
		gui_scroll->dirty = true;
	}
}
void gui_scroll_set_display_size(gui_scroll_t gui_scroll, unsigned display_size){
	if(display_size != gui_scroll->display_size){
		gui_scroll->display_size = display_size;
		gui_scroll->dirty = true;
	}
}

unsigned gui_scroll_get_current(gui_scroll_t gui_scroll){
	if(gui_scroll->dirty){
		gui_scroll_recompute_pos(gui_scroll);
	}
	return gui_scroll->current;
}
unsigned gui_scroll_get_offset(gui_scroll_t gui_scroll){
	if(gui_scroll->dirty){
		gui_scroll_recompute_pos(gui_scroll);
	}
	return gui_scroll->offset;
}
unsigned gui_scroll_get_last_elt(gui_scroll_t gui_scroll){
	if(gui_scroll->dirty){
		gui_scroll_recompute_pos(gui_scroll);
	}
	return gui_scroll->last_elt;
}

static void gui_scroll_recompute_pos(gui_scroll_t gui_scroll){
	gui_scroll->dirty = false;
	
	// current out of list
	if(gui_scroll->current >= gui_scroll->nb_elt){
		gui_scroll->current = gui_scroll->nb_elt - 1;
	}
	
	// current out of display
	if(gui_scroll->current < gui_scroll->offset){
		gui_scroll->offset = gui_scroll->current;
	}else if(gui_scroll->current > gui_scroll->last_elt){
		gui_scroll->offset = gui_scroll->current - gui_scroll->display_size;
	}
	
	// not enough element to fill the screen => display all
	if(gui_scroll->display_size >= gui_scroll->nb_elt){
		gui_scroll->offset = 0;
		gui_scroll->last_elt = gui_scroll->nb_elt-1;
		return;
	}
	
	// scroll back up to not leave a blank after the list
	if(gui_scroll->offset + gui_scroll->display_size >= gui_scroll->nb_elt){
		gui_scroll->offset = gui_scroll->nb_elt - gui_scroll->display_size - 1;
		gui_scroll->last_elt = gui_scroll->nb_elt - 1;
		return;
	}
	
	// adjust last_elt
	gui_scroll->last_elt = gui_scroll->offset + gui_scroll->display_size;
}