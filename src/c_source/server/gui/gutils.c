#include "gutils.h"
#include "../config.h"

#include "terminal_lite.h"
#include "utils.h"

#include <string.h>


void draw_layout(){
	term_write_middle(1, "Server v0.0.0");
	
	int height = term_height();
	term_draw_line(height-1);
	term_move_write(2, height-1, "Command");
}

void draw_command(int nb_cmd, char const* cmd[], int selected, _Bool focused){
	int pos = 1;
	int height = term_height();
	
	for(int i=0; i<nb_cmd; i++){
		if(i == selected){
			if(focused){
				term_set_color_front(TERM_WHITE);
			}else{
				term_set_color_front(term_bright(TERM_BLACK));
			}
			term_move(pos, height);
			printf("[%s]", cmd[i]);
		}else{
			term_set_color_front(TERM_BLACK);
			term_move_write(pos+1, height, cmd[i]);
		}
		pos += strlen(cmd[i]) + 3;
	}
	term_reset_color();
}

void draw_user(int position, struct message_user user){
	term_move_write(5, position, "%d.%d.%d.%d:%d",
			user.addr.ip[0], user.addr.ip[1], user.addr.ip[2], user.addr.ip[3], user.addr.port);
	term_move_write(27, position, user.username);
}

void draw_list_user(int position, struct message_user user, int timeout){
	struct config_raw conf = config_get_legacy();
	int val = timeout * 100 / conf.timeout_max;
	if(val <= 50){
		term_set_color_front(TERM_WHITE);
	}else if(val <= 80){
		term_set_color_front(TERM_YELLOW);
	}else{
		term_set_color_front(TERM_RED);
	}
	term_move_write(1, position, "(%d)", timeout);
	term_reset_color();
	
	draw_user(position, user);
}
void draw_list_users(int nb_user, struct message_user* users, int* timeouts, int offset){
	int height = term_height() - 4;
	int nb_user_displayed = height < nb_user - offset ? height : nb_user - offset;
	term_draw_line(2);
	term_move_write(2, 2, "Users (%d-%d/%d)", nb_user == 0 ? 0 : offset+1, nb_user_displayed + offset, nb_user);
	for(int i=0; i<nb_user_displayed; i++){
		draw_list_user(i + 3, users[i + offset], timeouts[i + offset]);
	}
}

void draw_select_user(int position, struct message_user user, _Bool selected, _Bool current){
	if(current){
		if(selected){
			term_move_write(2, position, "<");
			term_set_color_front(term_bright(TERM_YELLOW));
		}else{
			term_move_write(2, position, ">");
			term_set_color_front(term_bright(TERM_BLACK));
		}
	}else{
		if(selected){
			term_set_color_front(TERM_YELLOW);
		}else{
			term_set_color_front(TERM_BLACK);
		}
	}
	draw_user(position, user);
	term_reset_color();
}
void draw_select_users(int nb_user, struct message_user* users, _Bool selected[], int current, int offset){
	int height = term_height() - 4;
	int nb_user_displayed = height < nb_user - offset ? height : nb_user - offset;
	term_draw_line(2);
	term_move_write(2, 2, "Users (%d-%d/%d)", offset+1, nb_user_displayed + offset, nb_user);
	for(int i=0; i<nb_user_displayed; i++){
		draw_select_user(i + 3, users[i + offset], selected[i], current == i);
	}
}