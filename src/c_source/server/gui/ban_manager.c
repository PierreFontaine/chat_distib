#include "ban_manager.h"

#include "gutils.h"

#include <stdbool.h>
#include <stdlib.h>

static bool is_open = false;
static struct message_user* user = NULL;
static bool* selected = NULL;
static int nb_user = 0;


bool gui_ban_update(struct menu_sub* sub, struct keyboard_event event){
	if(!is_open){
		if(((struct gui_ban_extra*)sub->extra)->ban){
			nb_user = core_get_users(((struct gui_ban_extra*)sub->extra)->state, &user, NULL);
		}else{
			nb_user = core_get_banned(((struct gui_ban_extra*)sub->extra)->state, &user);
		}
		if(nb_user < 1){ // exit if error or no users
			return false;
		}
		selected = malloc(sizeof(bool) * nb_user);
		if(selected == NULL){
			free(user);
			return false;
		}
		_Bool def_val = !((struct gui_ban_extra*)sub->extra)->ban;
		for(int i=0; i<nb_user; i++){
			selected[i] = def_val;
		}
		is_open = true;
	}
	if(menu_move_cursor(sub, event, nb_user)){
		// NO OP
	}else if(event.type == KEYBOARD_CMD){
		switch(event.cmd){
			case KEYBOARD_RIGHT:{
				selected[sub->current] = true;
				break;
			}
			case KEYBOARD_LEFT:{
				selected[sub->current] = false;
				break;
			}
			case KEYBOARD_ENTER:{
				int nb_selected = 0;
				for(int i=0; i<nb_user; i++){
					if(selected[i]){
						nb_selected++;
					}
				}
				struct message_user* tmp;
				tmp = malloc(sizeof(selected) * nb_selected);
				for(int i=0, j=0; i<nb_user; i++){
					if(selected[i]){
						tmp[j++] = user[i];
					}
				}
				free(selected);
				free(user);
				selected = NULL;
				user = NULL;
				is_open = false;
				if(((struct gui_ban_extra*)sub->extra)->ban){
					core_add_ban_list(((struct gui_ban_extra*)sub->extra)->state, tmp, nb_selected);
				}else{
					core_set_ban_list(((struct gui_ban_extra*)sub->extra)->state, tmp, nb_selected);
				}
				return false;
			}
			default: break;
		}
	}
	return true;
}
void gui_ban_draw(struct menu_sub const* sub){
	draw_layout();
	draw_select_users(nb_user, user, selected, sub->current, sub->offset);
}