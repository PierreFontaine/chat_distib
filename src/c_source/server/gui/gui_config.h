#ifndef PROJECT_GUI_CONFIG_NAME_H
#define PROJECT_GUI_CONFIG_NAME_H

#include "../config.h"

#include "keyboard.h"

typedef struct gui_config* gui_config_t;

gui_config_t gui_config_init(config_t config);
// no gui_config_free, the object is cleanable with a free

_Bool gui_config_update(gui_config_t gui_config, struct keyboard_event event);
void gui_config_draw(gui_config_t gui_config);

static void gui_config_load_values(gui_config_t gui_config);
static void gui_config_store_values(gui_config_t gui_config);

#endif
