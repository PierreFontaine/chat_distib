#ifndef PROJECT_GUI_SCROLL_H
#define PROJECT_GUI_SCROLL_H

typedef struct gui_scroll* gui_scroll_t;

/**
 * A util handling scroll position.
 */
struct gui_scroll{
	unsigned nb_elt;
	unsigned display_size;
	unsigned current;
	unsigned offset;
	unsigned last_elt;
	_Bool dirty;
};

void gui_scroll_reset(gui_scroll_t gui_scroll);

/**
 * Move the
 * @param gui_scroll The object to affect
 * @param pos How many line to move
 * @return Whether the position of the cursor changed
 */
void gui_scroll_move(gui_scroll_t gui_scroll, int pos);
void gui_scroll_set_nb_elt(gui_scroll_t gui_scroll, unsigned nb_elt);
void gui_scroll_set_display_size(gui_scroll_t gui_scroll, unsigned display_size);

unsigned gui_scroll_get_current(gui_scroll_t gui_scroll);
unsigned gui_scroll_get_offset(gui_scroll_t gui_scroll);
unsigned gui_scroll_get_last_elt(gui_scroll_t gui_scroll);

static void gui_scroll_recompute_pos(gui_scroll_t gui_scroll);

#endif
