#ifndef PROJECT_GUI_SELECT_USER_H
#define PROJECT_GUI_SELECT_USER_H

#include "message.h"
#include "keyboard.h"

typedef struct gui_select_user* gui_select_user_t;

typedef int (*gui_select_user_fetch)(void* extra, struct message_user** user_list);
typedef _Bool (*gui_select_user_commit)(void* extra, struct message_user const* user_list, int nb_user);

gui_select_user_t gui_select_user_init(void* extra, gui_select_user_fetch fetch, gui_select_user_commit commit, _Bool default_);
// no gui_user_list_free, use free instead

_Bool gui_select_user_update(gui_select_user_t gui_select_user, struct keyboard_event event);
void gui_select_user_draw(gui_select_user_t gui_select_user);

static _Bool gui_select_user_load(gui_select_user_t gui_select_user);
static _Bool gui_select_user_store(gui_select_user_t gui_select_user);

#endif
