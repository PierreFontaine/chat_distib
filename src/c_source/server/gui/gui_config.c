#include "gui_config.h"

#include "gui_scroll.h"

#include "utils.h"
#include "terminal_lite.h"

#include <stdlib.h>
#include <stdbool.h>

struct gui_config{
	struct gui_scroll scroll;
	config_t config;
	unsigned val[CONFIG_ENTRY_COUNT];
	_Bool open;
};

gui_config_t gui_config_init(config_t config){
	gui_config_t res = malloc(sizeof(*res));
	if(res == NULL){
		SCREAM("Failed to alloc a gui_config_t");
		return NULL;
	}
	gui_scroll_reset(&res->scroll);
	gui_scroll_set_nb_elt(&res->scroll, CONFIG_ENTRY_COUNT);
	res->config = config;
	res->open = false;
}

bool gui_config_update(gui_config_t gui_config, struct keyboard_event event){
	// the gui was just open
	if(!gui_config->open){
		gui_config_load_values(gui_config);
		gui_config->open = true;
	}
	
	unsigned current = gui_scroll_get_current(&gui_config->scroll);
	if(event.type == KEYBOARD_CMD){
		// move cursor
		if(event.cmd == KEYBOARD_UP){
			gui_scroll_move(&gui_config->scroll, -1);
		}else if(event.cmd == KEYBOARD_DOWN){
			gui_scroll_move(&gui_config->scroll, 1);
		// exit menu
		}else if(event.cmd == KEYBOARD_ENTER){
			gui_config_store_values(gui_config);
			gui_config->open = false;
			return false;
		// incr/decr value
		}else if(event.cmd == KEYBOARD_LEFT){
			gui_config->val[current]--;
		}else if(event.cmd == KEYBOARD_RIGHT){
			gui_config->val[current]++;
		}else if(event.cmd == KEYBOARD_DEL){
			gui_config->val[current] /= 10;
		}
	}else{
		if(event.key >= '0' && event.key <= '9'){
			gui_config->val[current] = gui_config->val[current] * 10 + event.key - '0';
		}
	}
	return true;
}
void gui_config_draw(gui_config_t gui_config){
	// screen too small
	if(term_height() <= 4){
		return;
	}
	
	gui_scroll_set_display_size(&gui_config->scroll, (unsigned)term_height()-4);
	unsigned current = gui_scroll_get_current(&gui_config->scroll);
	int pos = 3;
	for(unsigned i=gui_scroll_get_offset(&gui_config->scroll), c=gui_scroll_get_last_elt(&gui_config->scroll); i<=c; i++){
		if(current == i){
			term_set_color_front(term_bright(TERM_WHITE));
			term_move_write(2, pos, "#");
		}else{
			term_set_color_front(term_bright(TERM_BLACK));
		}
		
		term_move_write(4, pos, "%20s: %u", confing_entry_names[i], gui_config->val[i]);
		pos++;
	}
}

static void gui_config_load_values(gui_config_t gui_config){
	for(int i=0; i<CONFIG_ENTRY_COUNT; i++){
		gui_config->val[i] = config_get(gui_config->config, (enum config_entry)i);
	}
}
static void gui_config_store_values(gui_config_t gui_config){
	for(int i=0; i<CONFIG_ENTRY_COUNT; i++){
		SCREAM("Iter %d", i);
		if(!config_set(gui_config->config, (enum config_entry)i, gui_config->val[i])){
			SCREAM("Failed to set config entry %d to %u", i, gui_config->val[i]);
		}
	}
}