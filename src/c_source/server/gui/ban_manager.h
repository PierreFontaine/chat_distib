#ifndef PROJECT_BAN_MANAGER_H
#define PROJECT_BAN_MANAGER_H

#include "keyboard.h"
#include "message.h"

#include "../menu.h"
#include "../core.h"

// extra used to store a gui_ban_extra

struct gui_ban_extra {
	struct core_state* state;
	_Bool ban;
};

_Bool gui_ban_update(struct menu_sub* sub, struct keyboard_event event);
void gui_ban_draw(struct menu_sub const* sub);

#endif
