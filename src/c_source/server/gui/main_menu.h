#ifndef PROJECT_MAIN_MENU_H
#define PROJECT_MAIN_MENU_H

#include "../core.h"

struct gui_main_args {
	struct core_state* state;
};

void gui_main_draw(struct gui_main_args* extra);

#endif
