#ifndef PROJECT_GUI_LOGIC_H
#define PROJECT_GUI_LOGIC_H

#include "keyboard.h"

/**
 * A function updating a submenu.
 * param data - The data provided by the constructor
 * param event - The keyboard event that triggered the update
 * return Whether the menu is open
 */
typedef _Bool (*menu_handler_update)(void* extra, struct keyboard_event event);
typedef void (*menu_handler_draw)(void* extra);

typedef struct menu* menu_t;

struct menu* menu_init(menu_handler_draw draw, void* extra, _Bool free_extra);
_Bool menu_add_sub(struct menu* menu, char const* name, menu_handler_update update, menu_handler_draw draw, void* extra, _Bool free_extra);
void menu_free(struct menu* menu);

_Bool menu_update(struct menu* menu, struct keyboard_event event);
void menu_draw(struct menu* menu);
_Bool menu_running(struct menu* menu);

#endif
