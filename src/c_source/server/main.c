#include "gui/gutils.h"
#include "gui/main_menu.h"
#include "gui/gui_select_user.h"
#include "config.h"
#include "adapter_server.h"
#include "gui/gui_config.h"
#include "menu.h"

#include "adapter.h"
#include "udp.h"
#include "sync.h"
#include "utils.h"
#include "terminal_lite.h"

#include <stdlib.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <pthread.h>
#include <string.h>

static const struct utils_option main_help = {
		.name = "help",
		.help = "display this help and exit",
		.param = NULL,
		.sh_opt = 'h',
};
static const struct utils_option main_ttl = {
		.name = "udp-ttl",
		.help = "the ttl of message sent through the UDP multicast",
		.param = "TTL",
		.sh_opt = 't',
};
static const struct utils_option main_timeout_max= {
		.name = "timeout-max",
		.help = "The number of timeout before an user get kick",
		.param = "TIMEOUT_MAX",
		.sh_opt = 'm',
};
static const struct utils_option main_timeout_freq = {
		.name = "timeout-freq",
		.help = "The frequency between 2 pong in ms",
		.param = "TIMEOUT_FREQ",
		.sh_opt = 'f',
};
static const struct utils_option main_user_max = {
		.name = "user-max",
		.help = "The maximum number of user the server will accept at once",
		.param = "USER_MAX",
		.sh_opt = 'u',
};
static const struct utils_option main_username_length_max = {
		.name = "username-length-max",
		.help = "The maximum length of an username in byte",
		.param = "LENGTH",
		.sh_opt = 'l',
};
static const struct utils_option main_port = {
		.name = "port",
		.help = "The port the server will use",
		.param = "PORT",
		.sh_opt = 'p',
};
static const struct utils_option main_address = {
		.name = "address",
		.help = "The address of the multicast to connect to",
		.param = "ADDRESS",
		.sh_opt = 'a',
};

bool main_parse_arg(int argc, char** argv, char** address, int* port, config_t config){
	struct utils_option option[8];
	struct utils_task task[8];
	
	unsigned ttl = config_get(config, CONFIG_MULTICAST_TTL);
	unsigned timeout_max = config_get(config, CONFIG_TIMEOUT_MAX);
	unsigned timeout_freq = config_get(config, CONFIG_TIMEOUT_FREQ);
	unsigned user_max = config_get(config, CONFIG_USER_COUNT_MAX);
	unsigned username_length = config_get(config, CONFIG_USERNAME_LENGTH_MAX);
	int help = 0;
	
	utils_common_setup_option(0, main_help, UTILS_INCR_INT, &help, option, task);
	utils_common_setup_option(1, main_ttl, UTILS_SET_UINT8, &ttl, option, task);
	utils_common_setup_option(2, main_timeout_max, UTILS_SET_INT, &timeout_max, option, task);
	utils_common_setup_option(3, main_timeout_freq, UTILS_SET_INT, &timeout_freq, option, task);
	utils_common_setup_option(4, main_user_max, UTILS_SET_INT, &user_max, option, task);
	utils_common_setup_option(5, main_username_length_max, UTILS_SET_INT, &username_length, option, task);
	utils_common_setup_option(6, main_port, UTILS_SET_INT, port, option, task);
	utils_common_setup_option(7, main_address, UTILS_SET_STR, address, option, task);
	
	if(help > 0){
		utils_print_option("...", option, 8);
	}
	
	config_set(config, CONFIG_MULTICAST_TTL, ttl);
	config_set(config, CONFIG_TIMEOUT_MAX, timeout_max);
	config_set(config, CONFIG_TIMEOUT_FREQ, timeout_freq);
	config_set(config, CONFIG_USER_COUNT_MAX, user_max);
	config_set(config, CONFIG_USERNAME_LENGTH_MAX, username_length);
	
	return utils_parse_args(argc, argv, task, 8);
}
/*
struct timespec main_fetch_delay(void* null){
	struct config_raw conf = config_get_legacy();
	struct timespec res = {
			.tv_nsec = (conf.timeout_freq % 1000) * 1000,
			.tv_sec = conf.timeout_freq / 1000
	};
	return res;
}
bool main_start_timer(struct sync* sync_obj, pthread_t* thread){
	struct tim_args* args = malloc(sizeof(*args));
	if(args == NULL){
		return false;
	}
	
	args->sync_obj = sync_obj;
	args->sync = (_Bool (*)(void*, struct timespec))sync_timer_adapter;
	args->delay_obj = NULL;
	args->delay = main_fetch_delay;
	
	if(pthread_create(thread, NULL, (void* (*)(void*))tim_process, args) != 0){
		free(args);
		return false;
	}
	return true;
}

bool main_filter_udp(void* null, struct message message){
	SCREAM("Message of type: %d received => %d", message.type, message_for_server(message));
	return message_for_server(message);
}
bool main_start_udp(struct sync* sync_obj, struct udp_obj_legacy* udp_obj, pthread_t* thread){
	struct udp_args_legacy* args = malloc(sizeof(struct udp_args_legacy));
	if(args == NULL){
		return false;
	}
	
	args->state = udp_obj;
	args->sync_obj = sync_obj;
	args->sync = (_Bool (*)(void*, struct message))sync_udp_adapter;
	args->filter_obj = NULL;
	args->filter = main_filter_udp;
	
	if(pthread_create(thread, NULL, (void* (*)(void*))udp_process_legacy, args) != 0){
		free(args);
		return false;
	}
	return true;
}
bool main_start_keyboard(struct sync* sync_obj, pthread_t* thread){
	struct keyboard_args* args = malloc(sizeof(struct keyboard_args));
	args->sync_obj = sync_obj;
	args->sync = (_Bool (*)(void*, struct keyboard_event))sync_keyboard_adapter;
	if(pthread_create(thread, NULL, (void* (*)(void*))keyboard_process, args) != 0){
		return false;
	}
	return true;
}
*/
struct menu* main_gen_menu(core_t state, udp_t udp_obj, config_t config){
	// create args
	struct gui_main_args* args = malloc(sizeof(struct gui_main_args));
	if(args == NULL){
		return NULL;
	}
	args->state = state;
	
	// create menu
	struct menu* menu = menu_init((menu_handler_draw)gui_main_draw, args, true);
	if(menu == NULL){
		free(args);
		return NULL;
	}
	
	// create sub menu 1
	// -> args
	gui_select_user_t args1 = gui_select_user_init(state, (gui_select_user_fetch)adapter_server_get_user, (gui_select_user_commit)core_add_ban_list, false);
	if(args1 == NULL){
		menu_free(menu);
		return NULL;
	}
	
	// -> sub
	if(!menu_add_sub(menu, "ban", (menu_handler_update)gui_select_user_update, (menu_handler_draw)gui_select_user_draw, args1, true)){
		free(args1);
		menu_free(menu);
		return NULL;
	}
	
	// create sub menu 2
	// -> args
	gui_select_user_t args2 = gui_select_user_init(state, (gui_select_user_fetch)core_get_banned, (gui_select_user_commit)core_set_ban_list, true);
	if(args2 == NULL){
		menu_free(menu);
		return NULL;
	}
	// -> sub
	if(!menu_add_sub(menu, "unban", (menu_handler_update)gui_select_user_update, (menu_handler_draw)gui_select_user_draw, args2, true)){
		menu_free(menu);
		return NULL;
	}
	
	// create sub menu 3
	// -> arg
	gui_config_t args3 = gui_config_init(config);
	// -> sub
	if(!menu_add_sub(menu, "config", (menu_handler_update)gui_config_update, (menu_handler_draw)gui_config_draw, args3, true)){
		menu_free(menu);
		return NULL;
	}
	
	return menu;
}

int main(int argc, char **argv){
	int port = 6000;
	char* address = "239.0.1.1";
	
	pthread_t thr_timer;
	pthread_t thr_keyboard;
	pthread_t thr_udp;
	
	config_t config = config_init();
	if(config == NULL){
		SCREAM("Failed to alloc config");
		return EXIT_FAILURE;
	}
	
	printf("Parsing arguments\n");
	// ARG PARSE
	if(!main_parse_arg(argc, argv, &address, &port, config)){
		SCREAM("Error while parsing args");
		return EXIT_FAILURE;
	}
	
	// STRUCT INIT
	printf("Creating core\n");
	core_t state = core_init();
	if(state == NULL){
		SCREAM("Error while starting the core");
		return EXIT_FAILURE;
	}
	printf("Done @ %p\n", state);
	
	printf("Creating sync object\n");
	sync_t sync_obj = sync_init();
	if(sync_obj == NULL){
		core_free(state);
		SCREAM("Error while initializing sync");
		return EXIT_FAILURE;
	}
	printf("Done @ %p\n", sync_obj);
	
	printf("Creating udp object\n");
	udp_t udp_obj = udp_init(inet_addr(address), (uint16_t)port);
	if(udp_obj == NULL){
		sync_free(sync_obj);
		core_free(state);
		SCREAM("Error while initializing udp");
		return EXIT_FAILURE;
	}
	printf("Done @ %p\n", udp_obj);
	
	printf("Creating udp object\n");
	tim_t tim = tim_init();
	if(tim == NULL){
		udp_free(udp_obj);
		sync_free(sync_obj);
		core_free(state);
		SCREAM("Error while initializing udp");
		return EXIT_FAILURE;
	}
	printf("Done @ %p\n", udp_obj);
	
	printf("Creating menu\n");
	struct menu* menu = main_gen_menu(state, udp_obj, config);
	if(menu == NULL){
		tim_free(tim);
		udp_free(udp_obj);
		sync_free(sync_obj);
		core_free(state);
		SCREAM("Error while initializing state");
		return EXIT_FAILURE;
	}
	printf("Done @ %p\n", menu);
	
	// SETUP LISTENER
	
	config_register(config, CONFIG_USER_COUNT_MAX, (config_handler)adapter_server_user_count, state, false);
	config_register(config, CONFIG_USERNAME_LENGTH_MAX, (config_handler)adapter_server_username_length, state, false);
	config_register(config, CONFIG_TIMEOUT_MAX, (config_handler)adapter_server_timeout_max, state, false);
	config_register(config, CONFIG_MULTICAST_TTL, (config_handler)adapter_server_ttl, udp_obj, false);
	config_register(config, CONFIG_TIMEOUT_FREQ, (config_handler)adapter_server_timeout_freq, tim, false);
	
	core_register_handler_kick(state, (core_handler_user_kick)adapter_server_kick, udp_obj, false);
	core_register_handler_update(state, (core_handler_user_list_update)adapter_server_update, udp_obj, false);
	core_register_handler_illegal(state, (core_handler_illegal_op)adapter_server_illegal, udp_obj, false);
	
	// load config
	{
		udp_change_ttl(udp_obj, (uint8_t)config_get(config, CONFIG_MULTICAST_TTL));
		core_set_timeout_max(state, config_get(config, CONFIG_TIMEOUT_MAX));
		unsigned time_freq = config_get(config, CONFIG_TIMEOUT_FREQ);
		struct timespec spec = {
				.tv_sec = time_freq/1000,
				.tv_nsec = (time_freq * 1000) % 1000,
		};
		tim_set_delay(tim, spec);
		core_set_user_max(state, config_get(config, CONFIG_USER_COUNT_MAX));
		core_set_username_length_max(state, config_get(config, CONFIG_USERNAME_LENGTH_MAX));
	}
	
	printf("Setup terminal\n");
	struct termios t = keyboard_setup();
	
	// SYNC CONNECTION
	printf("Linking sync w/ timer\n");
	if(!adapter_lauch_timer(sync_obj, tim, &thr_timer)){
		keyboard_reset(t);
		udp_free(udp_obj);
		sync_free(sync_obj);
		SCREAM("Error while starting the timer");
		return EXIT_FAILURE;
	}
	printf("Linking sync w/ udp_obj_legacy\n");
	if(!adapter_lauch_udp(sync_obj, udp_obj, &thr_udp)){
		keyboard_reset(t);
		udp_free(udp_obj);
		sync_free(sync_obj);
		SCREAM("Error while starting the udp");
		return EXIT_FAILURE;
	}
	printf("Linking sync w/ keyboard\n");
	if(!adapter_lauch_keyboard(sync_obj, &thr_keyboard)){
		keyboard_reset(t);
		udp_free(udp_obj);
		sync_free(sync_obj);
		SCREAM("Error while starting the keyboard");
		return EXIT_FAILURE;
	}
	
	// fixme changing logic behind user list message sending.
	// MAIN LOOP
	printf("Main loop started !\n");
	while(menu_running(menu)){
		// draw
		menu_draw(menu);
		fflush(stdout);
		
		// update
		struct sync_event event = sync_poll(sync_obj);
		switch(event.type){
			case SYNC_KEYBOARD:{
				menu_update(menu, event.keyboard);
				break;
			}
			case SYNC_UDP:{
				adapter_server_process_message(state, udp_obj, event.udp);
				break;
			}
			case SYNC_TIMER:{
				if(core_get_users(state, NULL, NULL) != 0){
					core_incr_timeout(state);
					if(core_get_users(state, NULL, NULL) != 0){
						udp_send(udp_obj, message_get_ping());
					}
				}
				break;
			}
		}
		
		// The code inspector think there is a chance
		if(event.type != 42)
			if(event.type == 42)
				break;
	}
	
	// WAITING
	sync_close(sync_obj);
	
	udp_send(udp_obj, message_get_closing());
	
	pthread_join(thr_udp, NULL);
	pthread_join(thr_timer, NULL);
	printf("Please print some char to unlock the keyboard reader\n");
	pthread_join(thr_keyboard, NULL);
	
	// CLEANING UP
	keyboard_reset(t);
	udp_free(udp_obj);
	sync_free(sync_obj);
	config_free(config);
	
	term_move(1, 1);
	term_clear();
	
	return EXIT_SUCCESS;
}