#ifndef PROJECT_CONFIG_H
#define PROJECT_CONFIG_H

#include <stdint.h>

#define CONFIG_ENTRY_COUNT 5
enum config_entry {
	CONFIG_TIMEOUT_MAX,
	CONFIG_TIMEOUT_FREQ,
	CONFIG_MULTICAST_TTL,
	CONFIG_USER_COUNT_MAX,
	CONFIG_USERNAME_LENGTH_MAX,
};
extern const char* confing_entry_names[];

struct config_change_event {
	enum config_entry entry;
	unsigned value;
};

typedef struct config* config_t;
typedef void (*config_handler)(void* data, struct config_change_event* event);

config_t config_init();
_Bool config_free(config_t config);

unsigned config_get(config_t config, enum config_entry entry);
_Bool config_set(config_t config, enum config_entry entry, unsigned val);
_Bool config_register(config_t config, enum config_entry entry, config_handler handler, void* data, _Bool clean_up);
_Bool config_unregister(config_t config, enum config_entry entry, config_handler handler);


struct config_raw{
	int timeout_max;
	int timeout_freq;
	uint8_t ttf;
	int user_max;
	int username_length_max;
};

struct config_raw config_get_legacy();
void config_set_legacy(struct config_raw config);

#endif
