#include "config.h"

#include "listener.h"
#include "utils.h"

#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>

struct config_cell {
	listener_t listener;
	unsigned val;
	unsigned max;
	unsigned min;
};

const char* confing_entry_names[] = {
		"TIMEOUT_MAX",
		"TIMEOUT_FREQ",
		"MULTICAST_TTL",
		"USER_COUNT_MAX",
		"USERNAME_LENGTH_MAX",
};

static const struct config_cell CONFIG_DEFAULT[] = {
		{ // timeout max
				.val = 5,
				.min = 0,
				.max = UINT_MAX,
		},
		{ // timeout freq
				.val = 5000,
				.min = 0,
				.max = UINT_MAX,
		},
		{ // multicast's ttl
				.val = 10,
				.min = 0,
				.max = 255,
		},
		{ // user max
				.val = 100,
				.min = 0,
				.max = UINT_MAX,
		},
		{ // username length max
				.val = 10,
				.min = 0,
				.max = UINT_MAX,
		}
};

struct config {
	struct config_cell lst_cell[CONFIG_ENTRY_COUNT];
};

config_t config_init(){
	config_t res = malloc(sizeof(*res));
	if(res == NULL){
		return NULL;
	}
	for(int i=0; i<CONFIG_ENTRY_COUNT; i++){
		res->lst_cell[i] = CONFIG_DEFAULT[i];
		res->lst_cell[i].listener = listener_init();
		if(res->lst_cell[i].listener == NULL){
			for(int j=0; j < i; j++){
				listener_free(res->lst_cell[j].listener);
			}
			free(res);
			return NULL;
		}
	}
	return res;
}
bool config_free(config_t config){
	if(config == NULL){
		return false;
	}
	bool res = true;
	for(int i=0; i<CONFIG_ENTRY_COUNT; i++){
		res = listener_free(config->lst_cell[i].listener) && res;
	}
	free(config);
	return res;
}

unsigned config_get(config_t config, enum config_entry entry){
	if(entry < 0 || entry >= CONFIG_ENTRY_COUNT){
		SCREAM("Invalid value of enum config_entry given: %d", entry);
		return 0;
	}
	
	return config->lst_cell[entry].val;
}
bool config_set(config_t config, enum config_entry entry, unsigned val){
	if(entry < 0 || entry >= CONFIG_ENTRY_COUNT){
		SCREAM("Invalid value of enum config_entry given: %d", entry);
		return false;
	}
	
	if(val < config->lst_cell[entry].min){
		config->lst_cell[entry].val = config->lst_cell[entry].min;
	}else if(val > config->lst_cell[entry].max){
		config->lst_cell[entry].val = config->lst_cell[entry].max;
	}else{
		config->lst_cell[entry].val = val;
	}
	struct config_change_event event;
	event.value = val;
	event.entry = entry;
	listener_emit(config->lst_cell[entry].listener, &event);
	return true;
}
bool config_register(config_t config, enum config_entry entry, config_handler handler, void* data, bool clean_up){
	if(entry < 0 || entry >= CONFIG_ENTRY_COUNT){
		SCREAM("Invalid value of enum config_entry given: %d", entry);
		return false;
	}
	
	return listener_register(config->lst_cell[entry].listener, (listener_handler)handler, data, clean_up);
}
bool config_unregister(config_t config, enum config_entry entry, config_handler handler){
	if(entry < 0 || entry >= CONFIG_ENTRY_COUNT){
		SCREAM("Invalid value of enum config_entry given: %d", entry);
		return false;
	}
	
	return listener_unregister(config->lst_cell[entry].listener, (listener_handler)handler);
}

static struct config_raw conf = {
		.timeout_max = 5,
		.timeout_freq = 5000,
		.ttf = 10,
		.user_max = 100,
		.username_length_max = 10,
};

struct config_raw config_get_legacy(){
	return conf;
}
void config_set_legacy(struct config_raw config){
	conf = config;
}