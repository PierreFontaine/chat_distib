#ifndef PROJECT_CORE_H
#define PROJECT_CORE_H

#include "array.h"
#include "message.h"

// NB: an user leave when they either leave or are kicked
enum core_kick_type{
	CORE_KICK_BANNED,
	CORE_KICK_USER_COUNT,
	CORE_KICK_USERNAME_LENGTH,
	CORE_KICK_TIMEOUT,
};
struct core_kick{
	enum core_kick_type type;
	struct message_user user;
};

enum core_join{
	CORE_JOIN_OK,
	CORE_JOIN_ALREADY_HERE,
	CORE_JOIN_BANNED,
	CORE_JOIN_USER_COUNT,
	CORE_JOIN_USERNAME_LENGTH,
	CORE_JOIN_ERROR,
};

typedef struct core_state* core_t;

typedef void (*core_handler_user_kick)(void* data, struct core_kick* kick);
typedef void (*core_handler_user_list_update)(void* data, array_t user_array);
typedef void (*core_handler_illegal_op)(void* data, struct message_addr* addr);

/**
 * Create a new core_t
 * @return the address of a valid core_t object. NULL if an error happened during creation
 */
core_t core_init();
/**
 * Free {@param state}
 * @param state the object to destroy
 * @return Whether some memory leaked
 */
_Bool core_free(core_t state);

enum core_join core_add_user(core_t state, struct message_user user);
_Bool core_user_leave(core_t state, struct message_addr addr); // i.e. an user leave properly
_Bool core_is_banned(core_t state, struct message_addr addr);
_Bool core_is_present(core_t state, struct message_addr addr);

_Bool core_incr_timeout_legacy(core_t state, int max_timeout);
_Bool core_incr_timeout(core_t state);
_Bool core_reset_timeout(core_t state, struct message_addr addr);

_Bool core_set_ban_list(core_t state, struct message_user const* users, int nb_user);
_Bool core_add_ban(struct core_state* state, struct message_user user);
_Bool core_add_ban_list(core_t state, struct message_user const* users, int nb_banned);

int core_get_users(core_t state, struct message_user** users, int** timeout);
int core_get_banned(core_t state, struct message_user** banned);

unsigned core_user_max(core_t state);
_Bool core_set_user_max(core_t state, unsigned val);
unsigned core_username_length_max(core_t state);
_Bool core_set_username_length_max(core_t state, unsigned val);
unsigned core_timeout_max(core_t state);
_Bool core_set_timeout_max(core_t state, unsigned val);


_Bool core_register_handler_kick(core_t state, core_handler_user_kick kick, void* data, _Bool clean_up);
_Bool core_unregister_handler_kick(core_t state, core_handler_user_kick kick);
void core_emit_kick(core_t state, enum core_kick_type type, struct message_user user);

_Bool core_register_handler_update(core_t state, core_handler_user_list_update update, void* data, _Bool clean_up);
_Bool core_unregister_handler_update(core_t state, core_handler_user_list_update update);
_Bool core_emit_update(core_t state);

_Bool core_register_handler_illegal(core_t state, core_handler_illegal_op illegal, void* data, _Bool clean_up);
_Bool core_unregister_handler_illegal(core_t state, core_handler_illegal_op illegal);
void core_emit_illegal(core_t state, struct message_addr addr);


static struct message_user* core_get_user(core_t state, int pos);
static _Bool core_remove_user_list(core_t state, int pos);

#endif
