#include "menu.h"

#include "gui/gutils.h"
#include "terminal_lite.h"

#include "utils.h"
#include "array.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

struct menu_sub {
	char* name;
	menu_handler_update update;
	menu_handler_draw draw;
	void* extra;
	bool free_extra;
};
struct menu {
	menu_handler_draw draw;
	void* extra;
	array_t submenu;
	int selected;
	int current_menu;
	_Bool free_extra;
	bool running;
};

menu_t menu_init(menu_handler_draw draw, void* extra, _Bool free_extra){
	menu_t res = malloc(sizeof(*res));
	if(res == NULL){
		return NULL;
	}
	res->submenu = array_init();
	if(res->submenu == NULL){
		free(res);
		return NULL;
	}
	res->current_menu = -1;
	res->selected = 0;
	
	res->extra = extra;
	res->draw = draw;
	res->free_extra = free_extra;
	res->running = true;
	
	return res;
}
bool menu_add_sub(struct menu* menu, char const* name, menu_handler_update update, menu_handler_draw draw, void* extra, _Bool free_extra){
	if(update == NULL || draw == NULL){
		SCREAM("Attempt to create a sub menu without draw and/or update function");
		return false;
	}
	
	struct menu_sub* sub = malloc(sizeof(*sub));
	if(sub == NULL){
		SCREAM("Failed to allocate a menu_sub");
		return false;
	}
	sub->name = utils_copy_alloc(name);
	if(sub->name == NULL){
		free(sub);
		SCREAM("Failed to allocate the array for the sub menu");
		return false;
	}
	
	sub->extra = extra;
	sub->free_extra = free_extra;
	sub->update = update;
	sub->draw = draw;
	
	array_push(menu->submenu, sub);
	
	return true;
}
void menu_free(struct menu* menu){
	if(menu->free_extra){
		free(menu->extra);
	}
	for(int i=0, c=array_size(menu->submenu); i<c; i++){
		struct menu_sub* sub = array_get(menu->submenu, i);
		free(sub->name);
		if(sub->free_extra){
			free(sub->extra);
			sub->extra = NULL;
		}
	}
	array_free(menu->submenu);
	menu->submenu = NULL;
}


bool menu_update(struct menu* menu, struct keyboard_event event){
	if(menu->current_menu < -1 || menu->current_menu >= array_size(menu->submenu)){
		SCREAM("Menu is in an inconsistent state (pos: %d | size: %d)", menu->current_menu, array_size(menu->submenu));
		return false;
	}
	
	if(!menu->running){
		SCREAM("Updating a stopped menu");
		return false;
	}
	
	// main menu
	if(menu->current_menu == -1){
		if(event.type == KEYBOARD_CMD){
			if(event.cmd == KEYBOARD_LEFT){
				if(menu->selected > 0){
					menu->selected--;
				}
			}else if(event.cmd == KEYBOARD_RIGHT){
				if(menu->selected < array_size(menu->submenu)){
					menu->selected++;
				}
			}else if(event.cmd == KEYBOARD_ENTER){
				if(menu->selected == array_size(menu->submenu)){
					menu->running = false;
					return true;
				}
				struct menu_sub *current_sub = array_get(menu->submenu, menu->selected);
				if(current_sub == NULL){
					SCREAM("Failed to get sub menu (pos: %d | size: %d)", menu->selected, array_size(menu->submenu));
					menu->current_menu = -1;
					return false;
				}
				// update the sub menu once before display (to allow setup)
				struct keyboard_event dummy_event;
				dummy_event.type = KEYBOARD_CMD;
				dummy_event.cmd = KEYBOARD_NONE;
				// if the menu close instantly, doesn't switch
				if(current_sub->update(current_sub->extra, dummy_event)){
					menu->current_menu = menu->selected;
				}
			}
		}
	// sub menu
	}else{
		struct menu_sub *current_sub = array_get(menu->submenu, menu->current_menu);
		if(current_sub == NULL){
			SCREAM("Failed to get sub menu (pos: %d | size: %d)", menu->current_menu, array_size(menu->submenu));
			menu->current_menu = -1;
			return false;
		}
		if(!current_sub->update(current_sub->extra, event)){
			// back to main menu
			menu->current_menu = -1;
		}
	}
	return true;
}
void menu_draw(struct menu* menu){
	if(!menu->running){
		SCREAM("Updating a stopped menu");
		return;
	}
	
	bool main_menu = menu->current_menu == -1;
	
	// reset
	term_reset_color();
	term_clear();
	term_set_color_front(term_bright(TERM_WHITE));
	
	
	// draw top
	term_write_middle(1, "Server v.1.0.0");
	term_draw_line(2);
	if(main_menu){
		term_move_write(3, 2, "Main menu"); // todo change this
	}else{
		struct menu_sub* sub = array_get(menu->submenu, menu->current_menu);
		term_move_write(3, 2, "%s", sub->name);
	}
	
	
	// draw bottom
	int height = term_height();
	enum term_color color_selected;
	term_draw_line(height-1);
	if(main_menu){
		color_selected = term_bright(TERM_WHITE);
	}else{
		color_selected = term_bright(TERM_BLACK);
		term_set_color_front(term_bright(TERM_BLACK));
	}
	term_move_write(3, height-1, "Command");
	
	// draw commands
	int pos = 1;
	for(int i=0, c=array_size(menu->submenu); i<c; i++){
		struct menu_sub* current = array_get(menu->submenu, i);
		if(i == menu->selected){
			term_set_color_front(color_selected);
			term_move_write(pos, height, "[%s]", current->name);
		}else{
			term_set_color_front(TERM_BLACK);
			term_move_write(pos+1, height, "%s", current->name);
		}
		pos += 3 + strlen(current->name);
	}
	if(array_size(menu->submenu) == menu->selected){
		term_set_color_front(color_selected);
		term_move_write(pos, height, "[exit]");
	}else{
		term_set_color_front(TERM_BLACK);
		term_move_write(pos+1, height, "exit");
	}
	
	if(menu->current_menu == -1){
		menu->draw(menu->extra);
	}else{
		struct menu_sub* current = array_get(menu->submenu, menu->current_menu);
		current->draw(current->extra);
	}
}

bool menu_running(struct menu* menu){
	return menu->running;
}
