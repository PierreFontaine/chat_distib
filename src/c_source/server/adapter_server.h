#ifndef PROJECT_ADAPTER_SERVER_H
#define PROJECT_ADAPTER_SERVER_H

#include "core.h"
#include "config.h"
#include "udp.h"
#include "timer.h"

// config -> core
void adapter_server_user_count(core_t state, struct config_change_event* event);
void adapter_server_username_length(core_t state, struct config_change_event* event);
void adapter_server_timeout_max(core_t state, struct config_change_event* event);

// config -> udp
void adapter_server_ttl(udp_t udp, struct config_change_event* event);

// config -> timer
void adapter_server_timeout_freq(tim_t tim, struct config_change_event* event);

// core -> udp
void adapter_server_kick(udp_t udp, struct core_kick* kick);
void adapter_server_update(udp_t udp, array_t user_array);
void adapter_server_illegal(udp_t udp, struct message_addr* addr);

// sync(message) -> core
void adapter_server_process_message(core_t state, udp_t udp, struct message message);

// core -> menu
int adapter_server_get_user(core_t state, struct message_user** users);

#endif
