#include "utils.h"

#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
#include <string.h>

char const* program_name = NULL;


char* utils_copy_alloc(char const* str){
	char* tmp = malloc(strlen(str)+1);
	if(tmp == NULL){
		SCREAM("Failed to alloc a copy of '%s'", str);
		return NULL;
	}
	strcpy(tmp, str);
	return tmp;
}


void utils_set_program_name(char const* name){
	program_name = name;
}

char const* utils_get_program_name(){
	return program_name;
}


bool utils_str_to_uint8(char *str, uint8_t *res){
	long value;
	
	/* The parse failed */
	if(utils_str_to_long(str, &value)){
		SCREAM("Cannot parse \"%s\" to an long\n", str);
		return false;
	}
	/* too small */
	if(value < 0){
		SCREAM("The value %ld is too small for an uint8\n", value);
		return false;
	}
	/* too big */
	if(value > UCHAR_MAX){
		SCREAM("The value %ld is too big for an uint8\n", value);
		return false;
	}
	*res = (uint8_t)value;
	return true;
}
_Bool utils_str_to_short(char *str, short *res){
	long value;
	
	/* The parse failed */
	if(utils_str_to_long(str, &value)){
		SCREAM("Cannot parse \"%s\" to an long\n", str);
		return false;
	}
	/* too small */
	if(value < INT_MIN){
		SCREAM("The value %ld is too small for an short\n", value);
		return false;
	}
	/* too big */
	if(value > INT_MAX){
		SCREAM("The value %ld is too big for an short\n", value);
		return false;
	}
	*res = (short)value;
	return true;
}
_Bool utils_str_to_int(char *str, int *res){
	long value;
	
	/* The parse failed */
	if(utils_str_to_long(str, &value)){
		SCREAM("Cannot parse \"%s\" to an long\n", str);
		return false;
	}
	/* too small */
	if(value < INT_MIN){
		SCREAM("The value %ld is too small for an int\n", value);
		return false;
	}
	/* too big */
	if(value > INT_MAX){
		SCREAM("The value %ld is too big for an int\n", value);
		return false;
	}
	*res = (int)value;
	return true;
}
_Bool utils_str_to_long(char *str, long *res){
	long value;
	char *tmp;
	value = strtol(str, &tmp, 10);
	/* The parse failed */
	if(*tmp != '\0'){
		SCREAM("Cannot parse \"%s\" to an long\n", str);
		return 0;
	}
	*res = value;
	return 1;
}

void utils_option_print(struct utils_option opt){
	printf("\t-%c, --%s", opt.sh_opt, opt.name);
	if(opt.param != NULL)
		printf("=%s", opt.param);
	printf("\n\t\t%s\n", opt.help);
}
void utils_print_option(const char *intro, struct utils_option *options, int n_info){
	int i;
	printf("Usage: %s [OPTION]...\n", utils_get_program_name());
	printf("%s\n", intro);
	printf("\nOptions:\n");
	for(i = 0; i < n_info; i++){
		utils_option_print(options[i]);
	}
	printf("\nReport bugs to Johann Cebollado <johann.cebollado@etud.univ-pau.fr> and Pierre Fontaine <pierre.fontaine@etud.univ-pau.fr>.\n");
}

//#define DEBUG_OPTION_PARSE
#define FST_PRINTABLE_CHAR 32
#define LST_PRINTABLE_CHAR 126
bool utils_parse_args(int argc, char *argv[], struct utils_task* tasks, int n_task){
	struct utils_task *buff[LST_PRINTABLE_CHAR - FST_PRINTABLE_CHAR + 1] = {0};
	struct option *options = NULL;
	char *option_template = NULL;
	
	options = malloc(n_task * sizeof(struct option));
	if(options == NULL){
		SCREAM("Cannot alloc the options buffer.");
		return false;
	}
	option_template = malloc((n_task * 2 + 1) * sizeof(char));
	if(option_template == NULL){
		SCREAM("Cannot alloc the option template.");
		free(options);
		return false;
	}
	
	option_template[0] = ':';
	char* currentChar = option_template + 1;
	
	utils_set_program_name(argv[0]);
	
	/* Compile with -DDEBUG_OPTION_PARSE to display debug message */
#ifdef DEBUG_OPTION_PARSE
	SCREAM("Starting parsing args");
#endif
	
	/* Inspect the option given to populate "buff" (array containing a <short option> ->"pipe_common_task_t" mapping)
	 * and "options" (array containing the "struct option" that getopt_long will parse) */
	for(int i = 0; i < n_task; i++){
		/* put the "struct option" aside for getopt_long */
		options[i] = tasks[i].option;
		
		/* if flag is null, the field val should match a char */
		if(tasks[i].option.flag == NULL){
			/* check if a variable was provided */
			if(tasks[i].addr == NULL){
				SCREAM("The action on the option \"%s\" doesn't have a variable to work with\n",
						tasks[i].option.name);
				return false;
			}
			
			/* check if a task was provided */
			if(tasks[i].type == UTILS_NO_OP){
				SCREAM("The action on the option \"%s\" doesn't have a task\n", tasks[i].option.name);
				return false;
			}

#ifdef DEBUG_OPTION_PARSE
			SCREAM(" . Setting the task for the option '%c' at index %d", tasks[i].option.val, tasks[i].option.val - FST_PRINTABLE_CHAR);
#endif
			
			/* set the mapping for the <short option> option.val  */
			buff[tasks[i].option.val - FST_PRINTABLE_CHAR] = &tasks[i];
			
			/* register short option in "option_template" */
			*currentChar = (char)tasks[i].option.val;
			currentChar++;
			
			/* register argument */
			if(tasks[i].option.has_arg != no_argument){
				*currentChar = ':';
				currentChar++;
			}
		}
	}

#ifdef DEBUG_OPTION_PARSE
	SCREAM("Parsing options:");
#endif
	
	
	int opt;
	int index;
	opterr = 0; /* disable error display */
	while((opt = getopt_long(argc, argv, option_template, options, &index)) != -1){
		/* Happen when "struct option".flag isn't NULL. Nothing to do in that case so case ignored. */
		if(opt == 0){
			continue;
		}
		
		/* If "opt" is outside [MIN_PRINTABLE_CHAR, MAX_PRINTABLE_CHAR], doing buff[opt - MAGIC] would be dangerous
		 * but the only case when this should happen is if "struct option".flag != NULL (in this case, "opt" == 0)
		 * and that case is filtered out by the first if. */
		/* So, in theory, this is dead code. In practice, I don't know so I leave it here (maybe unicode could do it ?). */
		if(opt < FST_PRINTABLE_CHAR || opt > LST_PRINTABLE_CHAR){
			SCREAM("Char unprintable char has been return by getopt_long: '%c' (n°%d).\n", opt, opt);
			return false;
		}
		
		/* Usually trigger by unrecognized option */
		if(buff[opt - FST_PRINTABLE_CHAR] == NULL){
			if(opt == '?'){
				SCREAM("Unknown option encountered while parsing '%s'\n", argv[optind - 1]);
				return false;
			}
			if(opt == ':'){
				SCREAM("Missing parameter after '%s'\n", argv[optind - 1]);
				return false;
			}
			/* should never happen */
			SCREAM("no task provided to handle '%c'\n", opt);
			return false;
		}

#ifdef DEBUG_OPTION_PARSE
		SCREAM(" . Option '%c' (%s) founded, loading task at index %d", opt, buff[opt - FST_PRINTABLE_CHAR]->option.name, opt - FST_PRINTABLE_CHAR);
		SCREAM("     Associated value stored @ %p", buff[opt - FST_PRINTABLE_CHAR]->addr);
#endif
		/* do the requested action. */
		switch(buff[opt - FST_PRINTABLE_CHAR]->type){
			case UTILS_INCR_INT:{
				++*((int*)buff[opt - FST_PRINTABLE_CHAR]->addr);
#ifdef DEBUG_OPTION_PARSE
				SCREAM("   -> increment provided value to %d", *(int*)buff[opt - FST_PRINTABLE_CHAR]->addr);
#endif
				break;
			}
			case UTILS_SET_STR:{
				/* This is safe (scope wise), "optarg" point to an element of "argv" */
				*((char**)buff[opt - FST_PRINTABLE_CHAR]->addr) = optarg;
#ifdef DEBUG_OPTION_PARSE
				SCREAM("   -> redirecting string pointer to \"%s\"", optarg);
#endif
				break;
			}
			case UTILS_SET_UINT8:{
				if(!utils_str_to_uint8(optarg, buff[opt - FST_PRINTABLE_CHAR]->addr)){
					SCREAM("Error while parsing the argument of option \"%s\"\n",
							buff[opt - FST_PRINTABLE_CHAR]->option.name);
					return false;
				}
#ifdef DEBUG_OPTION_PARSE
				SCREAM("   -> set provided value to %d (u8)", *(uint8_t*)buff[opt - FST_PRINTABLE_CHAR]->addr);
#endif
				break;
			}
			case UTILS_SET_SHORT:{
				if(!utils_str_to_short(optarg, buff[opt - FST_PRINTABLE_CHAR]->addr)){
					SCREAM("Error while parsing the argument of option \"%s\"\n",
							buff[opt - FST_PRINTABLE_CHAR]->option.name);
					return false;
				}
#ifdef DEBUG_OPTION_PARSE
				SCREAM("   -> set provided value to %d (short)", *(short*)buff[opt - FST_PRINTABLE_CHAR]->addr);
#endif
				break;
			}
			case UTILS_SET_INT:{
				if(!utils_str_to_int(optarg, buff[opt - FST_PRINTABLE_CHAR]->addr)){
					SCREAM("Error while parsing the argument of option \"%s\"\n",
							buff[opt - FST_PRINTABLE_CHAR]->option.name);
					return false;
				}
#ifdef DEBUG_OPTION_PARSE
				SCREAM("   -> set provided value to %d (int)", *(int*)buff[opt - FST_PRINTABLE_CHAR]->addr);
#endif
				break;
			}
			case UTILS_SET_LONG:{
				if(!utils_str_to_long(optarg, buff[opt - FST_PRINTABLE_CHAR]->addr)){
					SCREAM("Error while parsing the argument of option \"%s\"\n",
							buff[opt - FST_PRINTABLE_CHAR]->option.name);
					return false;
				}
#ifdef DEBUG_OPTION_PARSE
				SCREAM("   -> set provided value to %ld (long)", *(long*)buff[opt - FST_PRINTABLE_CHAR]->addr);
#endif
				break;
			}
			case UTILS_NO_OP:{
				/* NO OP (and never happen but the compiler cry if not mentioned) */
				break;
			}
		}
	}
#ifdef DEBUG_OPTION_PARSE
	SCREAM("Option parsing ended");
#endif
	free(options);
	free(option_template);
	return true;
}
void utils_common_setup_option(int index, struct utils_option selected_option, enum utils_task_type action, void* addr, struct utils_option* option_array, struct utils_task* task_array){
	option_array[index] = selected_option;
	task_array[index].type = action;
	task_array[index].addr = addr;
	task_array[index].option.name = selected_option.name;
	task_array[index].option.has_arg = (selected_option.param != NULL ? required_argument : no_argument);
	task_array[index].option.flag = NULL;
	task_array[index].option.val = selected_option.sh_opt;
}
