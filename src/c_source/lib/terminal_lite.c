#include "terminal_lite.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdarg.h>

enum term_color term_bright(enum term_color color){
	return color + 60;
}
void term_set_color(enum term_color front, enum term_color back){
	printf("\e[%d;%dm", front, back + 10);
}
void term_set_color_front(enum term_color color){
	printf("\e[%dm", color);
}
void term_set_color_back(enum term_color color){
	printf("\e[%dm", color + 10);
}
void term_reset_color(){
	printf("\e[0m");
}

int term_width(){
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	return w.ws_col;
}
int term_height(){
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	return w.ws_row;
}

void term_clear(){
	printf("\e[2J\e[3J");
}
void term_clear_line(){
	printf("\e[2K");
}
void term_move(int x, int y){
	printf("\e[%d;%dH", y, x);
}
void term_write_v(char const* str, va_list arglist){
	vprintf(str, arglist); // fixme filter out ANSI escape code & not reach the end of the terminal
}
void term_write(char const* str, ...){
	va_list arglist;
	va_start(arglist, str);
	term_write_v(str, arglist);
	va_end(arglist);
}
void term_move_write(int x, int y, char const* str, ...){
	term_move(x, y);
	va_list arglist;
	va_start(arglist, str);
	term_write_v(str, arglist);
	va_end(arglist);
}

void term_draw_line(int y){
	int width = term_width();
	term_move(0, y);
	for(int i=0; i<width; i++){
		printf("\xe2\x94\x81");
	}
}
void term_write_right(int y, char const* str){
	int width = term_width();
	int length = strlen(str);
	term_move_write(width-length+1, y, str);
}
void term_write_middle(int y, char const* str){
	int width = term_width();
	int length = strlen(str);
	term_move_write(width/2-length/2, y, str);
}