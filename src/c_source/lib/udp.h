#ifndef PROJECT_UDP_H
#define PROJECT_UDP_H

#include "message.h"

typedef struct udp* udp_t;

struct udp_args {
	udp_t udp;
	void* sync_obj;
	_Bool (*sync)(void* sync_obj, struct message);
};

udp_t udp_init(in_addr_t addr, uint16_t port);
_Bool udp_free(udp_t obj);

_Bool udp_change_ttl(udp_t udp, uint8_t ttl);
void* udp_process(struct udp_args* args);
_Bool udp_send(udp_t udp, struct message message);


struct udp_obj_legacy {
	int sock;
	struct sockaddr_in addr;
};

struct udp_args_legacy {
	struct udp_obj_legacy* state;
	void* sync_obj;
	_Bool (*sync)(void *sync_obj, struct message message);
	void* filter_obj;
	_Bool (*filter)(void* filter_obj, struct message message);
};

struct udp_obj_legacy* udp_init_legacy(in_addr_t addr, uint16_t port);
_Bool udp_free_legacy(struct udp_obj_legacy* obj);
_Bool udp_change_ttl_legacy(struct udp_obj_legacy* obj, uint8_t ttl);

/**
 *
 * @param args the argument of the func. Must be malloc-ed
 * @return NULL
 */
void* udp_process_legacy(struct udp_args_legacy* args);
_Bool udp_send_legacy(struct udp_obj_legacy obj, struct message message);

#endif
