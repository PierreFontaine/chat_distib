#ifndef PROJECT_ARRAY_H
#define PROJECT_ARRAY_H

// Simple array "class"
// Store some void*, free them on destruction

typedef struct array* array_t;

array_t array_init();
_Bool array_free(array_t array);

int array_size(array_t array);
void* array_get(array_t array, int pos);
_Bool array_push(array_t array, void* data);
_Bool array_remove(array_t array, int pos);
void array_clean(array_t array);
_Bool array_strip(array_t array);

#endif
