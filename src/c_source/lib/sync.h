#ifndef PROJECT_SYNC_H
#define PROJECT_SYNC_H

#include "keyboard.h"
#include "message.h"
#include "timer.h"

enum sync_event_type {
	SYNC_KEYBOARD,
	SYNC_UDP,
	SYNC_TIMER,
};
struct sync_event {
	enum sync_event_type type;
	union {
		struct keyboard_event keyboard;
		struct message udp;
		struct timespec timer;
	};
};

typedef struct sync* sync_t;

sync_t sync_init();
void sync_close(sync_t obj);
_Bool sync_free(sync_t obj);

_Bool sync_keyboard_adapter(sync_t obj, struct keyboard_event event);
_Bool sync_udp_adapter(sync_t obj, struct message event);
_Bool sync_timer_adapter(sync_t obj, struct timespec event);
struct sync_event sync_poll(sync_t obj);

#endif
