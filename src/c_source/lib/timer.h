#ifndef PROJECT_LIB_TIMER_H
#define PROJECT_LIB_TIMER_H

#include <time.h>

typedef struct tim* tim_t;

tim_t tim_init();
_Bool tim_free(tim_t obj);

struct timespec tim_delay(tim_t obj);
void tim_set_delay(tim_t obj, struct timespec val);

struct tim_args {
	void* sync_obj;
	_Bool (*sync)(void* sync_obj, struct timespec time_elapsed);
	void* delay_obj;
	struct timespec (*delay)(void* delay_obj);
};

void* tim_process(struct tim_args* args);

#endif
