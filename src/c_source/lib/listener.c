#include "listener.h"

#include "array.h"
#include "utils.h"

#include <stdlib.h>
#include <stdbool.h>

static struct listener_cell {
	listener_handler handler;
	void* handler_data;
	_Bool clean_up;
};

struct listener{
	array_t array;
};

listener_t listener_init(){
	listener_t listener = malloc(sizeof(*listener));
	if(listener == NULL){
		SCREAM("Unable to alloc an listener_t");
		return NULL;
	}
	listener->array = array_init();
	if(listener->array == NULL){
		free(listener);
		SCREAM("Unable to alloc the array");
		return NULL;
	}
	return listener;
}
bool listener_free(listener_t listener){
	if(listener == NULL){
		return true;
	}
	for(int i=0, c=array_size(listener->array); i<c; i++){
		struct listener_cell* cell = array_get(listener->array, i);
		if(cell->clean_up){
			free(cell->handler_data);
		}
	}
	bool res = array_free(listener->array);
	free(listener);
	return res;
}

void listener_emit(listener_t listener, void* event_data){
	for(int i=0, c=array_size(listener->array); i<c; i++){
		struct listener_cell* cell = array_get(listener->array, i);
		cell->handler(cell->handler_data, event_data);
	}
}
bool listener_register(listener_t listener, listener_handler handler, void* handler_data, bool clean_up){
	struct listener_cell* cell = malloc(sizeof(*cell));
	if(cell == NULL){
		SCREAM("Unable to alloc a listener_cell");
		return false;
	}
	cell->handler = handler;
	cell->handler_data = handler_data;
	cell->clean_up = clean_up;
	if(!array_push(listener->array, cell)){
		free(cell);
		SCREAM("Unable to push a listener_cell in the array");
		return false;
	}
	return true;
}
bool listener_unregister(listener_t listener, listener_handler handler){
	for(int i=0, c=array_size(listener->array); i<c; i++){
		struct listener_cell* cell = array_get(listener->array, i);
		if(cell->handler == handler){
			if(cell->clean_up){
				free(cell->handler_data);
			}
			return array_remove(listener->array, i);
		}
	}
	return false;
}
