#ifndef PROJECT_MESSAGE_H
#define PROJECT_MESSAGE_H

#include <netinet/in.h>

/**
 * Structure for modeling users network identification
 */
struct message_addr {
	unsigned short port;
	unsigned char ip[4];
};

#define MESSAGE_EQ_ADDR(a, b) \
     ((a).port == (b).port   \
   && (a).ip[0] == (b).ip[0] \
   && (a).ip[1] == (b).ip[1] \
   && (a).ip[2] == (b).ip[2] \
   && (a).ip[3] == (b).ip[3])
#define MESSAGE_ADDR_FMT "%d.%d.%d.%d:%d"
#define MESSAGE_ADDR_FMT_PARAM(addr) addr.ip[0], addr.ip[1], addr.ip[2], addr.ip[3], addr.port
/**
 * Structure for modeling users
 */
struct message_user {
	struct message_addr addr;
	char* username;
};

struct message_user* message_user_copy(struct message_user user);
_Bool message_user_free(struct message_user* user);

/**
 * Enumeration giving and ID for each kind of message
 */
enum message_type{
	MESSAGE_TYPE_LST_USER = 0x10,
	MESSAGE_TYPE_JOIN = 0x00,
	MESSAGE_TYPE_LEAVE = 0x01,
	MESSAGE_TYPE_PING = 0x11,
	MESSAGE_TYPE_PONG = 0x02,
	MESSAGE_TYPE_REQ_LST = 0x03,
	MESSAGE_TYPE_ERR_CO = 0x12,
	MESSAGE_TYPE_CLOSING = 0x13,
};

/**
 * Structure for storing users
 */
struct message_lst_user {
	struct message_user *users;
	unsigned int nb_user;
};

enum message_err_co_type {
	MESSAGE_ERRCO_BANNED = 0x00,
	MESSAGE_ERRCO_TOO_MANY_USER = 0x01,
	MESSAGE_ERRCO_USERNAME_TOO_LONG = 0x02,
	MESSAGE_ERRCO_NOT_CONNECTED = 0x03,
};

struct message_err_co {
	enum message_err_co_type type;
	struct message_addr addr;
};

/**
 * Structure for modeling a message
 */
struct message{
	enum message_type type;
	union {
		struct message_lst_user lst_user;
		struct message_user join;
		struct message_addr leave;
		struct {} ping;
		struct message_addr pong;
		struct message_addr req_lst;
		struct message_err_co err_co;
		struct {} closing;
	};
};

/**
 * Method that specifically return a message for ListUser request.
 *
 * @param users an array of users
 * @param nb_user how many users the array is storing users
 * @return a message
 */
struct message message_get_lst_user(struct message_user* users, unsigned nb_user);
struct message message_get_join(struct message_user user);
struct message message_get_leave(struct message_addr addr);
struct message message_get_ping();
struct message message_get_pong(struct message_addr addr);
struct message message_get_req_lst(struct message_addr addr);
struct message message_get_err_co(struct message_addr addr, enum message_err_co_type type);
struct message message_get_closing();

// Here if you can don't want to ignore message
ssize_t receive_data(int sock, void** data);
_Bool parse_data(void const* current, size_t msgLength, struct message* message);

/**
 * Given a sock wait for a message
 *
 * @param sock the socket id
 * @return a message
 */
struct message receive_message(int sock);
_Bool send_message(int sock, struct sockaddr_in addr, struct message message);

void message_print(struct message message);
void message_free(struct message* message);

_Bool message_from_client(struct message message);
struct message_addr message_get_client(struct message message);

_Bool message_for_server(struct message message);
_Bool message_for_client(struct message message, struct message_addr addr);

#endif
