#include "array.h"

#include "utils.h"

#include <stdlib.h>
#include <stdbool.h>

struct array{
	int nb_elt;
	int cap_elt;
	void** elts;
};

array_t array_init(){
	array_t array = malloc(sizeof(*array));
	if(array == NULL){
		SCREAM("Failed to alloc an array_t");
		return NULL;
	}
	array->nb_elt = 0;
	array->cap_elt = 10;
	array->elts = malloc(sizeof(*array->elts) * array->cap_elt);
	if(array->elts == NULL){
		free(array);
		SCREAM("Failed to alloc buffer (size: %ld)", sizeof(*array->elts) * array->cap_elt);
		return NULL;
	}
	return array;
}
bool array_free(array_t array){
	if(array == NULL){
		return true;
	}
	for(int i=0; i<array->nb_elt; i++){
		free(array->elts[i]);
	}
	free(array->elts);
	free(array);
	return true;
}

int array_size(array_t array){
	return array->nb_elt;
}
void* array_get(array_t array, int pos){
	if(pos < 0 || pos >= array->nb_elt){
		SCREAM("Attempt to get an element out of bound (length: %d; pos: %d)",  array->nb_elt, pos);
		return NULL;
	}
	return array->elts[pos];
}
bool array_push(array_t array, void* data){
	if(array->nb_elt == array->cap_elt){
		void** tmp = malloc(sizeof(*tmp) * array->cap_elt * 2);
		if(tmp == NULL){
			SCREAM("Failed to alloc a new buffer (size: %ld)", sizeof(*tmp) * array->cap_elt * 2);
			return false;
		}
		for(int i = 0; i < array->cap_elt; i++){
			tmp[i] = array->elts[i];
		}
		free(array->elts);
		array->elts = tmp;
		array->cap_elt *= 2;
	}
	array->elts[array->nb_elt] = data;
	array->nb_elt++;
	return true;
}
bool array_remove(array_t array, int pos){
	if(pos < 0 || pos >= array->nb_elt){
		SCREAM("Attempt to remove an out of bound element (length: %d; pos: %d)", array->nb_elt, pos);
		return false;
	}

	for(int i = pos; i < array->nb_elt - 1; i++){
		array->elts[i] = array->elts[i + 1];
	}
	array->nb_elt--;
	return true;
}

void array_clean(array_t array){
	for(int i=0; i<array->nb_elt; i++){
		free(array->elts[i]);
	}
	array->nb_elt = 0;
}
bool array_strip(array_t array){
	if(array->nb_elt == array->cap_elt){
		return true;
	}
	void** tmp = malloc(sizeof(*tmp) * array->nb_elt);
	if(tmp == NULL){
		SCREAM("Failed to alloc memory to strip the array (size: %ld)", sizeof(*tmp) * array->nb_elt);
		return false;
	}
	for(int i=0; i<array->nb_elt; i++){
		tmp[i] = array->elts[i];
	}
	free(array->elts);
	array->elts = tmp;
	return true;
}