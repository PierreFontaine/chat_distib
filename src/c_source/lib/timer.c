#include "timer.h"

#include "utils.h"

#include <stdlib.h>
#include <stdbool.h>

struct tim{
	struct timespec timespec;
};

tim_t tim_init(){
	tim_t res = malloc(sizeof(*res));
	if(res == NULL){
		SCREAM("Failed to malloc");
		return NULL;
	}
	res->timespec.tv_sec = 1;
	res->timespec.tv_nsec = 0;
	return res;
}
_Bool tim_free(tim_t obj){
	free(obj);
	return true;
}

struct timespec tim_delay(tim_t obj){
	return obj->timespec;
}
void tim_set_delay(tim_t obj, struct timespec val){
	obj->timespec = val;
}


void* tim_process(struct tim_args* t_args){
	struct timespec slept;
	struct tim_args args = *t_args;
	free(t_args);
	do{
		slept = args.delay(args.delay_obj);
		nanosleep(&slept, NULL);
	}while(args.sync(args.sync_obj, slept));
	return NULL;
}
