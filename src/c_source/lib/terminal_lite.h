#ifndef PROJECT_TERMINAL_LITE_H
#define PROJECT_TERMINAL_LITE_H

enum term_color{
	TERM_BLACK = 30,
	TERM_RED = 31,
	TERM_GREEN = 32,
	TERM_YELLOW = 33,
	TERM_BLUE = 34,
	TERM_MAGENTA = 35,
	TERM_CYAN = 36,
	TERM_WHITE = 37,
};

enum term_color term_bright(enum term_color color);
void term_set_color(enum term_color front, enum term_color back);
void term_set_color_front(enum term_color color);
void term_set_color_back(enum term_color color);
void term_reset_color();

int term_width();
int term_height();

void term_clear();
void term_clear_line();
void term_move(int x, int y);
void term_write(char const* str, ...);
void term_move_write(int x, int y, char const* str, ...);

void term_draw_line(int y);
void term_write_right(int y, char const* str);
void term_write_middle(int y, char const* str);

#endif
