#include "udp.h"
#include "utils.h"

#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <memory.h>

struct udp {
	int sock;
	struct sockaddr_in addr;
};

udp_t udp_init(in_addr_t addr, uint16_t port){
	int tmp_sock = socket(AF_INET, SOCK_DGRAM, 0);
	if(tmp_sock < 0){
		SCREAM("Failed to create the socket");
		return NULL;
	}
	if(setsockopt(tmp_sock, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0){
		SCREAM("Failed to enable SO_REUSEADDR");
		close(tmp_sock);
		return NULL;
	}
	if (setsockopt(tmp_sock, SOL_SOCKET, SO_BROADCAST, &(int) {1}, sizeof(int)) < 0) {
		SCREAM("Failed to enable SO_BROADCAST");
		close(tmp_sock);
		return NULL;
	}
	
	struct ip_mreq mreq;
	mreq.imr_multiaddr.s_addr = addr;
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if(setsockopt(tmp_sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0){
		SCREAM("Failed to add membership to the multicast");
		close(tmp_sock);
		return NULL;
	}
	
	struct sockaddr_in socket_addr;
	memset(&socket_addr, 0, sizeof(socket_addr));
	socket_addr.sin_family = AF_INET;
	socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	socket_addr.sin_port = htons(port);
	if (bind(tmp_sock, (struct sockaddr *) &socket_addr, sizeof(socket_addr)) < 0) {
		SCREAM("Failed to bind the socket");
		close(tmp_sock);
		return NULL;
	}
	
	udp_t res = malloc(sizeof(*res));
	if(res == NULL){
		SCREAM("Failed to malloc");
		close(tmp_sock);
		return NULL;
	}
	res->sock = tmp_sock;
	
	struct sockaddr_in multi_addr;
	memset(&multi_addr, 0, sizeof(multi_addr));
	multi_addr.sin_family = AF_INET;
	multi_addr.sin_addr.s_addr = addr;
	multi_addr.sin_port = htons(port);
	res->addr = multi_addr;
	return res;
}
_Bool udp_free(udp_t obj){
	if(obj == NULL){
		SCREAM("Attempt to free an uninitialized udp object");
		return false;
	}
	close(obj->sock);
	free(obj);
	return true;
}

_Bool udp_change_ttl(udp_t obj, uint8_t ttl){
	if(obj == NULL){
		SCREAM("Attempt to change the TTL of an uninitialized udp object");
		return false;
	}
	if (setsockopt(obj->sock, IPPROTO_IP, IP_DEFAULT_MULTICAST_TTL, &ttl, sizeof(ttl)) < 0) {
		SCREAM("Failed to change the TTL of the socket");
		return false;
	};
	return true;
}
void* udp_process(struct udp_args* args){
	// get the arg out of the heap
	struct udp_args args_r = *args;
	free(args);
	struct message mess;
	while(true){
		mess = receive_message(args_r.udp->sock);
		// send the message to be process
		if(!args_r.sync(args_r.sync_obj, mess)){
			break;
		}
	}
	return NULL;
}
bool udp_send(udp_t udp, struct message message){
	return send_message(udp->sock, udp->addr, message);
}



struct udp_obj_legacy* udp_init_legacy(in_addr_t addr, uint16_t port){
	int tmp_sock = socket(AF_INET, SOCK_DGRAM, 0);
	if(tmp_sock < 0){
		SCREAM("Can't create the socket.");
		return NULL;
	}
	if(setsockopt(tmp_sock, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0){
		SCREAM("Cannot enable SO_REUSEADDR on the socket.");
		close(tmp_sock);
		return NULL;
	}
	if (setsockopt(tmp_sock, SOL_SOCKET, SO_BROADCAST, &(int) {1}, sizeof(int)) < 0) {
		SCREAM("Cannot enable SO_BROADCAST on the socket.");
		close(tmp_sock);
		return NULL;
	}
	
	struct ip_mreq mreq;
	mreq.imr_multiaddr.s_addr = addr;
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if(setsockopt(tmp_sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0){
		SCREAM("Cannot add membership.");
		close(tmp_sock);
		return NULL;
	}
	
	struct sockaddr_in socket_addr;
	memset(&socket_addr, 0, sizeof(socket_addr));
	socket_addr.sin_family = AF_INET;
	socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	socket_addr.sin_port = htons(port);
	if (bind(tmp_sock, (struct sockaddr *) &socket_addr, sizeof(socket_addr)) < 0) {
		SCREAM("ERROR WHILE BINDING THE SOCKET");
		close(tmp_sock);
		return NULL;
	}
	
	struct udp_obj_legacy *res = malloc(sizeof(struct udp_obj_legacy));
	if(res == NULL){
		close(tmp_sock);
		return NULL;
	}
	res->sock = tmp_sock;
	
	struct sockaddr_in multi_addr;
	memset(&multi_addr, 0, sizeof(multi_addr));
	multi_addr.sin_family = AF_INET;
	multi_addr.sin_addr.s_addr = addr;
	multi_addr.sin_port = htons(port);
	res->addr = multi_addr;
	return res;
}
bool udp_free_legacy(struct udp_obj_legacy* obj){
	if(obj == NULL)
		return false;
	close(obj->sock);
	free(obj);
	return true;
}

bool udp_change_ttl_legacy(struct udp_obj_legacy* obj, uint8_t ttl){
	if(obj == NULL){
		return false;
	}
	if (setsockopt(obj->sock, IPPROTO_IP, IP_DEFAULT_MULTICAST_TTL, &ttl, sizeof(ttl)) < 0) {
		SCREAM("Cannot change IP_DEFAULT_MULTICAST_TTL of the socket n°%d", obj->sock);
		return false;
	};
	return true;
}


void* udp_process_legacy(struct udp_args_legacy* args){
	// get the arg out of the heap
	struct udp_args_legacy args_r = *args;
	free(args);
	struct message mess;
	while(true){
		mess = receive_message(args_r.state->sock);
		// check if the message should get process
		if(!args_r.filter(args_r.filter_obj, mess)){
			continue;
		}
		// send the message to be process
		if(!args_r.sync(args_r.sync_obj, mess)){
			break;
		}
	}
	return NULL;
}
bool udp_send_legacy(struct udp_obj_legacy obj, struct message message){
	return send_message(obj.sock, obj.addr, message);
}