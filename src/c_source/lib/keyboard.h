#ifndef PROJECT_KEYBOARD_H
#define PROJECT_KEYBOARD_H

#include <termio.h>

enum keyboard_type {
	KEYBOARD_KEY,
	KEYBOARD_CMD,
};
enum keyboard_cmd {
	KEYBOARD_UP,
	KEYBOARD_DOWN,
	KEYBOARD_LEFT,
	KEYBOARD_RIGHT,
	KEYBOARD_ENTER,
	KEYBOARD_DEL,
	KEYBOARD_NONE, // null value of the keyboard
};

struct keyboard_event {
	enum keyboard_type type;
	union{
		int key;
		enum keyboard_cmd cmd;
	};
};

struct termios keyboard_setup();
void keyboard_reset(struct termios t);

struct keyboard_args {
	void* sync_obj;
	_Bool (*sync)(void* sync_obj, struct keyboard_event);
};

/**
 *
 * @param adapter function getting the event
 * @return NULL
 */
void* keyboard_process(struct keyboard_args* args);

#endif
