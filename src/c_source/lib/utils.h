#ifndef PROJECT_UTILS_H
#define PROJECT_UTILS_H

#include <stdio.h>
#include <getopt.h>
#include <stdint.h>

struct utils_option {
	char const* name;
	char const* help;
	char const* param;
	char sh_opt;
};

enum utils_task_type {
	UTILS_NO_OP,
	UTILS_INCR_INT,
	UTILS_SET_STR,
	UTILS_SET_UINT8,
	UTILS_SET_SHORT,
	UTILS_SET_INT,
	UTILS_SET_LONG,
};
struct utils_task {
	void *addr;
	enum utils_task_type type;
	struct option option;
};

char* utils_copy_alloc(char const* str);

_Bool utils_str_to_uint8(char *str, uint8_t *res);
_Bool utils_str_to_short(char *str, short *res);
_Bool utils_str_to_int(char *str, int *res);
_Bool utils_str_to_long(char *str, long *res);

void utils_print_option(const char *intro, struct utils_option *options, int n_info);
_Bool utils_parse_args(int argc, char *argv[], struct utils_task* tasks, int n_task);
void utils_common_setup_option(int index, struct utils_option selected_option, enum utils_task_type action, void* addr, struct utils_option* option_array, struct utils_task* task_array);

#define SCREAM(msg, args...) fprintf(stderr, "%s:%s:%d: "msg"\n", utils_get_program_name(), __FILE__, __LINE__, ##args)

void utils_set_program_name(char const* name);
char const* utils_get_program_name();

#endif
