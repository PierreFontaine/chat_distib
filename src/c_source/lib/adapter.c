#include "adapter.h"

#include <stdbool.h>
#include <stdlib.h>

_Bool adapter_lauch_udp(sync_t sync, udp_t udp, pthread_t* thread){
	struct udp_args* args = malloc(sizeof(struct udp_args_legacy));
	if(args == NULL){
		return false;
	}
	
	args->udp = udp;
	args->sync_obj = sync;
	args->sync = (_Bool (*)(void*, struct message))sync_udp_adapter;
	
	if(pthread_create(thread, NULL, (void* (*)(void*))udp_process, args) != 0){
		free(args);
		return false;
	}
	return true;
}
_Bool adapter_lauch_keyboard(sync_t sync, pthread_t* thread){
	struct keyboard_args* args = malloc(sizeof(struct keyboard_args));
	args->sync_obj = sync;
	args->sync = (_Bool (*)(void*, struct keyboard_event))sync_keyboard_adapter;
	if(pthread_create(thread, NULL, (void* (*)(void*))keyboard_process, args) != 0){
		return false;
	}
	return true;
}
_Bool adapter_lauch_timer(sync_t sync, tim_t tim, pthread_t* thread){
	struct tim_args* args = malloc(sizeof(*args));
	if(args == NULL){
		return false;
	}
	
	args->sync_obj = sync;
	args->sync = (_Bool (*)(void*, struct timespec))sync_timer_adapter;
	args->delay_obj = tim;
	args->delay = (struct timespec(*)(void*))tim_delay;
	
	if(pthread_create(thread, NULL, (void* (*)(void*))tim_process, args) != 0){
		free(args);
		return false;
	}
	return true;
}
