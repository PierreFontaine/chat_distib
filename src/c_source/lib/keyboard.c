#include "keyboard.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


struct termios keyboard_setup(){
	struct termios newt, oldt;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ECHO | ICANON);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	return oldt;
}
void keyboard_reset(struct termios t){
	tcsetattr(STDIN_FILENO, TCSANOW, &t);
}

void* keyboard_process(struct keyboard_args* t_args){
	struct keyboard_args args = *t_args;
	free(t_args);
	struct keyboard_event event;
	int key;
	while(1){
		key = getchar();
		if(key == 27){
			key = getchar();
			if(key == 91){
				key = getchar();
				if(key == 65){
					event.type = KEYBOARD_CMD;
					event.cmd = KEYBOARD_UP;
					if(!args.sync(args.sync_obj, event)) break;
				}else if(key == 66){
					event.type = KEYBOARD_CMD;
					event.cmd = KEYBOARD_DOWN;
					if(!args.sync(args.sync_obj, event)) break;
				}else if(key == 67){
					event.type = KEYBOARD_CMD;
					event.cmd = KEYBOARD_RIGHT;
					if(!args.sync(args.sync_obj, event)) break;
				}else if(key == 68){
					event.type = KEYBOARD_CMD;
					event.cmd = KEYBOARD_LEFT;
					if(!args.sync(args.sync_obj, event)) break;
				}else{
					event.type = KEYBOARD_KEY;
					event.key = 27;
					if(!args.sync(args.sync_obj, event)) break;
					event.key = 91;
					if(!args.sync(args.sync_obj, event)) break;
					event.key = key;
					if(!args.sync(args.sync_obj, event)) break;
				}
			}else{
				event.type = KEYBOARD_KEY;
				event.key = 27;
				if(!args.sync(args.sync_obj, event)) break;
				event.key = key;
				if(!args.sync(args.sync_obj, event)) break;
			}
		}else if(key == 10){
			event.type = KEYBOARD_CMD;
			event.cmd = KEYBOARD_ENTER;
			if(!args.sync(args.sync_obj, event)) break;
		}else if(key == 127){
			event.type = KEYBOARD_CMD;
			event.cmd = KEYBOARD_DEL;
			if(!args.sync(args.sync_obj, event)) break;
		}else{
			event.type = KEYBOARD_KEY;
			event.key = key;
			if(!args.sync(args.sync_obj, event)) break;
		}
	}
	return NULL;
}
