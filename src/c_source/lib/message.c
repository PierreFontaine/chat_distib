#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "message.h"
#include "utils.h"

struct message_user* message_user_copy(struct message_user user){
	struct message_user* res = malloc(sizeof(*res));
	if(res == NULL){
		SCREAM("Failed to alloc an message_user");
		return NULL;
	}
	res->username = utils_copy_alloc(user.username);
	if(res->username == NULL){
		free(res);
		SCREAM("Failed to copy the username '%s'", user.username);
		return NULL;
	}
	res->addr = user.addr;
	return res;
}
bool message_user_free(struct message_user* user){
	if(user == NULL){
		return true;
	}
	free(user->username);
	free(user);
	return true;
}

uint8_t read_u8(void const** data){
	if(*data == NULL){
		return 0;
	}
	uint8_t var = *(uint8_t*)(*data);
	*data += sizeof(uint8_t);
	return var;
}
uint16_t read_u16(void const** data){
	if(*data == NULL){
		return 0;
	}
	uint16_t var = *(uint16_t*)(*data);
	*data += sizeof(uint16_t);
	return var;
}
uint32_t read_u32(void const** data){
	if(*data == NULL){
		return 0;
	}
	uint32_t var = *(uint32_t*)(*data);
	*data += sizeof(uint32_t);
	return var;
}

// must make sure that "end - beg > 6" before call
bool read_ip_data(void const** beg, void const* end, struct message_addr* user){
	if(beg == NULL){
		return NULL;
	}
	if(end - *beg <= 6){
		SCREAM("Not enough byte remaining to parse an ip & port: %lu remaining", end-*beg);
		return false;
	}
	
	for(int i=0; i<4; ++i){
		user->ip[i] = read_u8(beg);
	}
	user->port = read_u16(beg);
	
	return true;
}

bool read_username(void const** beg, void const* end, struct message_user* user){
	size_t length = strlen(*beg)+1;
	if(length > end - *beg){ // does the username fit ?
		SCREAM("Username not terminating within the message");
		return false;
	}
	
	user->username = malloc(length);
	strcpy(user->username, *beg);
	*beg += length;
	
	return true;
}
bool read_user_and_ip(void const** beg, void const* end, struct message_user* user){
	return read_ip_data(beg, end, &user->addr) && read_username(beg, end, user);
}

uint32_t size_username(char* username){
	return (uint32_t)(strlen(username)+1);
}
uint32_t size_ip(){
	return sizeof(uint8_t)*4 + sizeof(uint16_t);
}
uint32_t size_user_and_ip(struct message_user user){
	return size_ip() + size_username(user.username);
}

void write_u8(void** cur, uint8_t val){
	*(uint8_t*)(*cur) = val;
	*cur += sizeof(val);
}
void write_u16(void** cur, uint16_t val){
	*(uint16_t*)(*cur) = val;
	*cur += sizeof(val);
}
void write_u32(void** cur, uint32_t val){
	*(uint32_t*)(*cur) = val;
	*cur += sizeof(val);
}

void write_username(void** cur, char* username){
	strcpy(*cur, username);
	*cur += strlen(username)+1;
}
void write_ip(void** cur, struct message_addr addr_user){
	for(int i=0; i<4; ++i){
		write_u8(cur, addr_user.ip[i]);
	}
	write_u16(cur, addr_user.port);
}
void write_user_and_ip(void** cur, struct message_user user){
	write_ip(cur, user.addr);
	write_username(cur, user.username);
}

struct message get_message(enum message_type type, struct message_user user){
	struct message result;
	result.type = type;
	switch(type){
		case MESSAGE_TYPE_LST_USER:{
			SCREAM("Attempt to get a MESSAGE_TYPE_LST_USER message from get_message. Should go through message_get_lst_user");
			result.type = (enum message_type)-1;
			break;
		}
		case MESSAGE_TYPE_JOIN:{
			result.join.addr = user.addr;
			result.join.username = utils_copy_alloc(user.username);
			break;
		}
		case MESSAGE_TYPE_LEAVE:{
			result.leave = user.addr;
			break;
		}
		case MESSAGE_TYPE_PING:{
			// NO-OP
			break;
		}
		case MESSAGE_TYPE_PONG:{
			result.pong = user.addr;
			break;
		}
		case MESSAGE_TYPE_REQ_LST:{
			result.req_lst = user.addr;
			break;
		}
		default:{
			SCREAM("Unknown message id: %#0x", type);
		}
	}
	return result;
}
struct message message_get_lst_user(struct message_user* users, unsigned nb_user){
	struct message message;
	message.type = MESSAGE_TYPE_LST_USER;
	message.lst_user.nb_user = nb_user;
	message.lst_user.users = malloc(sizeof(struct message_user) * nb_user);
	for(unsigned i=0; i<nb_user; ++i){
		message.lst_user.users[i].addr = users[i].addr;
		message.lst_user.users[i].username = malloc(strlen(users[i].username)+1);
		strcpy(message.lst_user.users[i].username, users[i].username);
	}
	return message;
}
struct message message_get_join(struct message_user user){
	struct message message;
	message.type = MESSAGE_TYPE_JOIN;
	message.join = user;
	return message;
}
struct message message_get_leave(struct message_addr addr){
	struct message message;
	message.type = MESSAGE_TYPE_LEAVE;
	message.leave = addr;
	return message;
	
}
struct message message_get_ping(){
	struct message message;
	message.type = MESSAGE_TYPE_PING;
	return message;
}
struct message message_get_pong(struct message_addr addr){
	struct message message;
	message.type = MESSAGE_TYPE_PONG;
	message.leave = addr;
	return message;
	
}
struct message message_get_req_lst(struct message_addr addr){
	struct message message;
	message.type = MESSAGE_TYPE_REQ_LST;
	message.req_lst = addr;
	return message;
	
}
struct message message_get_err_co(struct message_addr addr, enum message_err_co_type type){
	struct message message;
	message.type = MESSAGE_TYPE_ERR_CO;
	message.err_co.type = type;
	message.err_co.addr = addr;
	return message;
}
struct message message_get_closing(){
	struct message message;
	message.type = MESSAGE_TYPE_CLOSING;
	return message;
}

ssize_t receive_data(int sock, void** data){
	char buffer[64*1024];
	
	ssize_t nbBit = recvfrom(sock, buffer, 64*1024, 0, NULL, NULL);
	
	if(nbBit <= 0){
		return -1;
	}

#ifdef DEBUG_DISPLAY_PACKET
	printf("packet with id: %i\n", buffer[0]);
	for(ssize_t i=0; i<nbBit; ++i){
		printf("%i ", (unsigned)buffer[i]);
	}
	printf("\n");
#endif
	
	*data = malloc((size_t)nbBit);
	memcpy(*data, buffer, (size_t)nbBit);
	return nbBit;
}

bool parse_data(void const* current, size_t msgLength, struct message* message){
	uint8_t id = read_u8(&current);
	void const* end = current + msgLength;
	switch(id){
		case MESSAGE_TYPE_LST_USER:{
			uint32_t nb_user = read_u32(&current);
			struct message_user *user = malloc(sizeof(struct message_user) * nb_user);
			memset(user, 0, sizeof(struct message_user) * nb_user);
			for(int i=0; i<nb_user && current != NULL; ++i){
				if(!read_user_and_ip(&current, end, &user[i])){
					SCREAM("Failed to load user n°%i of a MESSAGE_TYPE_LST_USER message", i);
					return false;
				}
			}
			message->lst_user.nb_user = nb_user;
			message->lst_user.users = user;
			break;
		}
		case MESSAGE_TYPE_JOIN:{
			if(!read_user_and_ip(&current, end, &message->join)){
				SCREAM("Failed to load the user of a MESSAGE_TYPE_JOIN message");
				return false;
			}
			break;
		}
		case MESSAGE_TYPE_LEAVE:{
			if(!read_ip_data(&current, end, &message->leave)){
				SCREAM("Failed to load the ip of a MESSAGE_TYPE_LEAVE message");
				return false;
			}
			break;
		}
		case MESSAGE_TYPE_PING:{
			// NO-OP
			break;
		}
		case MESSAGE_TYPE_PONG:{
			if(!read_ip_data(&current, end, &message->pong)){
				SCREAM("Failed to load the ip of a MESSAGE_TYPE_LEAVE message");
				return false;
			}
			break;
		}
		case MESSAGE_TYPE_REQ_LST:{
			if(!read_ip_data(&current, end, &message->req_lst)){
				SCREAM("Failed to load the ip of a MESSAGE_TYPE_REQ_LST message");
				return false;
			}
			break;
		}
		case MESSAGE_TYPE_ERR_CO:{
			if(!read_ip_data(&current, end, &message->err_co.addr)){
				SCREAM("Failed to load the ip of a MESSAGE_TYPE_ERR_CO message");
				return false;
			}
			if(end - current < 1){
				SCREAM("Not enough data left to read the type of a MESSAGE_TYPE_ERR_CO message");
				return false;
			}
			uint8_t type = read_u8(&current);
			if(type != MESSAGE_ERRCO_BANNED
					&& type != MESSAGE_ERRCO_TOO_MANY_USER
					&& type != MESSAGE_ERRCO_USERNAME_TOO_LONG
					&& type != MESSAGE_ERRCO_NOT_CONNECTED){
				SCREAM("Unknown err code");
				return false;
			}
			message->err_co.type = (enum message_err_co_type)type;
			break;
		}
		case MESSAGE_TYPE_CLOSING:{
			//NO-OP
			break;
		}
		default:{
			SCREAM("Unknown message id: %#0x", id);
			return false;
		}
	}
	message->type = (enum message_type)id;
	return true;
}

struct message receive_message(int sock){
	struct message result;
	void* data;
	bool done = false;
	
	while(!done){
		ssize_t length = receive_data(sock, &data);
		if(length > 0 && parse_data(data, (size_t)length, &result)){
			done = true;
		}
		free(data);
	}
	
	return result;
}

bool send_message(int sock, struct sockaddr_in addr, struct message message){
	uint32_t header_size = 1;
	uint32_t size_message;
	
	void* buffer = NULL;
	void* current = NULL;
	
	switch(message.type){
		case MESSAGE_TYPE_LST_USER:{
			size_message = header_size + sizeof(uint32_t);
			for(unsigned i=0; i<message.lst_user.nb_user; ++i){
				size_message += size_user_and_ip(message.lst_user.users[i]);
			}
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			current = buffer + header_size;
			write_u32(&current, message.lst_user.nb_user);
			for(unsigned i=0; i<message.lst_user.nb_user; ++i){
				write_user_and_ip(&current, message.lst_user.users[i]);
			}
			break;
		}
		case MESSAGE_TYPE_JOIN:{
			size_message = header_size + size_user_and_ip(message.join);
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			current = buffer + header_size;
			write_user_and_ip(&current, message.join);
			break;
		}
		case MESSAGE_TYPE_LEAVE:{
			size_message = header_size+size_ip();
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			current = buffer + header_size;
			write_ip(&current, message.leave);
			break;
		}
		case MESSAGE_TYPE_PING:{
			size_message = header_size;
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			break;
		}
		case MESSAGE_TYPE_PONG:{
			size_message = header_size+size_ip();
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			current = buffer + header_size;
			write_ip(&current, message.pong);
			break;
		}
		case MESSAGE_TYPE_REQ_LST:{
			size_message = header_size+size_ip();
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			current = buffer + header_size;
			write_ip(&current, message.pong);
			break;
		}
		case MESSAGE_TYPE_ERR_CO:{
			size_message = header_size + size_ip() + sizeof(uint8_t);
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			current = buffer + header_size;
			write_ip(&current, message.err_co.addr);
			write_u8(&current, (uint8_t)message.err_co.type);
			break;
		}
		case MESSAGE_TYPE_CLOSING:{
			size_message = header_size;
			buffer = malloc(size_message);
			if(buffer == NULL) return false;
			break;
		}
		default:{
			SCREAM("Invalid message type");
			return false;
		}
	}
	
	// write the header
	current = buffer;
	write_u8(&current, message.type);
	
	// send the message
	if(sendto(sock, buffer, size_message, 0, (struct sockaddr*)&addr, sizeof(addr)) < 0){
		SCREAM("Unable to launch a message of type %#0x", message.type);
		return false;
	}
	
	return true;
}

void message_print(struct message message){
	switch(message.type){
		case MESSAGE_TYPE_LST_USER:{
			printf("Message containing a list of user (0x10):\n");
			for(int i=0; i<message.lst_user.nb_user; i++){
				struct message_user current = message.lst_user.users[i];
				printf("\t-'%s' with TCP @ "MESSAGE_ADDR_FMT"\n",
						current.username, MESSAGE_ADDR_FMT_PARAM(current.addr));
			}
			break;
		}
		case MESSAGE_TYPE_JOIN:{
			printf("Message containing a log in request (0x00) from '%s' with TCP @ "MESSAGE_ADDR_FMT"\n",
					message.join.username, MESSAGE_ADDR_FMT_PARAM(message.join.addr));
			break;
		}
		case MESSAGE_TYPE_LEAVE:{
			printf("Message containing a log out request (0x01) from an user with TCP @ "MESSAGE_ADDR_FMT"\n",
					MESSAGE_ADDR_FMT_PARAM(message.leave));
			break;
		}
		case MESSAGE_TYPE_PING:{
			printf("Message containing a ping (0x11)\n");
			break;
		}
		case MESSAGE_TYPE_PONG:{
			printf("Message containing a pong (0x02) from an user with TCP @ "MESSAGE_ADDR_FMT"\n",
					MESSAGE_ADDR_FMT_PARAM(message.pong));
			break;
		}
		case MESSAGE_TYPE_REQ_LST:{
			printf("Message containing a request of the user list (0x03) from user with TCP @ "MESSAGE_ADDR_FMT"\n",
					MESSAGE_ADDR_FMT_PARAM(message.req_lst));
			break;
		}
		case MESSAGE_TYPE_ERR_CO:{
			switch(message.err_co.type){
				case MESSAGE_ERRCO_BANNED:{
					printf("Message containing a deny of connection from the server (0x12) because you're banned\n");
					break;
				}
				case MESSAGE_ERRCO_TOO_MANY_USER:{
					printf("Message containing a deny of connection from the server (0x12) because too many are connected\n");
					break;
				}
				case MESSAGE_ERRCO_USERNAME_TOO_LONG:{
					printf("Message containing a deny of connection from the server (0x12) because your username is too long\n");
					break;
				}
				case MESSAGE_ERRCO_NOT_CONNECTED:{
					printf("Message containing a deny of connection from the server (0x12) because you're not connected\n");
					break;
				}
			}
			break;
		}
		case MESSAGE_TYPE_CLOSING:{
			printf("Message containing a closing notice from the server (0x13)\n");
			break;
		}
		default:{
			printf("Invalid message type\n");
		}
	}
}

void message_free(struct message* message){
	if(message->type == MESSAGE_TYPE_LST_USER){
		if(message->lst_user.users != NULL){
			for(int i = 0; i < message->lst_user.nb_user; ++i){
				free(message->lst_user.users[i].username);
			}
			free(message->lst_user.users);
			message->lst_user.users = NULL;
		}
	}else if(message->type == MESSAGE_TYPE_JOIN){
		if(message->join.username != NULL){
			free(message->join.username);
			message->join.username = NULL;
		}
	}
}

bool message_from_client(struct message message){
	return message.type < 16;
}
struct message_addr message_get_client(struct message message){
	switch(message.type){
		case MESSAGE_TYPE_JOIN:{
			return message.join.addr;
		}
		case MESSAGE_TYPE_LEAVE:{
			return message.leave;
		}
		case MESSAGE_TYPE_PONG:{
			return message.pong;
		}
		case MESSAGE_TYPE_REQ_LST:{
			return message.req_lst;
		}
		default:{
			SCREAM("Attempt to extract a client address from a server message (id: %#x)", message.type);
		}
	}
}

bool message_for_server(struct message message){
	return message.type < 16; // come from a client
}
bool message_for_client(struct message message, struct message_addr addr){
	switch(message.type){
		case MESSAGE_TYPE_LST_USER:
			return true;
		case MESSAGE_TYPE_PING:
			return true;
		case MESSAGE_TYPE_ERR_CO:
			return MESSAGE_EQ_ADDR(addr, message.err_co.addr);
		case MESSAGE_TYPE_CLOSING:
			return true;
		default:
			return false;
	}
}