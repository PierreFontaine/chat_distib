#ifndef PROJECT_ADAPTER_H
#define PROJECT_ADAPTER_H

#include "sync.h"
#include "udp.h"
#include "array.h"

#include <pthread.h>

// todo add tcp listener


_Bool adapter_lauch_udp(sync_t sync, udp_t udp, pthread_t* thread);
_Bool adapter_lauch_keyboard(sync_t sync, pthread_t* thread);
_Bool adapter_lauch_timer(sync_t sync, tim_t tim, pthread_t* thread);

#endif
