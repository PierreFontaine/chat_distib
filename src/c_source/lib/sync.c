#include "sync.h"

#include "utils.h"

#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>


struct sync {
	int pipe[2];
	_Bool is_open;
};

struct sync* sync_init(){
	struct sync* res = malloc(sizeof(struct sync));
	if(res == NULL){
		SCREAM("Failed to malloc");
		return NULL;
	}
	
	if(pipe(res->pipe) < 0){
		free(res);
		SCREAM("Failed to open the pipe");
		return NULL;
	}
	res->is_open = true;
	return res;
}
void sync_close(struct sync* obj){
	obj->is_open = false;
}
bool sync_free(struct sync* obj){
	if(obj == NULL){
		SCREAM("Attempt to free an uninitialized sync object");
		return false;
	}
	close(obj->pipe[0]);
	close(obj->pipe[1]);
	free(obj);
	return true;
}

bool sync_keyboard_adapter(sync_t obj, struct keyboard_event event){
	struct sync_event container;
	container.type = SYNC_KEYBOARD;
	container.keyboard = event;
	write(obj->pipe[1], &container, sizeof(container));
	return obj->is_open;
}
bool sync_udp_adapter(sync_t obj, struct message event){
	struct sync_event container;
	container.type = SYNC_UDP;
	container.udp = event;
	write(obj->pipe[1], &container, sizeof(container));
	return obj->is_open;
}
bool sync_timer_adapter(sync_t obj, struct timespec event){
	struct sync_event container;
	container.type = SYNC_TIMER;
	container.timer = event;
	write(obj->pipe[1], &container, sizeof(container));
	return obj->is_open;
}
struct sync_event sync_poll(sync_t obj){
	struct sync_event container;
	read(obj->pipe[0], &container, sizeof(container));
	return container;
}