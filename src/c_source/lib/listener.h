#ifndef PROJECT_LISTENER_H
#define PROJECT_LISTENER_H

typedef struct listener* listener_t;
typedef void (*listener_handler)(void* handler_data, void* event_data);

listener_t listener_init();
_Bool listener_free(listener_t listener);

void listener_emit(listener_t listener, void* event_data);
_Bool listener_register(listener_t listener, listener_handler handler, void* handler_data, _Bool clean_up);
_Bool listener_unregister(listener_t listener, listener_handler handler);

#endif
