package net.server;

import net.message.BadMessageException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;

class ServerTest {
    InetAddress ip_m = InetAddress.getByName("239.0.0.1");
    Server s = new Server(ip_m, 6666, 4000, "finalway", NetworkInterface.getByName("en0"));

    ServerTest() throws IOException, RangePortException {
    }

    @Test
    void connection() throws IOException, NullResponseException, InterruptedException, BadMessageException {
        s.connection();
    }

    @Test
    void pong() {
    }

    @Test
    void requestUserList() {
    }

    @Test
    void leave() {
    }

    @Test
    void testReception() {
    }
}