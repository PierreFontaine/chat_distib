package misc.numbers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteUtils {
    /**
     *
     * @param i an integer to fit in a byte array dim 2
     * @return a byte array of dim 2
     * @author Fontaine Pierre
     */
    static public byte[] IntToShortByte(int i) {
        byte[] res = new byte[2];

        res[0] = (byte) (i);
        res[1] = (byte) (i >> 8);

        return res;
    }

    /**
     *
     * @param i an integer to fit in a byte array dim 4
     * @return a byte array dim 4
     * @author Fontaine Pierre
     */
    static public byte[] IntToIntByte(int i) {
        byte[] res = new byte[4];

        res[3] = (byte) (i>>24);
        res[2] = (byte) (i>>16);
        res[1] = (byte) (i>>8);
        res[0] = (byte) (i);

        return res;
    }

    /**
     * convert a byte array corresponding to a c-short in an Integer
     *
     * @param b
     * @return an Integer that represent the short int passed
     */
    static public Integer unsignedShortByteToInt(byte[] b) {
        Integer res;

        res = (b[0] & (0xFF)) + ((b[1] & (0xFF)) << 8);

        return res;
    }

    /**
     * convert a byt array corresponding to a c-unsigned-int in an Integer
     *
     * @param b
     * @return
     */
    static public Integer unsignedIntByteToInt(byte[] b) {
        Integer res;

        printByteArray(b);

        res = 0;
        res = res + b[0] + (b[1] << 8) + (b[2] << 16) + (b[3] << 24);

        return res;
    }

    /**
     * print a byte array as 0bxxxxxxxx format
     *
     * @param b byte array we want to display
     * @see <a href="https://gist.github.com/jpwsutton/6d0607cc849f480f25d1f51fddecbf4a"></a>
     */
    static public void printByteArray(byte[] b) {
        System.out.println("");
        for (byte b1 : b){
            String s1 = String.format("%8s", Integer.toBinaryString(b1 & 0xFF)).replace(' ', '0');
            s1 += " " + Integer.toHexString(b1);
            s1 += " " + b1;
            System.out.println(s1);
        }
        System.out.println("");
    }


}
