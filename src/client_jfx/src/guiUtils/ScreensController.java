package guiUtils;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.util.HashMap;

/**
 * @see <a href="https://www.youtube.com/watch?v=5GsdaZWDcdY" >tuto officiel @java screen framework</a>
 * @see <a href="https://github.com/acaicedo/JFX-MultiScreen/tree/master/ScreensFramework/src/screensframework">git</a>
 */
public class ScreensController extends StackPane {

    //need to know all scenes, for switching from one to another
    private HashMap<String, Node> screens = new HashMap<>();
    private HashMap<String, ControlledScreen> control = new HashMap<>();

    //ADD, LOAD, REMOVE SCREENS
    public ScreensController() {
        super();
    }

    /**
     * Add a screen to our screens containers
     *
     * @param name   the name given to the screen
     * @param screen the screen entity
     */
    public void addScreen(String name, Node screen) {
        screens.put(name, screen);
    }

    /**
     * Get a screen from our screens containers
     *
     * @param name the name that should match the screen we want to fetch
     * @return the root node of the screen
     */
    public Node getScreen(String name) {
        return screens.get(name);
    }

    /**
     * Add a controller from our controller container
     *
     * @param name the name of the vue
     */
    public void addController(String name, ControlledScreen c) {
        control.put(name, c);
    }

    /**
     * Get a controller given the name of a vue
     *
     * @param name name of the vue
     * @return a the controller linked to the vue
     */
    public ControlledScreen getController(String name) {
        return control.get(name);
    }

    /**
     * Load the fxml tree
     *
     * @param name     the name of the fxml tree we want to load
     * @param resource the URI of the fxml file storing the tree
     * @return true if fxml file match the URI
     */
    public boolean loadScreen(String name, String resource) {
        try {
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
            Parent loadScreen = (Parent) myLoader.load();
            ControlledScreen myScreenController = ((ControlledScreen) myLoader.getController());
            myScreenController.setScreenParent(this);
            addController(name, myScreenController);
            addScreen(name, loadScreen);

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * Method that allow us to display the screen that match the given name
     *
     * @param name the name of the screen we want to load
     * @return true if we could load the screen
     */
    public boolean setScreen(final String name) {
        if (screens.get(name) != null) { // We found the fxml file
            final DoubleProperty opacity = opacityProperty();


            if (!getChildren().isEmpty()) { // If there is already a scene displayed
                Timeline fade = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                getChildren().remove(0);
                                getChildren().add(0, screens.get(name));
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(800), new KeyValue(opacity, 1.0)));
                                fadeIn.play();
                            }
                        }, new KeyValue(opacity, 0.0)));
                fade.play();
            } else { // If there is no scene displayed so we can add it directly
                setOpacity(0.0);
                getChildren().add(screens.get(name));
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(2500), new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
            return true;
        } else { // FXML file wasn't solved
            System.out.println("Screen hasn't been loaded \n");
            return false;
        }
    }

    /**
     * Method that allow us to unload a screen from the container
     *
     * @param name name of the screen we want to unload
     * @return true if name was found in the container
     */
    public boolean unloadScreen(String name) {
        if (screens.remove(name) == null) {
            System.out.println("screen didn't exist");
            return false;
        } else {
            return true;
        }
    }
}
