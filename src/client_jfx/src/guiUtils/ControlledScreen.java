package guiUtils;

import net.server.Server;

public interface ControlledScreen {
    public void setScreenParent(ScreensController screenPage);
    public void setServer(Server s);
}
