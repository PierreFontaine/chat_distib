package net.server;

public class RangePortException extends Exception {
    private Integer port;

    public RangePortException(Integer port) {
        super("bad port boudaries");
        this.port = port;
    }

    /**
     * getter for port
     */
    public Integer getPort() {
        return this.port;
    }

    /**
     * setter for port
     * @param port
     */
    public void setPort(Integer port) {
        this.port = port;
    }
}
