package net.server;

import net.message.*;
import net.peer.Peer;
import net.sasync.AsynchronousDatagramSocket;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Server {
    private InetAddress in_addr_multicast_group;
    private Integer port_multicast_group;
    private SocketAddress group;
    private MulticastSocket m_socket;
    private Integer tcp_port; //port used for tcp co between clients
    private NetworkInterface ni;
    private AsynchronousDatagramSocket ads;
    private Peer user;
    private InetAddress interface_ip;
    private ArrayList<Peer> user_lst = new ArrayList<>();
    private Integer flagBanned = 0;

    /**
     * Use this constructor if you know the multicast settings.
     *
     * @param m_ip   the multicast address
     * @param m_port the multicast port
     * @throws IOException
     */
    public Server(InetAddress m_ip, Integer m_port, Integer t_port, String username, NetworkInterface ni) throws IOException, RangePortException {
        this.ni = ni;

        interface_ip = Collections.list(ni.getInetAddresses()).get(1);
        //check for port value boundaries
        if (t_port > 0 && t_port <= 65535) {
            this.tcp_port = t_port;
        } else {
            throw new RangePortException(t_port);
        }

        this.in_addr_multicast_group = m_ip;
        if (m_port > 0 && m_port <= 65535) {
            this.port_multicast_group = m_port;
        } else {
            throw new RangePortException(m_port);
        }

        //Socket Settings
        this.m_socket = new MulticastSocket(m_port);
        this.group = new InetSocketAddress(this.in_addr_multicast_group, this.port_multicast_group);

        this.m_socket.joinGroup(this.group, this.ni);

        this.m_socket.setReuseAddress(true);
        this.m_socket.setLoopbackMode(true); //don't want to receive my own packet

        //Async write and read
        this.ads = new AsynchronousDatagramSocket(this.m_socket, this.tcp_port, this.in_addr_multicast_group, this.port_multicast_group, interface_ip);

        this.user = new Peer(username, this.interface_ip, t_port);
        System.out.println(this.interface_ip);
    }

    /**
     * Tell the server you're joining the conv
     *
     * @author Fontaine Pierre
     */
    public void connection() throws IOException, NullResponseException, InterruptedException, BadMessageException {
        Integer count = 5;
        Boolean found = false;
        ConnectionMessage s = new ConnectionMessage(this.user.getUsername(),this.tcp_port, this.interface_ip);
        DatagramPacket dp = new DatagramPacket(s.toSend(), s.length(), this.in_addr_multicast_group, this.port_multicast_group);
        byte[] res = new byte[256];
        DatagramPacket dp2 = new DatagramPacket(res, 256);

        //SENDING PACKET VIA ADS
        System.out.println("Envoi du paquet");
        this.ads.send(dp);

        //CHECKING ADS BLOCKING QUEUE
        System.out.println("Reception du paquet réponse");
        while (count >= 0 && !found) {
            if(this.ads.asynchronousReceive(dp2)){
                System.out.println("list user received, are we in this list ???");
                ListUserMessage l = new ListUserMessage(dp2.getData());

                found = l.userIsInList(this.user);
                user_lst = l.getPeerList();

            }
            System.out.println("count : [" + count + "], we're going to sleep");
            count --;
            RequestListMessage rlm = new RequestListMessage(this.interface_ip, this.tcp_port);
            DatagramPacket dp_rlm = new DatagramPacket(rlm.toSend(), s.length(), this.in_addr_multicast_group, this.port_multicast_group);
            this.ads.send(dp_rlm);
        }

        if (found) {
            System.out.println("granted !");
        } else {
            throw new NullResponseException(count);
        }
    }

    /**
     * function used to request the list of users
     *
     * @throws IOException
     */
    public void requestUserList() throws IOException, InterruptedException, BadMessageException {
        RequestListMessage r = new RequestListMessage(interface_ip, tcp_port);
        DatagramPacket dp = new DatagramPacket(r.toSend(), r.length(), this.in_addr_multicast_group, this.port_multicast_group);
        byte[] res = new byte[256];
        DatagramPacket dp2 = new DatagramPacket(res, 256);

        System.out.println("Sending userlist request");
        this.ads.send(dp);

        if (this.ads.asynchronousReceive(dp2)) {
            System.out.println("new userlist received, we update the local one now");
            ListUserMessage l = new ListUserMessage(dp2.getData());
            user_lst = l.getPeerList();
        }
    }

    /**
     * function used to leave server
     *
     * @throws UnknownHostException
     */
    public void leave() throws IOException {
        LeaveMessage l = new LeaveMessage(this.interface_ip, this.tcp_port);
        DatagramPacket dp = new DatagramPacket(l.toSend(), l.length(), this.in_addr_multicast_group, this.port_multicast_group);

        this.ads.send(dp);
        this.ads.stopAsyncTasks();
        this.m_socket.close();
        System.exit(0);
    }

    /**
     * getter for multicastsocket
     *
     * @return the socket bound to multicast
     */
    public MulticastSocket getMulticastSocket() {
        return this.m_socket;
    }

    public void setUserLst(ArrayList<Peer> pl) {
        this.user_lst = pl;
    }

    public ArrayList<Peer> getUserLst() {
        return this.user_lst;
    }


    /**
     * test unit function cuz I don't know how to use JUnit yet
     * @param args
     */
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        String choice;

        InetAddress m_addr;

        System.out.println("Welcome in this debug console\n" +
                "use the following option without any dash !\n" +
                "\t p in order to send Pong message\n" +
                "\t c in order to send Connection message\n" +
                "\t l in order to send RequestUserList message\n");

        System.out.print(">>>");
        choice = sc.next();

        try {
            m_addr = InetAddress.getByName("239.0.1.1");
            Server s = new Server(m_addr, 6000, 4080,"Thingy", NetworkInterface.getByName("en0"));

            switch (choice) {
                case "c" :
                    System.out.println("We're going to send a Connection Message");
                    s.connection();
                    break;
                case "l" :
                    System.out.println("We're going to send a List Request Message");
                    s.requestUserList();
                    break;
                case "L":
                    System.out.println("We're going to send a Leave Message");
                    s.leave();
                    break;
                default :
                    System.out.println("Bad option");
                    System.exit(1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RangePortException e) {
            e.printStackTrace();
        } catch (NullResponseException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BadMessageException e) {
            e.printStackTrace();
        }

    }
}
