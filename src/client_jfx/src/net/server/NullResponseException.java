package net.server;

public class NullResponseException extends Exception {
    private Integer count;

    public NullResponseException (Integer count) {
        super("Didn't received a message that granted user");
        this.count = count;
    }

    /**
     * getter for count
     *
     * @return the number of try done by the client
     */
    public Integer getCount() {
        return count;
    }

    /**
     * setter for count
     *
     * @param count
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}
