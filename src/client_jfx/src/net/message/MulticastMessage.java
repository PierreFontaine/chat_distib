package net.message;

import java.io.Serializable;
import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * This class aim at describing packets for request between
 * Client and Server.
 * Packets'll be send via Multicast.
 */
public abstract class MulticastMessage implements Serializable {
    protected Integer id;
    protected String args;

    public ByteBuffer message = ByteBuffer.allocate(256);

    /**
     * Allow derived class to instance var on their own
     *
     * @author Fontaine Pierre
     */
    public MulticastMessage(){}

    /**
     * Make an instance for minimal config packet
     *
     * @param id Tell the server which kind of request your sending
     * @author Fontaine Pierre
     */
    public MulticastMessage(Integer id) {
        this.id = id;
    }

    /**
     * Make the packet
     *
     * @return the packet ready to be send through socket
     * @author Fontaine Pierre
     */
    abstract public byte[] toSend();

    public Integer length(){
        return this.message.capacity();
    }
}
