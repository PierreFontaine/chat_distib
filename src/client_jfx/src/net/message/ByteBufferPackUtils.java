package net.message;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import static misc.numbers.ByteUtils.unsignedShortByteToInt;

public class ByteBufferPackUtils {
    /**
     * method that extract the id value from the buffer
     *
     * @param b ByteBuffer storing all data sended
     * @return the id of the message
     */
    public static Integer extractId(ByteBuffer b) {
        Integer res;
        Byte id_b;

        id_b = b.get();
        res = id_b.intValue();

        return res;
    }

    /**
     * method that extract the ip value from the buffer
     *
     * @param b ByteBuffer storing all data sended
     * @return
     */
    public static InetAddress extractIp(ByteBuffer b) throws UnknownHostException {
        InetAddress ip;
        byte[] ip_b = new byte[4];

        for (int i = 0; i < ip_b.length; i++) {
            ip_b[i] = b.get();
        }

        ip = InetAddress.getByAddress(ip_b);

        return ip;
    }

    /**
     * method that extract the port value from the buffer
     *
     * @param b ByteBuffer storing all data sended
     * @return the port value
     */
    public static Integer extractPort(ByteBuffer b) {
        Integer port;
        byte[] port_b = new byte[2];

        port_b[0] = b.get();
        port_b[1] = b.get();

        port = unsignedShortByteToInt(port_b);

        return port;
    }
}
