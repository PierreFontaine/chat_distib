package net.message;

public class BadMessageException extends Exception {
    private Integer id;

    public BadMessageException(Integer id) {
        super("The id extracted is not the one expected ! " + id);
        this.id = id;
    }

    /**
     * Method that return the id extracted in the message, this id may not be one we were waiting for
     * so this exception was raised
     *
     * @return the id found in the packet
     */
    public Integer getId() {
        return id;
    }
}
