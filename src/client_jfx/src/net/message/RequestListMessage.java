package net.message;

import java.net.InetAddress;

import static misc.numbers.ByteUtils.IntToShortByte;

public class RequestListMessage extends MulticastMessage {

    private static Integer id = 0x03;
    private InetAddress ip;
    private Integer port;

    public RequestListMessage (InetAddress ip, Integer port) {
        super(RequestListMessage.id);
        this.ip = ip;
        this.port = port;
    }

    @Override
    public byte[] toSend() {
        byte id_b = this.id.byteValue();
        byte[] ip_b = this.ip.getAddress();
        byte[] port_b = IntToShortByte(this.port);

        this.message.put(id_b).put(ip_b).put(port_b);

        return this.message.array();
    }
}
