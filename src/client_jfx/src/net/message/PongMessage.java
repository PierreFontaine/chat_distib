package net.message;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static misc.numbers.ByteUtils.IntToShortByte;

public class PongMessage extends MulticastMessage {
    private static Integer id = 0x02;
    private InetAddress ip;
    private Integer port;

    /**
     *
     * @param p the port
     * @throws UnknownHostException
     */
    public PongMessage(Integer p, InetAddress ip_local) throws UnknownHostException {
        super(PongMessage.id);
        this.ip = ip_local;
        this.port = p;
    }

    @Override
    public byte[] toSend() {
        /* ********** */
        /* ID*IP*PORT */
        /* ********** */
        byte id_b = PongMessage.id.byteValue();
        byte[] ip_b = this.ip.getAddress();
        byte[] port_b = IntToShortByte(this.port);

        this.message.put(id_b).put(ip_b).put(port_b);

        this.message.put(id_b);
        return this.message.array();
    }
}
