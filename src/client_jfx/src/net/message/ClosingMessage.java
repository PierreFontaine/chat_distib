package net.message;

public class ClosingMessage {
    private static final Integer id = 0x13;

    /**
     * Mehtod that return the id of a closing message
     *
     * @return id of closing message
     */
    public static Integer getId(){
        return ClosingMessage.id;
    }
}
