package net.message;

import net.peer.Peer;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static misc.numbers.ByteUtils.*;

import static net.message.ByteBufferPackUtils.extractId;
import static net.message.ByteBufferPackUtils.extractIp;
import static net.message.ByteBufferPackUtils.extractPort;

/**
 * Class for parsing a list that store all users on the channel,
 * since this class is not used to send message we're not going
 * to make it inherited from MulticastMessage
 */
public class ListUserMessage {
    private ArrayList<Peer> peerList = new ArrayList<>(); //list to feed with peer
    private final static Integer id = 0x10;

    public ListUserMessage(byte[] peerList) throws UnknownHostException, UnsupportedEncodingException, BadMessageException {
        ByteBuffer p_bbf;
        Integer nbUsers;
        Integer id;

        Integer length = peerList.length;
        System.out.println("Taille du paquet recu : " + length);

        /* ************************ */
        /* ID*NB*{Port*IP*username} */
        /* ************************ */

        //Store peerList[] in a ByteBuffer p_ppf
        p_bbf = ByteBuffer.wrap(peerList);


        //Check if message type == 0x10
        id = extractId(p_bbf);

        if (!id.equals(ListUserMessage.id)) {
            throw new BadMessageException(id);
        }

        //nb user extract
        nbUsers = extractLength(p_bbf);
        System.out.println(nbUsers + " are stored in the given list");

        for (int i = 0; i < nbUsers; i++){
            Peer p = extractPeer(p_bbf);
            this.peerList.add(p);
        }
    }

    /**
     * check if a peer belong to list sended by server
     *
     * @param p
     * @return
     */
    public boolean userIsInList(Peer p) {
        System.out.println(p);
        boolean found = false;
        Iterator<Peer> it = this.peerList.iterator();

        while (it.hasNext() && !found) {
            Peer pi = it.next();
            if (pi.getUsername().equals(p.getUsername()) && pi.getAddress().equals(p.getAddress())) {
                found = true;
            }
        }

        return found;
    }

    /**
     * method that extract a peer
     *
     * @param b ByteBuffer storing all data sended
     * @return a peer
     * @throws UnknownHostException
     */
    private Peer extractPeer(ByteBuffer b) throws UnknownHostException, UnsupportedEncodingException {
        String username;
        InetAddress ip;
        Integer port;
        Peer res;

        //ip extract
        ip = extractIp(b);

        //port extract
        port = extractPort(b);

        //username extract
        username = extractUsername(b);

        res = new Peer(username, ip, port);
        System.out.println(res.toString()); //debug log

        return res;
    }

    /**
     * method that extract the username of the users stored in the buffer
     *
     * @param b ByteBuffer storing all data sended
     * @return the username fetched
     */
    private String extractUsername(ByteBuffer b) throws UnsupportedEncodingException {
        String res;
        byte[] username_b = new byte[256];
        int i = 0; //iter value

        while ((username_b[i] = b.get()) != '\0'){
            i++;
        }

        res = new String(Arrays.copyOfRange(username_b,0,i), "UTF-8");

        return res;
    }

    /**
     * method that extract the number of users stored from the buffer
     *
     * @param b ByteBuffer storing all data sended
     * @return the number of users stored
     */
    private Integer extractLength(ByteBuffer b) {
        Integer res;
        byte[] length_b = new byte[4];

        for (int i = 0; i < length_b.length; i++) {
            length_b[i] = b.get();
        }

        res = unsignedIntByteToInt(length_b);

        return res;
    }


    /**
     * setter for peer list
     *
     * @param p a peer list
     */
    public void setPeerList(ArrayList<Peer> p) {
        this.peerList = p;
    }

    /**
     * getter for peer list
     *
     * @return a peer list
     */
    public ArrayList<Peer> getPeerList() {
        return this.peerList;
    }

    /**
     * Method that return the id of a List user message
     *
     * @return the id of a list user message
     */
    public static Integer getId() {
        return ListUserMessage.id;
    }

}
