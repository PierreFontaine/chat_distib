package net.message;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;

import static misc.numbers.ByteUtils.IntToShortByte;

public class ConnectionMessage extends MulticastMessage {

    private static Integer id = 0x00;
    private String username;
    private InetAddress ip;
    private Integer port;

    /**
     *
     * @param username the username that client want for communicate
     * @param port the port that client is using for tcp connection
     * @throws UnknownHostException
     */
    public ConnectionMessage(String username, Integer port, InetAddress ip) throws SocketException {
        super(ConnectionMessage.id);
        this.username = username;
        this.ip = ip;
        this.port = port;
    }

    @Override
    public byte[] toSend() {
        /* ******************** */
        /* *ID*IP*PORT*USERNAME */
        /* ******************** */
        byte id_b = ConnectionMessage.id.byteValue();
        byte[] ip_b = this.ip.getAddress();
        byte[] port_b = IntToShortByte(this.port);
        byte[] username_b = this.username.getBytes();

        this.username += '\0'; /* null terminated string for C */
        this.message.put(id_b).put(ip_b).put(port_b).put(username_b);

        return this.message.array();
    }

    /**
     * Set the username
     *
     * @param username the username that overs gonna see
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the username
     *
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }
}
