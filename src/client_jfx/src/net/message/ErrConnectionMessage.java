package net.message;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import static net.message.ByteBufferPackUtils.extractId;
import static net.message.ByteBufferPackUtils.extractIp;
import static net.message.ByteBufferPackUtils.extractPort;

public class ErrConnectionMessage {

    private static final Integer id = 0x12;
    private InetAddress tcp_ip;
    private Integer tcp_port;
    private Integer error_code;
    private byte[] packet;

    public ErrConnectionMessage(byte[] p) throws BadMessageException, UnknownHostException {
        ByteBuffer p_bbf;
        Integer length;
        Integer id;

        length = p.length;

        this.packet = p;

        p_bbf = ByteBuffer.wrap(p);

        id = extractId(p_bbf);

        if (!id.equals(ErrConnectionMessage.id)) {
            throw new BadMessageException(id);
        }

        this.tcp_ip = extractIp(p_bbf);
        this.tcp_port = extractPort(p_bbf);
        this.error_code = extractErrCode(p_bbf);

    }

    /**
     * Method that print a message explaining why you received a so bad news
     */
    public void printWhyImGettingThisError() {
        switch (this.error_code) {
            case 0:
                System.out.println("You are banned");
                System.exit(0);
                break;
            case 1:
                System.out.println("Server reached its max capacity");
                break;
            case 2:
                System.out.println("Your username is too long");
                break;
            case 3:
                System.out.println("You're not connected, don't know why");
                break;
            default:
                System.out.println("You get an error while connecting, you may retry later");
        }
    }

    /**
     * Method that extract the error code wrapped in the packet
     *
     * @param b a ByteBuffer ref
     * @return the error code found
     */
    private Integer extractErrCode(ByteBuffer b) {
        Integer res;
        Byte errCode;

        errCode = b.get();
        res = errCode.intValue();

        return res;
    }

    /**
     * Method that return the id of a error connection message
     *
     * @return
     */
    public static Integer getId() {
        return ErrConnectionMessage.id;
    }

    /**
     * Method that return the ip address wrapped in the message
     *
     * @return the ip addr
     */
    public InetAddress getTcpIp() {
        return tcp_ip;
    }

    /**
     * Method that return the port wrapped in the message
     *
     * @return the port
     */
    public Integer getTcpPort() {
        return tcp_port;
    }

    /**
     * Method that return the error code wrapped in the message
     *
     * @return the error code
     */
    public Integer getErrorCode() {
        return error_code;
    }

    /**
     * Method that return the packet sended
     *
     * @return packet sended by the server
     */
    public byte[] getPacket() {
        return packet;
    }

    public boolean errorIsForMe(InetAddress ip, Integer port) {
        return ip.equals(getTcpIp()) && port.equals(getTcpPort());
    }
}
