package net.message;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static misc.numbers.ByteUtils.IntToShortByte;

public class LeaveMessage extends MulticastMessage{
    private static Integer id = 0x01;
    private Integer port;
    private InetAddress ip;

    public LeaveMessage(InetAddress ip, Integer port) throws UnknownHostException {
        super(LeaveMessage.id);
        this.port = port;
        this.ip = ip;
    }

    @Override
    public byte[] toSend() {
        byte id_b = id.byteValue();
        byte[] port_b = IntToShortByte(this.port);
        byte[] ip_b = this.ip.getAddress();

        this.message.put(id_b).put(ip_b).put(port_b);

        return this.message.array();
    }
}
