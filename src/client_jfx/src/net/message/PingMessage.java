package net.message;

public class PingMessage {
    private static final Integer id = 0x11;

    /**
     * Method that return the id of a Ping Message
     *
     * @return the id of a ping message
     */
    public static Integer getId() {
        return PingMessage.id;
    }
}
