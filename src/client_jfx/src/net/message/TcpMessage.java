package net.message;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class TcpMessage implements Serializable {
    private String message;

    /**
     * Constructor for making an instance from a String
     *
     * @param m
     */
    public TcpMessage(String m) {
        this.message = m;
    }

    /**
     * Constructor for parsing a message from byte[]
     *
     * @param b
     * @throws UnsupportedEncodingException
     */
    public TcpMessage(byte[] b) throws UnsupportedEncodingException {
        ByteBuffer p_bbf;

        p_bbf = ByteBuffer.wrap(b);
        message = extractMessage(p_bbf);
    }

    /**
     * Method that return the byte array conversion to send via the connection
     *
     * @return the message as a byte array
     */
    public byte[] toSend() {
        byte[] res = new String(this.message + '\0').getBytes();
        return  res;
    }

    /**
     * method that extract the username of the users stored in the buffer
     *
     * @param b ByteBuffer storing all data sended
     * @return the username fetched
     */
    private String extractMessage(ByteBuffer b) throws UnsupportedEncodingException {
        String res;
        byte[] username_b = new byte[256];
        int i = 0; //iter value

        while ((username_b[i] = b.get()) != '\0'){
            i++;
        }

        res = new String(Arrays.copyOfRange(username_b,0,i), "UTF-8");

        return res;
    }

    /**
     * Method that return the message
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }
}
