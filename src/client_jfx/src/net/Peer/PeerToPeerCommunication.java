package net.peer;

import misc.numbers.tuple.Pair;
import net.message.TcpMessage;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.*;

public class PeerToPeerCommunication {
    private Integer port_tcp;
    private InetAddress local_ip;
    private ServerSocket socket_ecoute;
    private ListenForMessageTask lfmt;
    private BlockingQueue<Pair<InetAddress, TcpMessage>> bq;
    private ExecutorService executor;

    /**
     * Constructor
     *
     * @param port port on wich we want to be available
     */
    public PeerToPeerCommunication(Integer port) throws IOException {
        //We bind the socket to the predefined port
        this.socket_ecoute = new ServerSocket(port);

        this.bq = new ArrayBlockingQueue<>(200);

        this.lfmt = new ListenForMessageTask(socket_ecoute, bq);
        Thread listen_thread = new Thread(lfmt);
        listen_thread.start();

    }

    /**
     * Send a message to over users
     *
     * @param m
     */
    public void sendMessage(String m, ArrayList<Peer> lst) throws InterruptedException, IOException {
        ExecutorService executor;

        executor = new ThreadPoolExecutor(20, 50, 120, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        Iterator<Peer> it;

        it = lst.iterator();

        while (it.hasNext()){
            Peer p = it.next();

            System.out.println("We send the message to " + p.getUsername() + " @" + p.getAddress().toString());

            executor.submit(new SendMessageTask(m, p));
            System.out.println("Task have been submitted to thread pool");
        }

        executor.shutdown();
        executor.awaitTermination(300, TimeUnit.SECONDS);
    }

    /**
     * Method for fetching the last message sent by any terminals
     *
     * @return message
     */
    public Pair<InetAddress, TcpMessage> getNextMessage() throws InterruptedException {
        return bq.take();
    }

    /**
     * Close the sockets
     */
    public void close() throws IOException {
        this.socket_ecoute.close();
    }
}
