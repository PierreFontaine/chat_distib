package net.peer;

import java.net.InetAddress;

/**
 * Class for modeling a user
 */
public class Peer {
    private String username;
    private InetAddress address;
    private Integer port;

    public Peer(String u, InetAddress a, Integer p) {
        this.username = u;
        this.address = a;
        this.port = p;
    }

    public String toString() {
        return String.format("Peer\n\tusername : %s\n\taddress : %s\n\tport : %d\n ", this.username, this.address.getHostName(), this.port);
    }

    /**
     * Get the username
     *
     * @return the username of the client
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Set the username
     *
     * @param username the username of the client
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the Address
     *
     * @return the address of the client
     */
    public InetAddress getAddress() {
        return address;
    }

    /**
     * Get the address of the client
     *
     * @param a the address of the client
     */
    public void setAddress(InetAddress a) {
        this.address = a;
    }

    /**
     * Get the port of the client
     *
     * @return the port of the client
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Set the port of the client
     *
     * @param p the port of client
     */
    public void setPort(Integer p) {
        this.port = p;
    }
}
