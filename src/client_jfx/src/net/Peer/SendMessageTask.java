package net.peer;

import net.message.TcpMessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SendMessageTask implements Runnable {

    private String message;
    private Socket s;

    public SendMessageTask(String m, Peer p) throws IOException {
        this.message = m;
        this.s = new Socket(p.getAddress(), p.getPort());
    }

    @Override
    public void run() {
        TcpMessage t_m = new TcpMessage(message);
        try {
            DataOutputStream outputStream = new DataOutputStream(s.getOutputStream());
            outputStream.write(t_m.toSend());
            s.close();
        } catch (IOException e) {
            System.err.println("Erreur dans SendMessageTask à l'envoi");
            e.printStackTrace();
        }
    }
}
