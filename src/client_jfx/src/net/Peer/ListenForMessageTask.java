package net.peer;

import misc.numbers.tuple.Pair;
import net.message.TcpMessage;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ListenForMessageTask implements Runnable{

    private ServerSocket socket_ecoute;
    private BlockingQueue<Pair<InetAddress, TcpMessage>> bq;

    public ListenForMessageTask(ServerSocket s, BlockingQueue bq) {
        this.socket_ecoute = s;
        this.bq = bq;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("ready to listen for incomming co");
                Socket socket = socket_ecoute.accept();

                DataInputStream input = new DataInputStream(socket.getInputStream());
                byte[] data = new byte[256];
                input.read(data);
                TcpMessage t = new TcpMessage(data);
                System.out.println("Message from " + socket.getLocalAddress().toString() + " : " +  t.getMessage());

                Pair<InetAddress, TcpMessage> p = new Pair(socket.getInetAddress(), t);
                socket.close();
                bq.add(p);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
