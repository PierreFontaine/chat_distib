package net.sasync;

import java.net.DatagramPacket;
import java.util.concurrent.Callable;

public class listUserPacketTask implements Callable<DatagramPacket> {
    private DatagramPacket dp;

    public listUserPacketTask(DatagramPacket dp){
        this.dp = dp;
    }

    public DatagramPacket call() {
        return this.dp;
    }
}
