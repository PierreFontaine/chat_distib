package net.sasync;

import net.message.PongMessage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class pongTask implements Runnable {
    private final MulticastSocket socket;
    private final Integer port;
    private InetAddress ms_addr;
    private Integer ms_port;
    private InetAddress ip_local;

    public pongTask(MulticastSocket ms, Integer p, InetAddress ms_addr, Integer ms_port, InetAddress ip_local) {
        this.ms_addr = ms_addr;
        this.ms_port = ms_port;
        this.socket = ms;
        this.port = p;
        this.ip_local = ip_local;
    }

    @Override
    public void run() {
        try {
            PongMessage p = new PongMessage(this.port, this.ip_local);
            DatagramPacket dp = new DatagramPacket(p.toSend(), p.length(), this.ms_addr, this.ms_port);
            synchronized (socket) {
                System.err.println("replying to ping message");
                System.err.println("in thread pong task : " + socket.getInterface());
                socket.send(dp);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
