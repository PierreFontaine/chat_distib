package net.sasync;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class AsynchronousDatagramSocket {

    private MulticastSocket socket;
    private Thread t;
    private Integer tcp_port;
    private BlockingQueue<DatagramPacket> bq;
    private packetManagerTask pmt;
    private InetAddress ms_addr;
    private Integer ms_port;
    private InetAddress ip_local;

    /**
     * Constructor
     *
     * @param ms a multicast socket for conversation with multicast channel
     * @param port the port used for tcp conversation between client
     */
    public AsynchronousDatagramSocket(MulticastSocket ms, Integer port, InetAddress ms_addr, Integer ms_port, InetAddress ip_local) {
        this.socket = ms;
        this.tcp_port = port;
        this.ms_addr = ms_addr;
        this.ms_port = ms_port;
        this.ip_local = ip_local;
        this.bq = new ArrayBlockingQueue<>(200);
        this.pmt = new packetManagerTask(bq,socket,tcp_port, ms_addr, ms_port,ip_local);
        this.t = new Thread(pmt);
        this.t.start();
    }

    public void stopAsyncTasks(){
        this.pmt.stopTask();
    }

    /**
     * method for sending message to multicast channel, note that we're making sure
     * that socket is not being used via 'synchronized' keyword
     *
     * @param d a packet
     * @throws IOException
     */
    public synchronized void send(DatagramPacket d) throws IOException {
        socket.send(d);
    }

    /**
     * method for retrieving packet in stack
     *
     * @param dp a packet in wich data'll be injected
     * @throws InterruptedException
     */
    public boolean asynchronousReceive(DatagramPacket dp) throws InterruptedException {
        DatagramPacket res;
        System.out.println("trying to fetch message");
        if ((res = this.bq.poll(5,TimeUnit.SECONDS)) != null){
            dp.setData(res.getData());
            return  true;
        }
        return false;
    }
}
