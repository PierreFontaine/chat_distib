package net.sasync;

import net.message.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;

import static java.lang.System.exit;
import static net.message.ByteBufferPackUtils.extractId;

public class packetManagerTask implements Runnable {
    private BlockingQueue<DatagramPacket> bq;
    private final MulticastSocket socket;
    private volatile boolean stop;
    private final Integer tcp_port;
    private InetAddress ms_addr;
    private Integer ms_port;
    private InetAddress ip_local;

    public packetManagerTask(BlockingQueue bq, MulticastSocket ms, Integer tcp_port, InetAddress ms_addr, Integer ms_port, InetAddress ip_local) {
        this.ms_addr = ms_addr;
        this.ms_port = ms_port;
        this.bq = bq;
        this.socket = ms;
        this.tcp_port = tcp_port;
        this.ip_local = ip_local;
        this.stop = false;
    }

    /**
     * May stop the task
     */
    public void stopTask(){
        this.stop = true;
    }

    @Override
    public void run() {
        while (!stop) {
            byte[] buffer = new byte[256];

            Integer xtracted_h;
            DatagramPacket d = new DatagramPacket(buffer, 256);
            try {
                ByteBuffer p_bbf;
                System.out.println("ready to fetch data in packetManagerTask");
                socket.receive(d);
                p_bbf = ByteBuffer.wrap(d.getData());
                //We only process that come from server
                xtracted_h = extractId(p_bbf);
                System.err.println(xtracted_h);

                if (xtracted_h == 0x10) {
                    //place packet in the blocking queue
                    System.err.println(xtracted_h + "we add it to list user message");
                    this.bq.add(d);

                } else if (xtracted_h == 0x11) {

                    //we reply to the ping, create a thread so we can continue process data
                    System.err.println("in thread pmt : " + socket.getInterface());
                    pongTask p_t = new pongTask(this.socket, this.tcp_port, this.ms_addr, this.ms_port, this.ip_local);
                    Thread t = new Thread(p_t);
                    t.start();

                } else if (xtracted_h.equals(ErrConnectionMessage.getId())) {

                    try {
                        ErrConnectionMessage e = new ErrConnectionMessage(d.getData());

                        if (e.errorIsForMe(ip_local, tcp_port)) {
                            e.printWhyImGettingThisError();
                        }

                    } catch (BadMessageException e1) {
                        e1.printStackTrace();
                    }

                } else if (xtracted_h.equals(ClosingMessage.getId())) {
                    exit(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
