package sample;

import javafx.concurrent.Task;
import javafx.scene.control.TextArea;
import misc.numbers.tuple.Pair;
import net.message.TcpMessage;
import net.peer.PeerToPeerCommunication;

import java.net.InetAddress;

public class UpdateMessageGUI extends Task<Void> {

    private PeerToPeerCommunication ppco;
    private final TextArea textarea;


    public UpdateMessageGUI(TextArea t, PeerToPeerCommunication p) {
        this.textarea = t;
        this.ppco = p;
    }

    @Override
    public Void call() throws Exception{
        while(true) {
            TcpMessage t;
            try {
                synchronized (textarea) {
                    Pair<InetAddress, TcpMessage> p = ppco.getNextMessage();
                    t = p.getRight();
                    InetAddress i = p.getLeft();
                    String message = t.getMessage();

                    System.out.println("In Runnable we retrieved : " + message);
                    textarea.appendText(">>> ");
                    textarea.appendText(i.toString() + " : " + message);
                    textarea.appendText("\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
