package sample.connectionGUI.alert;

public class AlertTimeOutCo extends AlertGeneral {
    public AlertTimeOutCo() {
        super("Le serveur à mis trop de temps pour répondre", "Attente trop longue");
    }
}
