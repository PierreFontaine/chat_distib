package sample.connectionGUI.alert;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertPortOutOfBound extends AlertGeneral{
    public AlertPortOutOfBound() {
        super("Verifiez la valeur du port", "Invalid Port value");
    }
}
