package sample.connectionGUI;

import guiUtils.ControlledScreen;
import guiUtils.ScreensController;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import net.server.RangePortException;
import net.server.Server;
import sample.ScreensFramework;
import sample.connectionGUI.alert.AlertPortOutOfBound;
import sample.connectionGUI.alert.AlertTimeOutCo;

import java.io.IOException;
import java.net.*;
import java.util.*;

public class Connection implements Initializable, ControlledScreen {

    public TextField inpLogin;
    public TextField inpAddIp;
    public TextField inpPort;
    public Button btnConnection;
    public ComboBox<String> list_interface;

    private InetAddress ip_m;
    private Integer port_m;
    private String pseudo;
    private Integer port_tcp;
    private NetworkInterface ni;

    private Server s;

    ScreensController myController;

    public Connection() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            Enumeration<NetworkInterface> ni_list = NetworkInterface.getNetworkInterfaces();

            for (NetworkInterface n: Collections.list(ni_list)) {
                list_interface.getItems().addAll(n.toString());
            }

        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

    /**
     * setter for server, useful for passing arg from Main to this controller
     *
     * @param s
     */
    public void setServer(Server s) {
        this.s = s;
    }

    /**
     * check if field are not empty
     *
     * @return true if fields aren't empty
     */
    private boolean checkField() {
        return this.inpAddIp.getText().length() > 0 && this.inpLogin.getText().length() > 0 && this.inpPort.getText().length() > 0;
    }

    /**
     * method that block all field
     */
    private void blockAllField() {
        setDisableFields(true);
    }

    private void setDisableFields(boolean b) {
        this.btnConnection.setDisable(b);
        this.inpPort.setDisable(b);
        this.inpLogin.setDisable(b);
        this.inpAddIp.setDisable(b);
        this.list_interface.setDisable(b);
    }

    private void allowAllField() {
        setDisableFields(false);
    }


    /**
     * gather field from gui
     *
     * @throws UnknownHostException
     */
    private void gatherField() throws UnknownHostException {
        ip_m = InetAddress.getByName(inpAddIp.getText());
        port_m = Integer.valueOf(inpPort.getText());
        pseudo = inpLogin.getText();
    }

    /**
     * Handle the instance of the server, and try to connect
     *
     * @param e
     */
    public void btnConnectionClicked(MouseEvent e) {
        System.out.println("u clicked co");

        try {
            gatherField();


            //Instance of server entity
            this.s = new Server(this.ip_m, this.port_m, 4080, this.pseudo, this.ni);

            //Make the cursor waiting
            blockAllField();

            //Create the task
            final Service<Void> connectionService = new Service<Void>() {
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            s.connection();
                            return null;
                        }
                    };
                }
            };

            //If co succeeded we may go forward and display the message window
            connectionService.setOnSucceeded(event -> {
                myController.getController(ScreensFramework.screenMessID).setServer(s);
                myController.setScreen(ScreensFramework.screenMessID);
            });

            //If connection throw error
            connectionService.setOnFailed(event -> {
                allowAllField();
                btnConnection.setDisable(false);
                AlertTimeOutCo a = new AlertTimeOutCo();
                a.show();
            });

            connectionService.start();

        } catch (UnknownHostException e1) {
            e1.printStackTrace();
        } catch (RangePortException e1) {
            AlertPortOutOfBound a = new AlertPortOutOfBound();
            a.show();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }

    public void cboChanged(Event e) {
        System.out.println("CLICK SUR CBO !" + list_interface.getValue().split(":")[1].split(" ")[0]);

        try {
            this.ni = NetworkInterface.getByName(list_interface.getValue().split(":")[1].split(" ")[0]);
            System.out.println(this.ni.getDisplayName());
        } catch (SocketException e1) {
            System.err.println("CBO CHANGED ! GIVE US BACK THE CBO" + list_interface.getValue().split(":")[1]);
            e1.printStackTrace();
        }

    }
}
