package sample.helpGUI;

import guiUtils.ControlledScreen;
import guiUtils.ScreensController;
import javafx.fxml.Initializable;
import net.server.Server;
import sample.ScreensFramework;

import java.net.URL;
import java.util.ResourceBundle;

public class Help implements Initializable, ControlledScreen {

    private ScreensController myController;
    private Server server;

    public Help() {

    }

    @Override
    public void setScreenParent(ScreensController screenPage) {
        this.myController = screenPage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public void setServer(Server s) {
        this.server = s;
    }
}
