package sample;

import guiUtils.ControlledScreen;
import guiUtils.ScreensController;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import net.message.BadMessageException;
import net.peer.Peer;
import net.peer.PeerToPeerCommunication;
import net.server.Server;

import java.io.IOException;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

public class Controller implements Initializable, ControlledScreen {

    public ListView<String> lstUsers = new ListView<>();
    public TextArea txtviewMess = new TextArea();
    public Button btnSend;
    public MenuItem refreshMenuItem;
    public TextField inpMess;
    public MenuItem closeMenuItem;
    private ScreensController myController;
    private Server server;
    private PeerToPeerCommunication ppco;
    private Service<Void> listenForMessage;
    private UpdateMessageGUI umGUI;

    public Controller() {
        try {
            ppco = new PeerToPeerCommunication(4080);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showUsers();
        txtviewMess.appendText("Start of communication .......\n");

        listenForMessage = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new UpdateMessageGUI(txtviewMess, ppco);
            }
        };

        listenForMessage.start();
    }

    @Override
    public void setScreenParent(ScreensController screenPage) {
        this.myController = myController;
    }

    /**
     * Method that request the user list and show it on the gui
     */
    public void showUsers() {
        if (server != null) {

            final Service<Void> requestUserListService = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            try {
                                server.requestUserList();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (BadMessageException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    };
                }
            };

            requestUserListService.setOnSucceeded(event -> {
                System.out.println("OK NOW WE LOAD THE USERS IN GUI");
                ArrayList<Peer> userLst;
                Iterator<Peer> it;

                //we request the user list
                userLst = server.getUserLst();

                it = userLst.iterator();

                //we clear the gui list
                lstUsers.getItems().clear();

                while (it.hasNext()){
                    Peer p = it.next();
                    System.out.println(p.getUsername());
                    lstUsers.getItems().add(p.getUsername());
                }
            });

            requestUserListService.start();
        } else {
            System.err.println("We'd like to maj users but server is a null entity :/");
        }
    }

    /**
     * Method that handle a click event on the send button
     *
     * @param e a click event on a button
     */
    public void btnSendClicked(Event e) {
        //Check message and send it to others terminals

        btnSend.setDisable(true);

        final Service<Void> sendMessageTask = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        try {
                            ppco.sendMessage(inpMess.getText(), server.getUserLst());
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        } catch (SocketException e1) {
                            e1.printStackTrace();
                        }
                        return null;
                    }
                };
            }
        };


        sendMessageTask.setOnSucceeded(event -> {
            btnSend.setDisable(false);
            inpMess.clear();
        });

        sendMessageTask.setOnFailed(event -> {
            btnSend.setDisable(false);
            inpMess.clear();
            System.err.println("Error sendMessageTask");
            System.err.println(event.toString());
        });

        sendMessageTask.start();



    }

    public void btnCloseClicked(Event e) {
        try {
            server.leave();
            Platform.exit();

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Method that allow us to pass a server, initialized in the Connection GUI
     *
     * @param s a ref server
     */
    public void setServer(Server s) {
        this.server = s;
    }
}
