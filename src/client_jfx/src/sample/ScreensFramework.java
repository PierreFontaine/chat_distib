package sample;

import guiUtils.ScreensController;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.server.Server;
import sample.connectionGUI.Connection;

import java.io.IOException;

public class ScreensFramework extends Application {

    public static String screenCoID = "connection";
    public static String screenCoFile = "/sample/connectionGUI/connection.fxml";
    public static String screenMessID = "message";
    public static String screenMessFile = "/sample/sample.fxml";

    @Override
    public void start(Stage primaryStage) {
        ScreensController mainContainer = new ScreensController();

        mainContainer.loadScreen(ScreensFramework.screenCoID, ScreensFramework.screenCoFile);
        mainContainer.loadScreen(ScreensFramework.screenMessID, ScreensFramework.screenMessFile);

        mainContainer.setScreen(ScreensFramework.screenCoID);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
