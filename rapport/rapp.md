

# Rapport projet système distribué

## Abstract

​	Dans ce projet, nous nous penchons sur la création d'une application permettant de communiquer entre terminaux via le protocole TCP, tout cela orchestré par un serveur communicant sur un canal multicast.

​	Dans ce document nous expliquerons comment est articulé le programme pour répondre à la problématique du projet.

## Client JAVA

###Liste des points essentiels

#### Connexion

Lors d'une tentative de connexion nous instancions un message de connexion (leur fonctionnement sera décrit à la suite). Le serveur doit répondre, soit favorablement en envoyant la liste des utilisateurs présent ou si le cas n'est pas favorable, par un message d'erreur.

Dans le cas où le serveur ne serait pas joignable, nous mettons en place un nombre d'essai limité, séparé par un timeout de quelques secondes. Une fois que le nombre d'essais est épuisé, nous affichons via le GUI une fenêtre modal précisant que le serveur à mis trop de temps à répondre.

#### Messages

Dans le cadre d'une communication bi-directionnelle entre le serveur et le client, nous avons établi un protocole.

Ce protocole définit pour chaque messages un identifiant, et selon les besoins peut contenir une adresse IP, un port, un pseudo ...

Dans le cadre de notre implémentation nous créons une classe abstraite (cf : *code_1*[^1]).

```java
public abstract class MulticastMessage implements Serializable {
    protected Integer id;
    protected String args;

    public ByteBuffer message = ByteBuffer.allocate(256);

    public MulticastMessage(){}

    public MulticastMessage(Integer id) {
        this.id = id;
    }
    abstract public byte[] toSend();

    public Integer length(){
        return this.message.capacity();
    }
}
```

*Code_1* [^1]

Cette classe sert à implémenter les messages de type [client $\rightarrow$ serveur], obligeant l'implémentation d'une fonction `toSend` transformant toutes les données en un paquet d'octet qui sera lisible par le serveur distant codé en *C*.

Les classes messages qui transitent exclusivement du serveur vers le client n'héritent pas de cette classe, car la redéfinition d'une méthode `toSend` ne porte aucun intéret dans ce cas. 
Elle porteront le code qui les définissent respectivement, et selon le besoin disposeront de getters pour extraire les données transmise (ie : nombre d'utilisateurs, port, adresse ...)

##### ByteBuffer

Afin de gagner du temps, nous avons utilisé un outil puissant pour traiter des flots de données de bas niveau. Fortement utilisé dans les applications I/O, le ByteBuffer nous donne accès à des méthodes telles que `get()`, `wrap()` … 

Dans l'exemple ci dessous[^2], nous montrons l'intêret de cette classe dans le cas d'une extraction d'adresse IP.

```java
public ListUserMessage(byte[] peerList) throws UnknownHostException, UnsupportedEncodingException, BadMessageException {
        ByteBuffer p_bbf;
        Integer nbUsers;
        Integer id;

        Integer length = peerList.length;
        System.out.println("Taille du paquet recu : " + length);
		...
        p_bbf = ByteBuffer.wrap(peerList);
    	...
}

...

public static InetAddress extractIp(ByteBuffer b) throws UnknownHostException {
    InetAddress ip;
    byte[] ip_b = new byte[4];

    for (int i = 0; i < ip_b.length; i++) {
        ip_b[i] = b.get();
    }

    ip = InetAddress.getByAddress(ip_b);

    return ip;
}
```

*Code_2*[^2]

##### C vers JAVA

Pour la conversion Big Endian vers Little Endian, mais aussi pour que l'on puisse utiliser des types comme `Short` en *C*, nous avons mis au point un package nommé `ByteBufferPackUtils`.

Il aurait été surement plus efficace d'utiliser des methodes comme `reverseByte()` pour certains usages face à un décalage bas niveau des bytes d'un entier.

```java
static public byte[] IntToShortByte(int i) {
    byte[] res = new byte[2];

    res[0] = (byte) (i);
    res[1] = (byte) (i >> 8);

    return res;
}
```

*Code_3*[^3] 

##### Prévention message

Nous avons défini une exception `BadMessageException` qui peut être levé si jamais l'id d'un message était corrompu.

#### Multicast

Nous modélisons le serveur par une classe, instancier celle ci permettra de communiquer avec le serveur pour par la suite mettre a jour la liste d'utilisateur, et répondre à différentes requêtes.

##### Socket

En Java pour créer une socket permettant la communication sur un groupe multicast, nous utilisons la méthode `MulticastSocket()`, ensuite il convient de créer un groupe en instanciant une objet de classe `InetSocketAddress` avec l'ip et le port du groupe multicast que nous souhaitons rejoindre. Enfin pour finir avec la MulticastSocket nous appliquons la méthode `.joinGroup(InetSocketAddress i, NetworkInterface ni)`.

```java
public class Server {
    private MulticastSocket m_socket;
    ...

    public Server(InetAddress m_ip, Integer m_port, Integer t_port, String username, NetworkInterface ni) throws IOException, RangePortException {
		...
        this.m_socket = new MulticastSocket();
        this.group = new InetSocketAddress(this.in_addr_multicast_group, this.port_multicast_group);
        this.m_socket.joinGroup(this.group, this.ni);
        ...
}
```

*Code_4*[^4]

L'utilisation de cette socket sera décrite dans la suite de ce rapport.

#### Communication client - serveur bidirectionnel

##### Traiter les messages serveurs

Nous créons un Runnable `packetManagerTask`, celui ci sera instancié et lancé dans la classe `AsynchronousDatagramSocket` au sein d'un Thread, comme le décrit l'exemple suivant[^5]. Nous noterons l'utilisation d'une `ArrayBlockingQueue`, essentielle pour comprendre le mecanisme de gestion de la liste d'utilisateurs.

```java
public AsynchronousDatagramSocket(MulticastSocket ms, Integer port, InetAddress ms_addr, Integer ms_port, InetAddress ip_local) {
    ...
    this.bq = new ArrayBlockingQueue<>(200);
    this.pmt = new packetManagerTask(bq,socket,tcp_port, ms_addr, ms_port,ip_local);
    this.t = new Thread(pmt);
    this.t.start();
    ...
}
```

*Code_5*[^5]

```java
public class packetManagerTask implements Runnable {
    
    ...
        
    @Override
    public void run() {
        while (!stop) {
            byte[] buffer = new byte[256];

            Integer xtracted_h;
            DatagramPacket d = new DatagramPacket(buffer, 256);
            try {
                ByteBuffer p_bbf;
                System.out.println("ready to fetch data in packetManagerTask");
                socket.receive(d);
                p_bbf = ByteBuffer.wrap(d.getData());
                //We only process that come from server
                xtracted_h = extractId(p_bbf);
                System.err.println(xtracted_h);

                if (xtracted_h == 0x10) {
                    //place packet in the blocking queue
                    System.err.println(xtracted_h + "we add it to list user message");
                    this.bq.add(d);
                } else if (xtracted_h == 0x11) {
                    pongTask p_t = new pongTask(this.socket, this.tcp_port, this.ms_addr, this.ms_port, this.ip_local);
                    Thread t = new Thread(p_t);
                    t.start();
                } else if (xtracted_h.equals(ErrConnectionMessage.getId())) {
                    try {
                        ErrConnectionMessage e = new ErrConnectionMessage(d.getData());
                        if (e.errorIsForMe(ip_local, tcp_port)) {
                            e.printWhyImGettingThisError();
                        }
                    } catch (BadMessageException e1) {
                        e1.printStackTrace();
                    }
                } else if (xtracted_h.equals(ClosingMessage.getId())) {
                    exit(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
```

*Code_6*[^6]

Ce morceaux de code est donc lancé en parallèle, pour chaque message récupéré sur le canal multicast il sera en mesure de ne traiter que les messages [serveur $\rightarrow$ client] en se basant sur l'id de ces derniers.

Pour chaque code, un traitement particulier est affecté, si le traitement est lourd il sera effectué dans un nouveau Thread et implémenté par un runnable (ie: répondre à un Ping).

###### Recevoir une liste

```java
if (xtracted_h == 0x10) {
    //place packet in the blocking queue
    System.err.println(xtracted_h + "we add it to list user message");
    this.bq.add(d);
```

*Code_7*[^7]

Précedemment nous avons introduit la `BlockingQueue`, ce conteneur est approprié à un model producteur/consommateur, ici le producteur c'est notre `packetManagerTask` et notre consommateur est `AsynchronousDatagramSocket`.

Dès qu'un paquet identifier comme contenir une liste d'utilisateur, nous l'ajoutons dans cette liste, ainsi le serveur pour récupérer cette information, parser le message et récupérer la liste d'utilisateur sous une forme utilisable pour y appliquer des traitements.

###### Répondre à un Ping

Nous avons décrit le fait que nous délaissions les tâches lourdes à une execution parallèle. Nous allons voir dans l'exemple suivant[^8] comment nous procédons pour une réponse à un Ping, et comment cette réponse est implémenter pour être executé en parallèle[^9].

```java
else if (xtracted_h == 0x11) {
    pongTask p_t = new pongTask(this.socket, this.tcp_port, this.ms_addr, this.ms_port, this.ip_local);
    Thread t = new Thread(p_t);
    t.start();
} 
```

*Code_8*[^8]

```java
public class pongTask implements Runnable {
    ...

    public pongTask(MulticastSocket ms, Integer p, InetAddress ms_addr, Integer ms_port, InetAddress ip_local) {
		...
    }

    @Override
    public void run() {
        try {
            PongMessage p = new PongMessage(this.port, this.ip_local);
            DatagramPacket dp = new DatagramPacket(p.toSend(), p.length(), this.ms_addr, this.ms_port);
            synchronized (socket) {
                socket.send(dp);
            }
        } catch (UnknownHostException e) {
			...
        }
    }
}
```

*Code_9*[^9]

Nous notons que dans la socket est synchronisé pour éviter un appel concurrent sur cette référence.

###### Erreur : ban / leave / capacity / username too long

Même principe que précédemment, nous identifions le type de messages, dans le cas d'une erreur nous serons en mesure de determiner la cause en parsant le message via l'utilisation de la classe `ErrConnectionMessage`.

Selon le type d'erreur un traitement spécial peut être affecté, prenons le cas d'un *ban*. Nous nous assurons que le message d'erreur est bien adresser à notre client via une vérification IP/PORT, et nous appliquons un kill du client. Celui ci ne pourra plus se connecter tant qu'il sera *ban*.

```java
case 0:
    System.out.println("You are banned");
    System.exit(0);
```

*code_10*

#### Communication client - client

##### Définition d'une entité client

Nous définissons une classe Peer, représentant un client avec qui l'on pourra  communiquer en TCP. Lorsque nous recevons une liste d'utilisateur présent sur le réseau, il devient alors facile des les transformer en Peer et de les stocker dans une ArrayList.

```java
public class Peer {
    private String username;
    private InetAddress address;
    private Integer port;

    public Peer(String u, InetAddress a, Integer p) {
        this.username = u;
        this.address = a;
        this.port = p;
    }

    public String toString() {
        return String.format("Peer\n\tusername : %s\n\taddress : %s\n\tport : %d\n ", this.username, this.address.getHostName(), this.port);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress a) {
        this.address = a;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer p) {
        this.port = p;
    }
}
```

*Code_11*

##### Thread pool

Dans le cas d'un programme réseau, ce qui est le cas de ce projet, nous souhaitons pouvoir supporter une certaine charge de connection. L'objectif est donc d'optimiser le code pour la JVM puisse supporter une charge de travail la plus conséquente possible.

Pour ce faire nous utilisons la classe `Executors` appartenant à `java.util.concurrent` et instances de classe héritant de `Runnable`.

```java
public void sendMessage(String m, ArrayList<Peer> lst) throws InterruptedException, IOException {
    ExecutorService executor;

    executor = new ThreadPoolExecutor(20, 50, 120, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
    Iterator<Peer> it;

    it = lst.iterator();

    while (it.hasNext()){
        Peer p = it.next();
        executor.submit(new SendMessageTask(m, p));
    }

    executor.shutdown();
    executor.awaitTermination(300, TimeUnit.SECONDS);
}
```

*Code_12*

```java
public ThreadPoolExecutor(int corePoolSize,
                          int maximumPoolSize,
                          long keepAliveTime,
                          TimeUnit unit,
                          BlockingQueue<Runnable> workQueue)
```

*Code_13*

Nous instancions donc un `Executor` avec `executor = new ThreadPoolExecutor(20, 50, 120, TimeUnit.SECONDS, new LinkedBlockingQueue<>());`. Cela permet de créer 20 Threads même s'ils ne sont pas tous utilisé, de ne pas dépasser un maximum de 50 Threads.

```java
public class SendMessageTask implements Runnable {

    private String message;
    private Socket s;

    public SendMessageTask(String m, Peer p) throws IOException {
        this.message = m;
        this.s = new Socket(p.getAddress(), p.getPort());
    }

    @Override
    public void run() {
        TcpMessage t_m = new TcpMessage(message);
        try {
            DataOutputStream outputStream = new DataOutputStream(s.getOutputStream());
            outputStream.write(t_m.toSend());
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

*Code_14*

#### GUI

#####Tasks et services

Lorsque l'on propose une interface, il est important pour l'experience utilisateur que son fonctionnement soit fluide.

Un problème vient se poser lorsque l'on veut réagir communiquer avec le serveur, pendant que l'on demande une liste d'utilisateur nous bloquons toute interaction avec l'interface.

Prenons le cas de la connexion. Nous utilisons une méthode implémentée dans la classe `src.net.server` qui s'appele `Connexion` . Cette méthode dans une implémentation par terminal pourrait être appelé et executé dans le Thread principale, mais ici pour éviter les problème expliqué précedemment nous allons utiliser les classes `javafx.concurrent.Task` et `javafx.concurrent.Service`.

```java
final Service<Void> connectionService = new Service<Void>() {
    protected Task<Void> createTask() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                s.connection();
                return null;
            }
        };
    }
};

connectionService.setOnSucceeded(event -> {
    myController.getController(ScreensFramework.screenMessID).setServer(s);
    myController.setScreen(ScreensFramework.screenMessID);
});

connectionService.setOnFailed(event -> {
    allowAllField();
    btnConnection.setDisable(false);
    AlertTimeOutCo a = new AlertTimeOutCo();
    a.show();
});

connectionService.start();
```

*Code_15*

Dans l'exemple ci dessus, nous avons fait appel à une classe anonyme en redéfinissant la méthode `run`, mais il est aussi possible pour un soucie de maintenance du code, de créer une classe héritant de `Task`.

```java
public class UpdateMessageGUI extends Task<Void> {

    private PeerToPeerCommunication ppco;
    private final TextArea textarea;


    public UpdateMessageGUI(TextArea t, PeerToPeerCommunication p) {
        this.textarea = t;
        this.ppco = p;
    }

    @Override
    public Void call() throws Exception{
        while(true) {
            TcpMessage t;
            try {
                synchronized (textarea) {
                    t = ppco.getNextMessage();
                    String message = t.getMessage();
                    System.out.println("In Runnable we retrieved : " + message);
                    textarea.appendText(">>> ");
                    textarea.appendText(message);
                    textarea.appendText("\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
```

*Code_16*